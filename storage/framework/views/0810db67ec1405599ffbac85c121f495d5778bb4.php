

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">


                 <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>


      <div class="row">
        <div class="col-lg-8">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="allebestellingen-tab" data-toggle="tab" href="#allebestellingen" role="tab" aria-controls="allebestellingen" aria-selected="true">Alle bestellingen</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="newproducts-tab" data-toggle="tab" href="#newproducts" role="tab" aria-controls="newproducts" aria-selected="false">Nieuwe producten</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="openstaand-tab" data-toggle="tab" href="#openstaand" role="tab" aria-controls="openstaand" aria-selected="false">Openstaand</a>
  </li>
</ul>

<br />

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="allebestellingen" role="tabpanel" aria-labelledby="allebestellingen-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Laatste bestellingen</div>
        <div class="card-body">

          <?php if($orders->isNotEmpty()): ?>
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <td>Klant</td>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><a href="/bekijkklant/<?php echo e($order->klant()->first()->id); ?>"><?php echo e($order->klant()->first()->bedrijfsnaam); ?></a></td>
                  <td><?php echo e($order->status($order->id)); ?></td>
                  <td>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></td>
                  <td><?php echo e($order->created_at->format('m/d/Y h:i:s')); ?></td>
                  <td><a href="<?php echo e(route('bestelling', ['id' => $order->id])); ?>" class="btn btn-success">Bekijken</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
          <?php else: ?>
          Er zijn vandaag geen bestellingen gemaakt.
          <?php endif; ?>
        </div>
      </div>

       <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> On-hold bestellingen</div>
        <div class="card-body">
          <?php if($ordershold->isNotEmpty()): ?>
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $ordershold; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><a href="/bekijkklant/<?php echo e($order->klant()->first()->id); ?>"><?php echo e($order->klant()->first()->bedrijfsnaam); ?></a></td>
                  <td><?php echo e($order->status($order->id)); ?></td>
                  <td>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></td>
                  <td><?php echo e($order->created_at->format('m/d/Y h:i:s')); ?></td>
                  <td><a href="<?php echo e(route('checkonhold', ['id' => $order->id])); ?>" class="btn btn-success">Bekijken</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
          <?php else: ?> 
          Er zijn geen on-hold bestellingen gevonden.
          <?php endif; ?>
        </div>
      </div>

    </div>



  <div class="tab-pane fade" id="openstaand" role="tabpanel" aria-labelledby="openstaand-tab">
  <br />
              <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">

               <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Bekijken</th>
                </tr>
              </thead>

                <tbody>
                <?php $__currentLoopData = $waardig; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $waardigs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($waardigs->bedrijfsnaam); ?></td>
                  <td><a class="btn btn-success" href="/bekijkklant/<?php echo e($waardigs->id); ?>">Bekijken</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>

            </table>
          </div>

  </div>

  <div class="tab-pane fade" id="newproducts" role="tabpanel" aria-labelledby="newproducts-tab">

          <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Producten zie vandaag zijn toegevoegd.</div>
        <div class="card-body">

          <?php if($productstoday->isNotEmpty()): ?>
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Productnaam</th>
                  <th>Korte omschrijving</th>
                  <td>Categorie</th>
                  <th>Aanpassen</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $productstoday; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($product->name); ?></td>
                  <td><?php echo e($product->shortdescription); ?></td>
                  <td>-</td>
                  <td></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
          <?php else: ?>
          Er zijn vandaag geen producten toegevoegd.
          <?php endif; ?>
        </div>
      </div>
    </div>
      </div>


  </div>

          <div class="card mb-3" style="display: none; visibility: hidden;">
           <div class="card-header">
              <i class="fa fa-bell-o"></i> Activiteiten log</div>
            <div class="list-group list-group-flush small">

              <?php $__currentLoopData = $logs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $log): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <a class="list-group-item list-group-item-action" href="#">
                <div class="media">
                  <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
                  <div class="media-body">
                    <strong></strong> heeft <?php echo e($log->description); ?>.
                    <div class="text-muted smaller"><?php echo e($log->created_at->format('m/d/Y h:i:s')); ?></div>
                  </div>
                </div>
              </a>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <a class="list-group-item list-group-item-action" href="#">View all activity...</a>
            </div>
          </div>
   

          <div class="mb-3 col-lg-4">
            <div class="card">
            <div class="card-header">
              <i class="fa fa-bell-o"></i> Agenda <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modalAgenda">Nieuw item</button></div>
            <div class="list-group list-group-flush small">

             <?php $__currentLoopData = $kalender; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kalender): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <a class="list-group-item list-group-item-action" href="/deletekalender/<?php echo e($kalender->id); ?>">
                <div class="media">
                  <div class="media-body">
                    <strong><?php echo e($kalender->content); ?></strong>.
                    <div class="text-muted smaller"><?php echo e($kalender->created_at->format('m/d/Y h:i:s')); ?></div>
                    <div onclass="delete-agenda">Verwijderen</div>
                  </div>
                </div>
              </a>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
          </div>
     </div>
</div>
      </div>
    </div>


      <!-- Modal -->
  <div class="modal fade" id="modalAgenda" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo e(action('DashboardController@agendanew')); ?>" class="" method="post">

            <?php echo csrf_field(); ?>

            <div class="form-group">
              <input class="form-control" name="content" placeholder="Bericht" type="text">
            </div>

            <div class="form-group">
                <input class="form-control" name="tijd" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
            </div>

            <button class="btn btn-default" type="submit">Opslaan</button>

          </form>

         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Sluiten</button>
        </div>
      </div>
    </div>
  </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>