

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Klant aanpassen</h3> <br />


      <form method="post" action="<?php echo e(action('KlantController@modify', ['id' => $klant->id])); ?>">

      <?php echo csrf_field(); ?>

      <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

      <div class="row">

        <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Klantgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="voornaam">Voornaam</label>
            <input name="voornaam" class="form-control" placeholder="Voornaam van de klant" value="<?php echo e($klant->voornaam); ?>" required>
          </div>

          <div class="form-group">
             <label for="achternaam">Achternaam</label>
            <input name="achternaam" class="form-control" placeholder="Achternaam van de klant" value="<?php echo e($klant->achternaam); ?>" required>
          </div>

          <div class="form-group">
             <label for="bedrijfsnaam">Bedrijfsnaam</label>
            <input name="bedrijfsnaam" class="form-control" placeholder="Bedrijfsnaam van de klant" value="<?php echo e($klant->bedrijfsnaam); ?>" required>
          </div>

        </div>
      </div>

            <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-address-card"></i> Adresgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="adres">Adres</label>
            <input name="adres" class="form-control" placeholder="Adres van de klant"  value="<?php echo e($klant->adres); ?>" required>
          </div>

          <div class="form-group">
             <label for="adres">Huisnummer</label>
            <input name="huisnummer" class="form-control" placeholder="Huisnummer van de klant"  value="<?php echo e($klant->huisnummer); ?>" required>
          </div>

          <div class="form-group">
             <label for="stad">Stad</label>
            <input name="stad" class="form-control" placeholder="Stad van de klant"  value="<?php echo e($klant->stad); ?>" required>
          </div>

          <div class="form-group">
             <label for="postcode">Postcode</label>
            <input name="postcode" class="form-control" placeholder="Postcode van de klant" value="<?php echo e($klant->postcode); ?>" required>
          </div>

           <div class="form-group">
            <label for="country">Land</label>
         <select id="country" class="form-control" name="country">
            <option value="Netherlands">Nederland</option>
            <option value="Belgie">België</option>
            <option value="France">Frankrijk</option>
            <option value="Duitsland">Duitsland</option>
      </select>
       </div>



        </div>
      </div>
    </div>

            <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-phone"></i> Contactgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="email">E-mail adres</label>
            <input type="email" name="email" class="form-control" placeholder="E-mail adres van de klant" value="<?php echo e($klant->email); ?>" required>
          </div>

          <div class="form-group">
             <label for="telefoon">Telefoonnummer</label>
            <input type="text" name="telefoon" class="form-control" placeholder="Telefoonnummer van de klant" value="<?php echo e($klant->telefoon); ?>" required>
          </div>

        </div>
      </div>

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-credit-card"></i> Krediet</div>
        <div class="card-body">

          <div class="form-group">
          <input type="checkbox" class="" <?php if($klant->kredietplafon === '0'): ?> <?php else: ?> checked="checked" <?php endif; ?> name="checkkrediet" id="checkkrediet">
          <label class="" id="checkkrediet"  for="checkkrediet">Deze klant kan op krediet bestellen</label>
          </div>

          <div id="kredietplafon" class="form-group">
             <label for="maxkrediet">Kredietplafon (In euro's)</label>
            <input type="text" name="maxkrediet" class="form-control" value="<?php echo e($klant->kredietplafon); ?>" placeholder="Kredietplafon van de klant" required>
          </div>


          <div class="form-group">
          <input type="checkbox"  value="1" class="" <?php if(is_null($klant->factureren)): ?> <?php else: ?> checked="checked" <?php endif; ?> name="checkfactuur" id="checkfactuur">
          <label class="" id="checkfactuur" for="checkfactuur">Altijd factureren bij deze klant</label>
          </div>

        </div>
      </div>


                              <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password"> <br />

      <button class="btn btn-success" type="submit">Opslaan</button>

    </div>

  </form>

  </div>
 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
$(function () {

  <?php if($klant->kredietplafon === '0'): ?> $('#kredietplafon').hide(); <?php endif; ?>

    //show it when the checkbox is clicked
    $('#checkkrediet').on('click', function () {
        if ($(this).prop('checked')) {
            $('#kredietplafon').fadeIn();
        } else {
            $('#kredietplafon').hide();
        }
    });
});
</script>

<script>
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>