

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Klanten</h3>

      <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

<div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Alle klanten</div>
        <div class="card-body">

          <p><a class="btn btn-success" href="<?php echo e(route('newklant')); ?>">Nieuwe klant</a></p>

          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam</th>
                  <th>E-mail adres</th>
                  <td>Bekijken</td>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $klanten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $klant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($klant->bedrijfsnaam); ?></td>
                  <td><?php echo e($klant->email); ?></td>
                  <td><a href="<?php echo e(route('bekijkklant', ['id' => $klant->id])); ?>" class="btn btn-success">Bekijken</a>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div id="deleteModal" class="modal fade">
      <div class="modal-dialog modal-confirm">
        <div class="modal-content">
          <div class="modal-header">    
            <h4 class="modal-title">Zeker weten?</h4>  
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Weet je zeker dat je deze categorie wilt verwijderen?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            <a href="" id="deleteButton" class="btn btn-danger">Delete</a>
          </div>
        </div>
      </div>
    </div>  

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>

// Delete modal logica

var delUrl;

$('.deletemodal').click(function() {
   delUrl = $(this).attr('data-url'); 
   alert(delUrl);
});

$('#deleteModal').on('show.bs.modal', function (e) {
    $(this).find('#deleteButton').prop("href", delUrl)
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>