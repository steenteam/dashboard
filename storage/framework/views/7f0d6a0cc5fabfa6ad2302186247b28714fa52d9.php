  <!-- Navigation a -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?php echo e(route('dashboard')); ?>">Whiterose </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <br />
        <a href="/newbestelling" class="btn-new-bestelling btn btn-success">Nieuwe bestelling</a>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo e(route('dashboard')); ?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>


        <?php if(Auth::user()->id == 7 || Auth::user()->id = 1): ?>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo e(route('magazijn')); ?>">
            <i class="fa fa-fw fa-building"></i>
            <span class="nav-link-text">Magazijn</span>
          </a>
        </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo e(route('facturen')); ?>">
            <i class="fa fa-fw fa-credit-card"></i>
            <span class="nav-link-text">Factureren</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="<?php echo e(route('statistieken')); ?>">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Statistieken</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Voorraad">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#VoorraadPagina" data-parent="#VoorraadPagina">
            <i class="fa fa-fw fa-archive"></i>
            <span class="nav-link-text">Inkoop</span>
          </a>
          <ul class="sidenav-second-level collapse" id="VoorraadPagina">
            <li>
              <a href="<?php echo e(route('vorraadbeheer')); ?>">Inkoopbeheer</a>
            </li>
            <li>
              <a href="<?php echo e(route('exporteren')); ?>">Voorraad analyse</a>
            </li>
          </ul>
        </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="bestelling">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#bestellingPagina" data-parent="#bestellingPagina">
            <i class="fa fa-fw fa-folder-open"></i>
            <span class="nav-link-text">Bestellingen</span>
          </a>
          <ul class="sidenav-second-level collapse" id="bestellingPagina">
            <li>
              <a href="<?php echo e(route('bestellingen')); ?>">Alle bestellingen</a>
            </li>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create order')): ?>
            <li>
              <a href="<?php echo e(route('newbestelling')); ?>">Nieuwe bestelling</a>
            </li>
            <?php endif; ?>
          </ul>
        </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="userbeheer">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#userbeheerPagina" data-parent="#userbeheerPagina">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">Gebruikers</span>
          </a>
          <ul class="sidenav-second-level collapse" id="userbeheerPagina">
            <li>
              <a href="<?php echo e(route('userbeheer')); ?>">Alle gebruikers</a>
            </li>
            <li>
              <a href="<?php echo e(route('newuser')); ?>">Nieuwe gebruiker</a>
            </li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Product">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#productPagina" data-parent="#productPagina">
            <i class="fa fa-fw fa-product-hunt"></i>
            <span class="nav-link-text">Product</span>
          </a>
          <ul class="sidenav-second-level collapse" id="productPagina">
            <li>
              <a href="<?php echo e(route('product')); ?>">Product / Producten</a>
            </li>
            <li>
              <a href="<?php echo e(route('categorieen')); ?>">Product Categoriën</a>
            </li>
          </ul>
        </li>

        <?php endif; ?>



         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="klanten">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#klantenPagina" data-parent="#klantenPagina">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Klanten</span>
          </a>
          <ul class="sidenav-second-level collapse" id="klantenPagina">

        <?php if(Auth::user()->id == 7): ?>
            <li>
              <a href="<?php echo e(route('klanten')); ?>">Alle klanten</a>
            </li>
            <?php endif; ?>
             <li>
              <a href="<?php echo e(route('newklant')); ?>">Nieuwe klant</a>
            </li>
          </ul>
        </li>

         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="klanten">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Uitloggen</a>
        </li>


      </ul>


      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">


                    <form method="post" action="/zoekklant" id="formsearch" class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <?php echo csrf_field(); ?>
              <select onchange="this.form.submit()" name="q" class="search">
                <option value=""></option>
                <?php $__currentLoopData = $klanten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $klant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($klant->id); ?>"><?php echo e($klant->bedrijfsnaam); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
                            <span class="zoek-knop input-group-append">
                <button class="btn btn-primary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
      </ul>
    </div>
  </nav>