

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Bestelling #<?php echo e($order->ordercode); ?></h3>

      <div class="row">
        <div class="col-6">

            <div class="card">
              <div class="card-header">
                Bestelling van <strong><?php echo e($order->klant()->first()->bedrijfsnaam); ?></strong> op <?php echo e($order->created_at->format('m/d/Y h:i:s')); ?>

              </div>

              <div class="card-body">
                <table>
                  <tr>
                    <td><b>Status:</b></td>
                    <td><?php echo e($order->status($order->id)); ?></td>
                  </tr>
                  <tr>
                    <td><b>Totaal:</b></td>
                    <td>€ <?php echo e($order->order_total($order->id)  + $order->verzendkosten); ?> euro</td>
                  </tr>
                </table> <br />
                <p>
                  <a href="<?php echo e(route('bekijkklant', ['id' => $order->klant()->first()->id])); ?>" class="btn btn-warning">Bekijk het account van de deze klant</a>

                  <button data-target="#pakbonprint" data-toggle="modal" type="" class="btn btn-success"> <i class="fa fa-fw fa-print"></i> Print pakbon</button>

                                      <?php if($order->getTracking($order->id) != 'null'): ?>
                     <a href="<?php echo e($order->getTracking($order->id)); ?>" target="_blank" class="btn btn-warning">Traceren</a> &nbsp;

                     <?php endif; ?>
                </p>

                <?php if($order->status == 1): ?>
                  <form action="<?php echo e(action('OrderController@cancel', ['id' => $order->id])); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <button type="submit" class="btn btn-danger">Order annuleren</button>
                  </form>
                <?php endif; ?>
              </div>
            </div>
           <br />
        </div>
        
        <div class="col-6">
        <?php if($checkorders): ?>
            <div class="card">
              <div class="card-header">
                Verekening
              </div>

              <div class="card-body">

                <a class="btn btn-success" href="/bekijkverekening/<?php echo e($checkorders->verekening_id); ?>">Bekijk de verekening bij deze order</a>
                
              </div>
            </div>

            <?php endif; ?>

            <?php if(!is_null($order->factuur)): ?>

                        <div class="card">
              <div class="card-header">
                Factureren
              </div>

              <div class="card-body">

                <?php if($order->factuur == 0): ?>

                <form action="/voltooifactuur/<?php echo e($order->id); ?>">
                  <?php echo csrf_field(); ?>
                  <div class="form-group">
                    <input type="text" name="factuurnr" class="form-control" placeholder="Factuur nummer" required>
                  </div>
                    <button type="submit" class="btn btn-success">Factuur voltooien</button>
                  </div>
                </form>

                <?php endif; ?>

                <?php if($order->factuur == 1): ?>

                Deze order is gefactureerd en voltooid. <br />
                Factuurnummer: <b><?php echo e($order->factuurnr); ?></b>

                <?php endif; ?>
        
              </div>
            </div>

            <?php endif; ?>

        </div>
      </div>

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle producten</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Korte omschrijving</th>
                  <th>Aantal</th>
                  <td>Stukprijs</td>
                  <th>Totaal</th>
                </tr>
              </thead>
              <tfoot>
                 <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Verzendkosten:</th>
                  <th>€ <?php echo e($order->verzendkosten); ?></th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Totaal:</th>
                  <th>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></th>
                </tr>
              </tfoot>
              <tbody>
               <?php $__currentLoopData = $order->products()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($product->sku); ?></td>
                  <td><?php echo e($product->name); ?></td>
                  <td><?php echo e($product->shortdescription); ?></td>
                  <td><?php echo e($order->aantal($product->id, $order->id)); ?></td>
                  <td>€ <?php echo e($order->stukprijs($product->id, $order->id)); ?>

                  <td>€ <?php echo e($order->prijs($product->id, $order->id)); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>


    </div>

    <!-- Modal -->-
<div class="modal fade" id="pakbonprint" tabindex="-1" role="dialog" aria-labelledby="pakbonprintTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">De pakbon printen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Weet je zeker dat je deze pakbon wilt afdrukken?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <button type="button" id="printnu" class="btn btn-primary">Nu printen</button>
      </div>
    </div>
  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script>

  $('#printnu').click(function() {

    $(this).hide();

  });

$('#printnu').click(function() {
 $('#pakbonprint').modal('toggle');
   $.ajax({
  type: 'GET',
  url: '/printpakbon/<?php echo e($order->ordercode); ?>',
  data: "",
  async: false
}).done(function(response) {
   
  });

});

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>