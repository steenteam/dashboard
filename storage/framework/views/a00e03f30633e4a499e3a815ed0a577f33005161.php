

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Een nieuwe klant maken</h3> <br />


      <form method="post" action="<?php echo e(action('KlantController@create')); ?>">

      <?php echo csrf_field(); ?>

      <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

      <div class="row">

        <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Klantgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="voornaam">Voornaam</label>
            <input name="voornaam" class="form-control" placeholder="Voornaam van de klant" required>
          </div>

          <div class="form-group">
             <label for="achternaam">Achternaam</label>
            <input name="achternaam" class="form-control" placeholder="Achternaam van de klant" required>
          </div>

          <div class="form-group">
             <label for="bedrijfsnaam">Bedrijfsnaam</label>
            <input name="bedrijfsnaam" class="form-control" placeholder="Bedrijfsnaam van de klant" required>
          </div>

        </div>
      </div>

            <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-address-card"></i> Adresgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="adres">Adres</label>
            <input name="adres" class="form-control" placeholder="Adres van de klant" required>
          </div>

          <div class="form-group">
             <label for="adres">Huisnummer</label>
            <input name="huisnummer" class="form-control" placeholder="Huisnummer van de klant" required>
          </div>

          <div class="form-group">
             <label for="stad">Stad</label>
            <input name="stad" class="form-control" placeholder="Stad van de klant" required>
          </div>

          <div class="form-group">
             <label for="postcode">Postcode</label>
            <input name="postcode" class="form-control" placeholder="Postcode van de klant" required>
          </div>

           <div class="form-group">
            <label for="country">Land</label>
         <select id="country" class="form-control" name="country">
            <option value="Netherlands">Nederland</option>
            <option value="Belgie">België</option>
            <option value="France">Frankrijk</option>
            <option value="Duitsland">Duitsland</option>
      </select>
       </div>



        </div>
      </div>
    </div>

            <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-phone"></i> Contactgegevens</div>
        <div class="card-body">

          <div class="form-group">
             <label for="email">E-mail adres</label>
            <input type="email" name="email" class="form-control" placeholder="E-mail adres van de klant" required>
          </div>

          <div class="form-group">
             <label for="telefoon">Telefoonnummer</label>
            <input type="text" name="telefoon" class="form-control" placeholder="Telefoonnummer van de klant" required>
          </div>

        </div>
      </div>

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-credit-card"></i> Extra</div>
        <div class="card-body">

          <div class="form-group">
          <input type="checkbox" class="" name="checkkrediet" id="checkkrediet">
          <label class="" id="checkkrediet" for="checkkrediet">Deze klant kan op krediet bestellen</label>
          </div>

          <div id="kredietplafon" class="form-group">
             <label for="maxkrediet">Kredietplafon (In euro's)</label>
            <input type="text" name="maxkrediet" class="form-control" placeholder="Kredietplafon van de klant">
          </div>

                    <div class="form-group">
          <input type="checkbox" class="" value='1' name="checkfactuur" id="checkfactuur">
          <label class="" id="checkfactuur" for="checkfactuur">Altijd factureren bij deze klant</label>
          </div>


        </div>
      </div>

      <button class="btn btn-success" type="submit">Opslaan</button>

    </div>

  </form>

  </div>
 

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script>
$(function () {
    $('#kredietplafon').hide();

    //show it when the checkbox is clicked
    $('#checkkrediet').on('click', function () {
        if ($(this).prop('checked')) {
            $('#kredietplafon').fadeIn();
        } else {
            $('#kredietplafon').hide();
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>