

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Gebruikersbeheer</h3>

      <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

      <?php if(session('error')): ?>
        <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('error')); ?>

        </div>
      <?php endif; ?>

     <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Alle gebruikers</div>
        <div class="card-body">

          <p><a class="btn btn-success" href="<?php echo e(route('newuser')); ?>">Nieuwe gebruiker</a></p>

          <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Naam</th>
                  <th>Email</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($user->name); ?></td>
                  <td><?php echo e($user->email); ?> </td>
                  <td><a class="btn btn-success" href="<?php echo e(route('bekijkuser', ['id' => $user->id])); ?>">Gebruiker bekijken</a></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>