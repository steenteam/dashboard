

<?php $__env->startSection('content'); ?>

 <div class="container-fluid">

      <h2>Voorraad beheer</h2>
     <div class="card">

        <div class="card-body">

<div class="bd-example bd-example-tabs">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active show" id="uitvoerend-tab" data-toggle="tab" href="#uitvoerend" role="tab" aria-controls="uitvoerend" aria-selected="true">Uitvoerend</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="voltooid-tab" data-toggle="tab" href="#voltooid" role="tab" aria-controls="voltooid" aria-selected="false">Voltooid</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade active show" id="uitvoerend" role="tabpanel" aria-labelledby="uitvoerend-tab">

                <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Tracking / Status</th>
                  <th>Gecontroleerd</th>
                  <th>Datum</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $inkopentodo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $todo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($todo->id); ?></td>
                  <td><?php if( $todo->getStatus($todo->tracking) == 1): ?> <span id="bezorgd">Bezorgd</span> <?php else: ?> <span id="onderweg">Onderweg</span> <?php endif; ?></td>
                  <td><?php if( $todo->gecontroleerd == 0): ?> <span id="onderweg"><a href="/voorraad/controleer/<?php echo e($todo->id); ?>"> Nog niet gecontroleerd </a></span> <?php else: ?> <span id="bezorgd">Gecontroleerd</span> <?php endif; ?></td>
                  <td><?php echo e($todo->created_at); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>



    </div>
    <div class="tab-pane fade" id="voltooid" role="tabpanel" aria-labelledby="voltooid-tab">

                <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Tracking / Status</th>
                  <th>Gecontroleerd</th>
                  <th>Datum</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $inkopenaf; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $af): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($af->id); ?></td>
                  <td><?php if( $af->getStatus($af->tracking) == 1): ?> <span id="bezorgd">Bezorgd</span> <?php else: ?> <span id="onderweg">Onderweg</span> <?php endif; ?></td>
                  <td><?php if( $af->gecontroleerd == 0): ?> <span id="onderweg">Nog niet gecontroleerd</span> <?php else: ?> <span id="bezorgd">Gecontroleerd</span> <?php endif; ?></td>
                  <td><?php echo e($af->created_at); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
    </div>
  </div>
</div>
 

</div>
</div>

<br />

<a href="<?php echo e(route('newinkoop')); ?>" class="btn btn-success">Nieuwe inkoop</a>


</div>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>