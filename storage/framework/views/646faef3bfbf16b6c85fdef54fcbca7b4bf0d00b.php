

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">


                 <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>


      <div class="row">
        <div class="col-lg-8">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="allebestellingen-tab" data-toggle="tab" href="#allebestellingen" role="tab" aria-controls="allebestellingen" aria-selected="true">Alle openstaande debiteuren</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="hold-tab" data-toggle="tab" href="#hold" role="tab" aria-controls="hold" aria-selected="false">Voorraad</a>
  </li>
</ul>

<br />

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="allebestellingen" role="tabpanel" aria-labelledby="allebestellingen-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle openstaande debiteuren</div>
        <div class="card-body">
  
          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam</th>
                  <th>Openstaand</th>
                </tr>
              </thead>
              <tbody>

                <?php $__currentLoopData = $klanten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $klant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <tr>
                  <td><a href="/bekijkklant/<?php echo e($klant->id); ?>"><?php echo e($klant->bedrijfsnaam); ?></a></td>
                  <td>€ <?php echo e($klant->openstaand($klant->id)); ?></td>
                </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>

              <tfoot>
                <tr>
                  <th>Totaal openstaand:</th>
                  <th>€ <?php echo e($totaalopenstaand); ?></th>
                </tr>
              </tfoot>
            </table>
          </div>

        </div>
      </div>

    </div>

  <div class="tab-pane fade" id="hold" role="tabpanel" aria-labelledby="hold-tab">

 <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Voorraad</div>
        <div class="card-body">

Totale waarde voorraad: € <?php echo e($totalevoorraad); ?> <br />
Totaal aantal producten op voorraad:  <?php echo e($totaleaantal); ?>


        </div>
      </div>


  </div>




</div>
      </div>
    </div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>