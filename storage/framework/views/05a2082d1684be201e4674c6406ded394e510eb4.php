
<?php $__env->startSection('content'); ?>
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.mobile.all.min.css" rel="stylesheet" type="text/css" />
<div id="app" class="container-fluid">
   <h3>Bestelling </h3>
   <form method="post" id="orderform" action="<?php echo e(action('OrderController@create')); ?>">
      <?php echo csrf_field(); ?>
      <div class="row">
         <div class="col-md-4">
            <div class="card">
               <div class="card-header">
                  Klantinformatie
               </div>
               <div class="card-body">
                  <div class="form-group">
                     <label for="klant">Kies een klant</label>
                     <select onchange="getKrediet(this.value)" id="klant" class="klantkies" name="klant" required>
                        <option value=""></option>
                        <?php $__currentLoopData = $klanten; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $klant): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($klant->id); ?>"><?php echo e($klant->bedrijfsnaam); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                  </div>

                  <div class="form-group">
                     <label for="status">Status bestelling</label>
                     <select id="status" onchange="checkvoltooid(this)" class="form-control" name="status">
                        <option value="0">Openstaand</option>
                        <option value="1">On hold</option>
                        <option value="2">Voltooid</option>
                     </select>
                  </div>
                  <div class="form-group" id="">
                     <label for="verzendkosten">Hoeveel verzendkosten worden er gerekend</label>
                     <input id="verzendkosten" name="verzendkosten" value="6.00" class="form-control">
                  </div>
                  <div id="verzendmethodegroup" class="form-group">
                     <label for="verzendmethode">Verzendmethode</label>
                     <select id="verzendmethode" class="form-control" name="verzendmethode">
                        
                        <option value="137">UPS Standard</option>
                        <option value="326">PostNL Fulfilment met handtekening 0-30KG</option>
                        <option value="320">PostNL Fulfilment abroad</option>
                        <option value="ophalen">Bestelling afhalen</option>
                     </select>
                  </div>

                                      <a class="" data-toggle="collapse" href="#extra" role="button" aria-expanded="false" aria-controls="extra">
    Extra instellingen laden..
  </a>

    <a class="pull-right" data-toggle="collapse" href="#notes" role="button" aria-expanded="false" aria-controls="notes">
    Notities
  </a>
            <div class="form-group" id="betaaldbedrag">
                     <label for="betaald">Hoeveel is er betaald</label>
                     <input id="betaald" name="betaald" class="form-control">
                  </div>
                  <div class="form-group" id="passwordbevestig">
                     <b>Bevestigingswachtwoord:</b> 
                     <input type="password" placeholder="Password" class="form-control" id="password">
                     <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password">
                  </div>
               </div>
        </div>

        <div class="collapse" id="extra">
              <div class="card card-body">
                  <div id="productenvoorraadgroup" style="margin-left: 25px;" class="form-group">
                     <input type="checkbox" name="voorraadaf" class="form-check-input" id="voorraadaf" checked>
                     <label for="voorraadaf">Producten niet van de voorraad afhalen</label>
                  </div>
                  <div id="pakbongroup" style="margin-left: 25px;" class="form-group">
                     <input type="checkbox" name="geenpakbon" class="form-check-input" id="geenpakbon">
                     <label for="geenpakbon">Geen pakbon printen</label>
                  </div>

                            <div class="form-group">
          <input type="checkbox"  value="1" class="" <?php if(is_null($klant->factureren)): ?> <?php else: ?> checked="checked" <?php endif; ?> name="checkfactuur" id="checkfactuur">
          <label class="" id="checkfactuur" for="checkfactuur">De bestelling direct factureren</label>
          </div>

                                    <div id="adresandersgroup" class="form-group">
                     <label for="anderadres">Naar een ander adres versturen</label>
                     <select id="" onchange="checkadres(this)" class="form-control" name="anderadres">
                        <option value="0">Nee</option>
                        <option value="2">Ja</option>
                     </select>
                  </div>

                </div>

</div>

                             <div style="display: none;" id="anderadres">
                            <div class="card">
                              <div class="card-body">
                     <div class="form-group">
                        <label for="adres">Adres</label>
                        <input name="adres" class="form-control" placeholder="Adres van de klant">
                     </div>
                     <div class="form-group">
                        <label for="adres">Huisnummer</label>
                        <input name="huisnummer" class="form-control" placeholder="Huisnummer van de klant">
                     </div>
                     <div class="form-group">
                        <label for="stad">Stad</label>
                        <input name="stad" class="form-control" placeholder="Stad van de klant">
                     </div>
                     <div class="form-group">
                        <label for="postcode">Postcode</label>
                        <input name="postcode" class="form-control" placeholder="Postcode van de klant">
                     </div>
           <div class="form-group">
            <label for="country">Land</label>
         <select id="country" class="form-control" name="country">
            <option value="Netherlands">Nederland</option>
            <option value="Belgie">België</option>
            <option value="France">Frankrijk</option>
            <option value="Duitsland">Duitsland</option>
      </select>
       </div>
                   </div>
                 </div>
                  </div>

            
            <div id="melding">
            </div>
                <div class="collapse" id="notes">
            <div class="card">
               <div class="card-header">
                  Notities
               </div>
               <div class="card-body">
                  <div class="form-group">
                     <textarea name="note" class="form-control" placeholder="Vul hier notities in met betrekking tot deze order."></textarea>
                  </div>
               </div>
            </div>
          </div>
            <br />
         </div>
         <div class="col-md-8">
          <div id="stockstock" class="card">
            <div class="card-body">
              <span id="productnaam"></span> : 
              <span id="stockhoeveelheid"></span>
            </div>
          </div>
            <div class="card">
               <div class="card-header">
                  Producten
               </div>
               <div class="card-body">
                  <table class="table table-hover" id="bestellingTable">
                     <thead>
                        <tr>
                           <th>Productnaam</th>
                           <th>Aantal</th>
                           <th>Prijs</th>
                           <th>Totaalprijs</th>
                           <th>X</th>
                        </tr>
                     </thead>
                     <tr class="bestellingrow bestellingRow">
                        <td>
                           <select class="product-select product required-entry" name="products[]" id="product">
                              <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option data-stock="<?php echo e($product->hoeveelheid); ?>" value="<?php echo e($product->id); ?>"><?php echo e($product->name); ?> </option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                        </td>
                        <td>
                           <div class="form-group">
                              <input autocomplete="off" name="aantallen[]" id="aantal" type="number" onChange="calculateTotaal()" placeholder="Aantal" class="required-entry small-box aantal form-control" required>
                           </div>
                           <span></span>
                        </td>
                        <td>
                           <div class="form-group">
                              <input autocomplete="off" onkeyup="this.value=this.value.replace(',','')" name="prijzen[]" id="prijs" <?php if(Auth::user()->id == '6' || Auth::user()->id == '7' || Auth::user()->id == '1' ): ?> <?php else: ?> min="0" <?php endif; ?> onChange="calculateTotaal()" value="1" type="text" placeholder="Prijs" class="required-entry prijs small-box form-control" required>
                           </div>
                        </td>
                        <td>
                           € <span value="0" class="rowtotal" id="totalrow">0.00</span>
                        </td>
                        <td><button onclick="calculateTotaal();" class="btn btn-danger btn-delete-row remove" type="button">X</button></td>
                     </tr>
                  </table>
                  <table>
                     <tr id="addButtonRow">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Totaal:</td>
                        <td>€ <span value="0" id="sumTotal">0.00</span></td>
                     </tr>
                  </table>
                  <div class="text-center">
                     <button type="button" class="btn center-block btn-success" onclick="test();">Extra product</button>
                  </div>
               </div>
            </div>
            <br />
            <button type="submit" id="neworder" name="new" class="btn btn-primary">Bestelling plaatsen</button>
         </div>
      </div>
   </form>
</div>
<style>
   .table.user-select-none {
   -webkit-touch-callout: none;
   -webkit-user-select: none;
   -khtml-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   .table-remove:first-child {
   display: none;
   }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
   // $(".remove").hide();
   
   
   function test()
   {
   
   
     $('#appendrow').appendTo("#bestellingTable");
   
     $('#bestellingTable').append(' <tr class="bestellingrow bestellingRow clonerow"><td><select class="product-select product required-entry" name="products[]" id="product"><?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> <option data-stock="<?php echo e($product->hoeveelheid); ?>" value="<?php echo e($product->id); ?>"><?php echo e($product->name); ?></option> <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> </select></td><td><div class="form-group"><input autocomplete="off" name="aantallen[]" id="aantal" type="number" max="1000" onChange="calculateTotaal()" placeholder="Aantal" class="required-entry small-box aantal form-control" required> </div><span></span></td>  <td><div class="form-group"> <input autocomplete="off" name="prijzen[]" <?php if(Auth::user()->id == '6' || Auth::user()->id == '7' || Auth::user()->id == '1' ): ?> <?php else: ?> min="0" <?php endif; ?> value="" id="prijs" onChange="calculateTotaal()" type="text" placeholder="Prijs" class="required-entry small-box prijs form-control" required> </div> </td> <td> € <span value="0" class="rowtotal" id="totalrow">0.00</span> </td> <td><button onclick="calculateTotaal();" class="btn btn-danger btn-delete-row remove" type="button">X</button></td></tr>');
   
        $('.product:last').selectize({
       sortField: 'text'
   });
   
   }
   
   




</script>
 <script src="<?php echo e(asset('js/bestelling.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>