

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Magazijn</h3>

      <div class="row">

<div class="card card-magazijn mb-3 col-sm-6">
      <div class="card-header">
        <i class="fa fa-table"></i>Voorraad</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Locatie</th>
                  <th>Aantal op voorraad</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($product->sku); ?></td>
                  <td><?php echo e($product->name); ?></td>
                  <td><a href="/edithetmagazijn/<?php echo e($product->id); ?>"><?php echo e($product->locatie); ?></a></td>
                  <td><?php echo e($product->getStock($product->id)); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>

<div class="card card-magazijn mb-3 col-sm-6">
      <div class="card-header">
        <i class="fa fa-table"></i>Magazijnkaart</div>
        <div class="card-body">

          <img src="https://picqer.com/images/blog/locatienummer-magazijn-voorbeeld-2@2x.png">


        </div>
        <div class="card-footer small text-muted"></div>
      </div>
      
    </div>

  </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>