

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">



  	<h3>Klant bekijken: <?php echo e($klant->voornaam); ?> <?php echo e($klant->achternaam); ?></h3>


      <a href="/editklant/<?php echo e($klant->id); ?>" class="btn btn-success">Klant aanpassen</a> &nbsp;

    <?php if($klant->hasAnything($klant->id)): ?>

                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                  Verwijderen
                </button>

                     <?php endif; ?>

      <button type="button" data-val="<?php echo e($klant->id); ?>" data-max="" class="btn pull-right btn-success" data-toggle="modal" data-target="#pakbonModal">Nieuwe pakbon</button>
      
      <p>

      <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

    </p>

      <br />

      <div class="row">

        <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Klantgegevens</div>
        <div class="card-body">

          <div class="row">
            <div class="col-sm-6">
              <table>
              <tr>
                <td><b> Volledige naam: </b></td>
                <td><?php echo e($klant->voornaam); ?> <?php echo e($klant->achternaam); ?></td>
              </tr>
              <tr>
                <td><b> Bedrijfsnaam: </b></td>
                <td><?php echo e($klant->bedrijfsnaam); ?></td>
              </tr>
              <tr>
                <td><b> Adres: </b></td>
                <td><?php echo e($klant->adres); ?> <?php echo e($klant->huisnummer); ?></td>
              </tr>
              <tr>
                <td><b> Stad: </b></td>
                <td><?php echo e($klant->stad); ?></td>
              </tr>
              <tr>
                <td><b> Postcode: </b></td>
                <td><?php echo e($klant->postcode); ?></td>
              </tr>
              <tr>
                <td><b> Telefoon: </b></td>
                <td><?php echo e($klant->telefoon); ?></td>
              </tr>
              <tr>
                <td><b> E-mail: </b></td>
                <td><a href="mailto: <?php echo e($klant->email); ?>"><?php echo e($klant->email); ?></a></td>
              </tr>

              </table>

            </div>
            <div class="col-sm-6">

            </div>
          </div>

        </div>
      </div>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('see credit')): ?>
            <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-credit-card"></i> Kredietplafon</div>
        <div class="card-body">

          <?php if( ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100 >= 100): ?>
           <div class="alert alert-danger" role="alert">
  Het limiet is bereikt. Er kan niet meer op krediet worden besteld.
</div>
          <?php else: ?>
          <?php if($klant->openstaand($klant->id) != 0): ?>
          <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: <?php echo e(($klant->openstaand($klant->id) / $klant->kredietplafon) * 100); ?>%;" aria-valuenow="<?php echo e(($klant->openstaand($klant->id) / $klant->kredietplafon) * 100); ?>" aria-valuemin="0" aria-valuemax="100"><?php echo e(($klant->openstaand($klant->id) / $klant->kredietplafon) * 100); ?>%</div>
          </div>
          <?php else: ?>
                    <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
          </div>
          <?php endif; ?>

          <?php endif; ?>

          <p>
          <b>Kredietplafon: </b>€ <?php echo e($klant->kredietplafon); ?> <br />
          <b>Openstaand: </b> € <?php echo e($klant->openstaand($klant->id)); ?> <br />
          <b>Bestedingsruimte: </b> € <?php echo e($klant->kredietplafon - $klant->openstaand($klant->id)); ?> 
          </p>


        </div>
      </div>
      <?php endif; ?>

      <div class="card">

          <div class="card-header">

            Geschiedenis

          </div>

          <div class="card-body">

            <table class="dataTable table">
              <thead>
                <th>Bedrag</th>
                <th>Datum</th>
                <th>Bekijken</th>
              </thead>
              <tbody>
                <?php $__currentLoopData = $done; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $verekening): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    
                <tr>
                  <td>€ <?php echo e($verekening->verekeningTotaal($verekening->id)); ?></td>
                  <td><?php echo e($verekening->created_at); ?></td>
                  <td><a href="/bekijkverekening/<?php echo e($verekening->id); ?>">Bekijk</a></td>
                </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $histo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    
                <tr>
                  <td>€ <?php echo e($histo->bedrag); ?></td>
                  <td><?php echo e($histo->created_at); ?></td>
                  <td><a href="/bestelling/<?php echo e($histo->order_id); ?>">Bekijk</a></td>
                </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

              </tbody>
            </table>

          </div>

      </div>

                  <div class="card">
            <div class="card-header">
              <i class="fa fa-bell-o"></i> Agenda <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modalAgenda">Nieuw item</button></div>
            <div class="list-group list-group-flush small">

             <?php $__currentLoopData = $agenda; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agenda): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <a class="list-group-item list-group-item-action" href="/deleteagenda/<?php echo e($agenda->id); ?>">
                <div class="media">
                  <div class="media-body">
                    <strong><?php echo e($agenda->content); ?></strong>.
                    <div class="text-muted smaller"><?php echo e($agenda->created_at->format('m/d/Y h:i:s')); ?></div>
                    <div onclass="delete-agenda">Verwijderen</div>
                  </div>
                </div>
              </a>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </div>
          </div>

      <br />

    </div>
            <div id="klantorders" class="col-sm-6">

    <?php $__currentLoopData = $verekeningen; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $verekening): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <div class="card">
                <div class="card-header">
                  <span style="font-weight: 600;">Verekening</span> van <?php echo e($klant->bedrijfsnaam); ?> - <?php echo e($verekening->created_at->format('m/d/Y h:i:s')); ?>

                  <a class="btn btn-success btn-small-height pull-right" href="/bekijkverekening/<?php echo e($verekening->id); ?>">Bekijk verekening</a> &nbsp;
                  <?php if($verekening->prijs === 0): ?>
                  <a class="btn btn-danger" href="/deletepakbon/<?php echo e($verekening->id); ?>">Verwijderen</a>  &nbsp;
                  <?php endif; ?>
                </div>
                <?php if($verekening->orders()->get()->isNotEmpty()): ?>

                <div class="card-body">

                 <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <tr>
                    <th>Ordernummer</th>
                    <th>Totale prijs</th>
                    <th>Bekijk order</th>
                  </tr>             

                <?php $__currentLoopData = $verekening->orders()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                    <td><?php echo e($order->id); ?></td>
                    <td>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></td>
                    <td><a href="<?php echo e(route('bestelling', ['id' => $order->id])); ?>">Bekijk order</a>
                  </tr>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               </table>  

             </div>

                   <?php endif; ?>
                <div class="card-footer">
                  <strong>Status:</strong> <?php echo e($verekening->status($verekening->id)); ?> ( €<?php echo e($verekening->prijs); ?> )
                <button type="button" data-val="<?php echo e($verekening->id); ?>" data-max="<?php echo e($verekening->prijs); ?>" class="btn pull-right btn-small-height btn-success" data-toggle="modal" data-target="#voltooiModal">
  Voltooien
</button>
                </div>
              </div> <br />
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

    <?php if($orders->isEmpty()): ?>

    <div class="alert alert-primary" role="alert">
  Er zijn geen actieve bestellingen gevonden.
</div>

    <?php endif; ?>



    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $bestelling): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

              <div class="card">
                <div class="card-header">
                  <span style="font-weight: 700">Bestelling</span> van <?php echo e($bestelling->klant()->first()->bedrijfsnaam); ?> - <?php echo e($bestelling->created_at->format('m/d/Y h:i:s')); ?>

                  <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('edit order')): ?>
                  <button onclick="edit(<?php echo e($bestelling->id); ?>);" class="pull-right btnorder btnorder<?php echo e($bestelling->id); ?>  btn btn-small-height btn-success">Order aanpassen</button>
                  <?php endif; ?>
                </div>
                <div class="card-body">

                  <div class="edit edit<?php echo e($bestelling->id); ?>">
                    <div id="external<?php echo e($bestelling->id); ?>"></div>
  
                  </div>

     


                           

 <table class="dataTable table table-bordered order order<?php echo e($bestelling->id); ?>" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Aantal</th>
                  <th>Stukprijs</tth>
                  <th>Totaal</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Verzendkosten:</th>
                  <th>€ <?php echo e($bestelling->verzendkosten); ?></th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Totaal:</th>
                  <th>€ <?php echo e($bestelling->order_total($bestelling->id) + $bestelling->verzendkosten); ?></th>
                </tr>
              </tfoot>
              <tbody>
               <?php $__currentLoopData = $bestelling->products()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($product->sku); ?></td>
                  <td><?php echo e($product->name); ?></td>
                  <td><?php echo e($bestelling->aantal($product->id, $bestelling->id)); ?></td>
                  <td>€ <?php echo e($bestelling->stukprijs($product->id, $bestelling->id)); ?></td>
                  <td>€ <?php echo e($bestelling->prijs($product->id, $bestelling->id)); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>

            <?php if(!empty($bestelling->note)): ?>

            <div id="noteinklant">

            <?php echo e($bestelling->note); ?>


          </div>

          <?php endif; ?>


                </div>
                <div class="card-footer">
                  <strong>Status:</strong> <?php echo e($bestelling->status($bestelling->id)); ?> ( €<?php echo e($bestelling->totalprijs($product->id, $bestelling->id) + $bestelling->verzendkosten - $bestelling->betaald); ?> ) <?php if(!empty($bestelling->betaald)): ?> - Reeds betaald: € <?php endif; ?> <?php echo e($bestelling->betaald); ?>

                  <span class="float-right">

                    <?php if($bestelling->getTracking($bestelling->id) != 'null'): ?>
                     <a href="<?php echo e($bestelling->getTracking($bestelling->id)); ?>" target="_blank" class="btn btn-small-height btn-warning">Traceren</a> &nbsp;

                     <?php endif; ?>

                  <?php if(is_null($bestelling->betaald)): ?>
                     <a class="btn btn-small-height  btn-warning" href="/factureren/<?php echo e($bestelling->id); ?>">Factureren</a>
                     <?php endif; ?>

                    <a href="<?php echo e(route('bestelling', ['id' => $bestelling->id])); ?>" class="btn btn-small-height btn-success">Bekijk bestelling</a>  &nbsp;

                                    <button type="button" data-val="<?php echo e($bestelling->id); ?>" data-max="<?php echo e($bestelling->totalprijs($product->id, $bestelling->id) - $bestelling->betaald); ?>" class="btn btn-small-height pull-right btn-success" data-toggle="modal" data-target="#voltooiOrderModal">
  Voltooien
</button>
                  </span>
                </div>
              </div>

              <br />

              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


              <?php if($orders->isNotEmpty() || $verekeningen->isNotEmpty() ): ?>
              <div>
                <br />
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#verekenModal">
                  Deze bestellingen verekenen
                </button> 

                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#selectModal">
                  Selecteren
                </button>
              </div> <br />
              <?php endif; ?>




    </div>

  </form>

  </div>

  <!-- Modal -->
<div class="modal fade" id="selectModal" tabindex="-2" role="dialog" aria-labelledby="selectModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Selecteer orders</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form method="post" action="<?php echo e(action('VerekeningenController@voltooiSelected', ['som' => $verekensum, 'id' => $klant->id])); ?>">
          <?php echo csrf_field(); ?>

                           <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <tr>
                    <th>Check</th>
                    <th>Datum</th>
                    <th>Bedrag</th>
                  </tr>             
                    <?php $__currentLoopData = $ordersclean; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                  <tr>
                    <td><input id="verekeningselect" class="verekeningselect big-checkbox" name="verekeningselect[]" data-bedrag="<?php echo e($order->order_total($order->id) + $order->verzendkosten); ?>" type="checkbox" value="<?php echo e($order->id); ?>"></td>
                    <td><?php echo e($order->created_at->format('m/d/Y h:i:s')); ?></td>
                    <td>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></td>
                  </tr>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
               <tr>
                <td></td>
                <td>Totaal:</td>
                <td>€ <span class="totalsumselect"></span></td>
               </tr>
               </table>
      
          <input style="visibility: hidden" name="verekeningid" class="verekeningid" id="verekeningid" value="">
          <div class="form-group"> <br /><p>
            <input class="form-control" type="text" name="verekengetal" onkeyup="this.value=this.value.replace(',',''); this.value=this.value.replace('/','');" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid"><p></p><br>
                        <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-2" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Klant verwijderen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo e(action('KlantController@deleteKlant', ['id' => $klant->id])); ?>"> <br />
          <?php echo csrf_field(); ?>
          <input style="visibility: hidden" name="verekeningid" class="verekeningid" id="verekeningid" value="">
          <div class="form-group">
        <input type="password" placeholder="Password" class="form-control" id="password1" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password1">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Verwijder nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="verekenModal" tabindex="-1" role="dialog" aria-labelledby="verekenModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="verekenModalTitle">Bestellingen verekenen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Sluiten">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <b>Totaal openstaand: € <?php echo e($verekensum); ?> </b> <br /> <br />
        <form method="post" action="<?php echo e(action('VerekeningenController@verekenen', ['som' => $verekensum, 'id' => $klant->id])); ?>">
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <input class="form-control" type="text" max="<?php echo e($verekensum); ?>" name="verekengetal" placeholder="Hoeveel moet er worden verekend">
          </div>
                      <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password2" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password2">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Vereken nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="voltooiModal" tabindex="-2" role="dialog" aria-labelledby="voltooiModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verekening voltooien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo e(action('VerekeningenController@voltooiVerekening', ['som' => $verekensum, 'id' => $klant->id])); ?>"> <br />
          <?php echo csrf_field(); ?>
          <input style="visibility: hidden" name="verekeningid"  class="verekeningid" id="verekeningid" value="">
          <div class="form-group">
            <input class="form-control" type="text" onkeyup="this.value=this.value.replace(',','.')" max="0" name="verekengetal" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid"> <br />
                        <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password3" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password3">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="voltooiOrderModal" tabindex="-2" role="dialog" aria-labelledby="voltooiOrderModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verekening voltooien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo e(action('VerekeningenController@voltooiOrder', ['som' => $verekensum, 'id' => $klant->id])); ?>">
          <?php echo csrf_field(); ?>
          <input style="visibility: hidden" name="orderid" class="orderid" id="orderid" value="">
            <input style="visibility: hidden" name="klantid" class="klantid" id="klantid" value="<?php echo e($klant->id); ?>">
          <div class="form-group">
            <input class="form-control" type="text" max="0" onkeyup="this.value=this.value.replace(',','.'); this.value=this.value.replace('/','');" name="verekengetal" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid" required>
            <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password4" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password4">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

</div>

      <!-- Modal -->
  <div class="modal fade" id="modalAgenda" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="<?php echo e(action('KlantController@newagendaklant')); ?>" class="" method="post">

            <?php echo csrf_field(); ?>

            <div style="display: none;" class="form-group">
              <input class="form-control" name="klantid" value="<?php echo e($klant->id); ?>" type="text">
            </div>

            <div class="form-group">
              <input class="form-control" name="content" placeholder="Bericht" type="text">
            </div>

            <div class="form-group">
                <input class="form-control" name="tijd" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
            </div>

            <button class="btn btn-default" type="submit">Opslaan</button>

          </form>

         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Sluiten</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="pakbonModal" tabindex="-2" role="dialog" aria-labelledby="pakbonModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pakbon toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo e(action('VerekeningenController@newPakbon', ['id' => $klant->id])); ?>">
          <?php echo csrf_field(); ?>
          <div class="form-group">
            <input class="form-control" type="text" name="bedrag" id="bedrag" class="bedrag" placeholder="Bedrag pakbon"> <br />
                      <input class="form-control" type="date" max="0" name="created_at" id="created_at" class="created_at">

          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Toevoegen</button>
        </form>
      </div>
    </div>
  </div>
</div>
 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>

<script>

$('#voltooiOrderModal').on('show.bs.modal', function (event) {
  var max = $(event.relatedTarget).data('max');
  var myVal = $(event.relatedTarget).data('val');
  $(this).find(".orderid").val(myVal);
  $(this).find("#voltooigetal").attr('max', max);

});

</script>

<script>

$('#voltooiModal').on('show.bs.modal', function (event) {
  var max = $(event.relatedTarget).data('max');
  var myVal = $(event.relatedTarget).data('val');
  $(this).find(".verekeningid").val(myVal);

  $(this).find("#voltooigetal").attr('max', max);

});

</script>
<script>
  

  $('.')

  $('.edit').each(function() {
    $(this).hide();
  });

function edit(id)
{

  $('.edit').each(function() {
    $(this).hide();
  });

  // $('.dataTables_wrapper').each(function() {
  //  $(this).parents('div.dataTables_wrapper').first().show();
  //  $(this).show();
  // });

  $('.order'+id+'').hide();
  $('.btnorder'+id+'').hide();
  $('.edit'+id+'').show();
  $('#external'+id+'').load("/editorder/"+id+"");
  $('.btnorder').hide();

}

</script>

<script>
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password1")
  , confirm_password = document.getElementById("confirm_password1");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password2")
  , confirm_password = document.getElementById("confirm_password2");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password3")
  , confirm_password = document.getElementById("confirm_password3");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password4")
  , confirm_password = document.getElementById("confirm_password4");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>

  var sum = 0;

  $('.verekeningselect').on('change', function(){ // on change of state
   if(this.checked) // if changed state is "CHECKED"
    {

      sum = parseFloat(sum) + parseFloat($(this).attr("data-bedrag"));

        // do the magic here
    } else {

       sum = parseFloat(sum) - parseFloat($(this).attr("data-bedrag"));

    }

    $('.totalsumselect').text('');

    $('.totalsumselect').text(parseFloat(sum).toFixed(2));
});

     
     $('body').on('change', '.product', function () {
   
           $.ajaxSetup({
               headers:
               { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
           });
   
     $.ajax
   ({
     type: "POST",
     url: "/getprice/"+$(this).val()+"",
     async: false,
   
     success: function(response)
     {
        setPrice(response);
   
            calculateTotaal();
   
     }
   });
   
   
   // $(this).closest('td').find('.prijs').attr('value', price);
   $(this).closest('td').nextAll(':lt(2)').slice(1).find('div > input').val(price);

   <?php if(Auth::user()->id != 6 || Auth::user()->id != 7): ?>

   $(this).closest('td').nextAll(':lt(2)').slice(1).find('div > input').attr('min', price);
   
   <?php endif; ?>

   });

</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>