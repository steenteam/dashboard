

<?php $__env->startSection('content'); ?>

    <div class="container-fluid">

    	<h3>Alle bestellingen</h3>

            <?php if(session('status')): ?>
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <?php echo e(session('status')); ?>

        </div>
      <?php endif; ?>

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Bestellingen</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><a href="<?php echo e(route('bekijkklant', ['id' => $order->klant()->first()->id])); ?>"><?php echo e($order->klant()->first()->bedrijfsnaam); ?></a></td>
                  <td><?php echo e($order->status($order->id)); ?></td>
                  <td>€ <?php echo e($order->order_total($order->id) + $order->verzendkosten); ?></td>
                  <td><?php echo e($order->created_at->format('m/d/Y h:i:s')); ?></td>
                  <?php if($order->status === '1'): ?>
                  <td><a href="<?php echo e(route('checkonhold', ['id' => $order->id])); ?>" class="btn btn-success">Bekijken</a></td>
                  <?php else: ?>
                  <td><a href="<?php echo e(route('bestelling', ['id' => $order->id])); ?>" class="btn btn-success">Bekijken</a></td>
                  <?php endif; ?>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>