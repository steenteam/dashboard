<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {

        // Login tests

        $this->get('/')
             ->assertSee('Redirecting to http://localhost/login');

        $this->get('/login')
             ->assertSee('Sorry, but the page you were trying to view does not exist.');

    }
}
