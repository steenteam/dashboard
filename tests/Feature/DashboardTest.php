<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;

class DashboardTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

    	$user = User::find(1);

        $response = $this->actingAs($user)
                         ->withSession(['foo' => 'bar'])
                         ->get('/')
                         ->assertSee('Laatste bestellingen')
                         ->assertSee('Zoek naar klanten..');

    }
}
