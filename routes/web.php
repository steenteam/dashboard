<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('uitloggen');

Route::get('/', 'DashboardController@index')->name('dashboard');

// Product routes

Route::get('/product', 'ProductController@index')->name('product');

Route::get('/newproduct', 'ProductController@new')->name('productnew');

Route::post('/newproduct', 'ProductController@create')->name('productcreate');

Route::post('/deleteklant', 'KlantController@deleteKlant')->name('deleteKlant');

Route::get('/deleteproduct/{id}', 'ProductController@delete')->name('productdelete');

Route::get('/editproduct/{id}', 'ProductController@edit')->name('productedit');

Route::post('/editproduct/{id}', 'ProductController@update')->name('productupdate');

Route::get('/product/find', 'ProductController@searchProducts');

Route::get('/getstock/{id}', 'ProductController@getStock');

// Categorie routes


Route::get('/categorieen', 'CategorieController@index')->name('categorieen');

Route::get('/newcategorie', 'CategorieController@new')->name('categorienew');

Route::post('/newcategorie', 'CategorieController@create')->name('categoriecreate');

Route::get('/deletecategorie/{id}', 'CategorieController@delete')->name('categoriedelete');

Route::get('/editcategorie/{id}', 'CategorieController@edit')->name('categorieedit');

Route::post('/editcategorie/{id}', 'CategorieController@update')->name('categorieupdate');

// User beheer routes

Route::get('/userbeheer', 'BeheerUserController@index')->name('userbeheer');

Route::get('/newuser', 'BeheerUserController@new')->name('newuser');

Route::post('/newuser', 'BeheerUserController@create')->name('createuser');

Route::get('/deleteuser/{id}', 'BeheerUserController@delete')->name('deleteuser');

Route::get('/bekijkuser/{id}', 'BeheerUserController@show')->name('bekijkuser');

Route::post('/roles/save/{id}', 'BeheerUserController@saveRoles')->name('saveroles');

Route::post('/permissions/save/{id}', 'BeheerUserController@savePermissions')->name('savepermissions');

// Klanten

Route::get('/klanten', 'KlantController@index')->name('klanten');

Route::get('/newklant', 'KlantController@new')->name('newklant');

Route::post('/newklant', 'KlantController@create')->name('createklant');

Route::get('/bekijkklant/{id}', 'KlantController@show')->name('bekijkklant');

Route::post('/getkrediet/{id}', 'KlantController@isKredietWaardig')->name('getKrediet');

// Bestellingen

Route::get('/bestellingen', 'OrderController@index')->name('bestellingen');

Route::get('/bestelling/{id}', 'OrderController@show')->name('bestelling');

Route::get('/newbestelling', 'OrderController@new')->name('newbestelling');

Route::get('/editorder/{id}', 'OrderController@inlineedit')->name('editorder');

Route::post('/newbestelling', 'OrderController@create')->name('createbestelling');

Route::get('/getrowdata/{id}', 'OrderController@getrowdata')->name('getrowdata');

Route::post('/update/{id}', 'OrderController@update')->name('updateorder');
 
// Verekenen

Route::post('/verekenen/{som}/{id}', 'VerekeningenController@verekenen')->name('verekenen');

Route::get('/autocomplete', 'ProductController@searchProduct')->name('autocomplete');

Route::get('/errortest', 'DashboardController@testerror')->name('testerror');

Route::post('/voltooiverekening', 'VerekeningenController@voltooiVerekening')->name('voltooiverekening');

Route::post('/voltooiselected', 'VerekeningenController@voltooiSelected')->name('voltooiselected');

Route::post('/voltooiorder', 'VerekeningenController@voltooiOrder')->name('voltooiorder');

// Voorraad

Route::get('/voorraad/exporteren', 'VoorraadController@exportPagina')->name('exporteren');

Route::post('/voorraad/export', 'VoorraadController@export')->name('export');

Route::get('/voorraad/beheer', 'VoorraadController@voorraadBeheer')->name('vorraadbeheer');

Route::get('/voorraad/new', 'VoorraadController@newInkoop')->name('newinkoop');

Route::post('/voorraad/new', 'VoorraadController@create')->name('create');

Route::get('/voorraad/controleer/{id}', 'VoorraadController@controleer')->name('controleer');
Route::post('/voorraad/controleer/{id}', 'VoorraadController@gecontroleerd')->name('gecontroleerd');

Route::get('search/autocomplete', 'KlantController@autocomplete');

Route::get('/main-search-autocomplete', function(){
    return json_encode(DB::table('klants')->get()->all());
});

Route::post('/zoekklant', 'KlantController@zoekklant')->name('zoekklant');

// WooCommece

Route::get('/woocommerce', 'WoocommerceController@index')->name('wootest');

Route::post('/newagenda', 'DashboardController@agendanew')->name('agendanew');

Route::post('/newagendaklant', 'KlantController@newagendaklant')->name('newagendaklant');

Route::get('/test', 'DashboardController@test')->name('test');

Route::get('/prodimport', 'DashboardController@productsimport')->name('prods');
Route::get('/prodimport2', 'DashboardController@productsimport2')->name('prods');

Route::get('/voorraadimport', 'DashboardController@voorraadimport')->name('voorraadimport');

Route::get('/magazijn', 'ProductController@magazijn')->name('magazijn');


Route::get('/edithetmagazijn/{id}', 'ProductController@showeditMagazijn')->name('showeditmagazijn');

Route::post('/editmagazijn', 'ProductController@editMagazijn')->name('editmagazijn');

Route::get('/bekijkbonnen', 'DashboardController@importeerpakbonnen')->name('importpakbon');

Route::get('/deletekalender/{id}', 'KalenderController@delete')->name('deletekalender');
Route::get('/deleteagenda/{id}', 'KlantController@deleteagenda')->name('deleteagenda');

Route::get('/deletegeschiedenis/{id}', 'KlantController@delete')->name('deletegeschiedenis');


Route::post('/newpakbon/{id}', 'VerekeningenController@newPakbon')->name('newpakbon');

Route::get('/deletepakbon/{id}', 'VerekeningenController@deletepakbon')->name('deletepakbon');

Route::get('/testpakbon', 'DashboardController@testPakbon')->name('testpakbon');

Route::get('/testprint', 'DashboardController@testprint')->name('testprint');

Route::get('/editklant/{id}', 'KlantController@edit')->name('editklant');

Route::post('/editklant/{id}', 'KlantController@modify')->name('modifyklant');

Route::post('/getprice/{id}', 'ProductController@getprice')->name('getprice');

Route::post('/getinkoop/{id}', 'ProductController@getinkoop')->name('getinkoop');

Route::get('/getinkoop/{id}', 'ProductController@getinkoop')->name('getinkoop');

Route::get('/bekijkverekening/{id}', 'VerekeningenController@bekijkVerekening')->name('bekijkverekening');

Route::get('/printpakbon/{id}', 'OrderController@printPakbon')->name('printpakbon');

Route::get('/checkonhold/{id}', 'OrderController@checkonhold')->name('checkonhold');

Route::post('/onholdupdate/{id}', 'OrderController@onholdupdate')->name('onholdupdate');

Route::get('/check/auth', 'LoggedController@logged')->name('loggedin');

Route::get('/statistics', 'DashboardController@showStatistics')->name('statistieken');

Route::get('/editinkoop/{id}', 'ProductController@editInkoopshow');

Route::post('/editinkoop/{id}', 'ProductController@editInkoop');

Route::post('stockupdate', 'StockUpdateController@newOrder');

Route::get('/facturen', 'FactuurController@index')->name('facturen');

Route::get('/voltooifactuur/{id}', 'FactuurController@voltooi')->name('voltooifactuur');

Route::get('/factureren/{id}', 'FactuurController@factureren')->name('factureren');

Route::get('/cancel/{id}', 'OrderController@cancel')->name('cancel');

Route::get('/cancelonhold/{id}', 'OrderController@cancelOnHold');