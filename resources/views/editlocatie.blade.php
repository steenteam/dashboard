@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Locatie bewerken</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="{{ action('ProductController@editMagazijn') }}">

    @csrf 

    <input name="productid" value="{{ $locatie->id }}" style="visibility: hidden;">

    <div class="form-group">
      <label for="name">Locatie</label>
      <input name="locatie" placeholder="Aantal op voorraad" value="{{ $locatie->locatie }}" class="form-control" required>
    </div>


    <div class="form-group">
      <button class="btn btn-success" type="submit">Aanpassen</button>
    </div>


  </form>
  

</div>
@endsection