  <!-- Navigation a -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="{{ route('dashboard') }}">Whiterose </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <br />
        <a href="/newbestelling" class="btn-new-bestelling btn btn-success">Nieuwe bestelling</a>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>


        @if(Auth::user()->id == 7 || Auth::user()->id = 1)

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('magazijn') }}">
            <i class="fa fa-fw fa-building"></i>
            <span class="nav-link-text">Magazijn</span>
          </a>
        </li>

          <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="{{ route('facturen') }}">
            <i class="fa fa-fw fa-credit-card"></i>
            <span class="nav-link-text">Factureren</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <a class="nav-link" href="{{ route('statistieken') }}">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Statistieken</span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Voorraad">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#VoorraadPagina" data-parent="#VoorraadPagina">
            <i class="fa fa-fw fa-archive"></i>
            <span class="nav-link-text">Inkoop</span>
          </a>
          <ul class="sidenav-second-level collapse" id="VoorraadPagina">
            <li>
              <a href="{{ route('vorraadbeheer') }}">Inkoopbeheer</a>
            </li>
            <li>
              <a href="{{ route('exporteren') }}">Voorraad analyse</a>
            </li>
          </ul>
        </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="bestelling">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#bestellingPagina" data-parent="#bestellingPagina">
            <i class="fa fa-fw fa-folder-open"></i>
            <span class="nav-link-text">Bestellingen</span>
          </a>
          <ul class="sidenav-second-level collapse" id="bestellingPagina">
            <li>
              <a href="{{ route('bestellingen') }}">Alle bestellingen</a>
            </li>
            @can('create order')
            <li>
              <a href="{{ route('newbestelling') }}">Nieuwe bestelling</a>
            </li>
            @endcan
          </ul>
        </li>

                <li class="nav-item" data-toggle="tooltip" data-placement="right" title="userbeheer">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#userbeheerPagina" data-parent="#userbeheerPagina">
            <i class="fa fa-fw fa-users"></i>
            <span class="nav-link-text">Gebruikers</span>
          </a>
          <ul class="sidenav-second-level collapse" id="userbeheerPagina">
            <li>
              <a href="{{ route('userbeheer') }}">Alle gebruikers</a>
            </li>
            <li>
              <a href="{{ route('newuser') }}">Nieuwe gebruiker</a>
            </li>
          </ul>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Product">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#productPagina" data-parent="#productPagina">
            <i class="fa fa-fw fa-product-hunt"></i>
            <span class="nav-link-text">Product</span>
          </a>
          <ul class="sidenav-second-level collapse" id="productPagina">
            <li>
              <a href="{{ route('product') }}">Product / Producten</a>
            </li>
            <li>
              <a href="{{ route('categorieen') }}">Product Categoriën</a>
            </li>
          </ul>
        </li>

        @endif



         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="klanten">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#klantenPagina" data-parent="#klantenPagina">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Klanten</span>
          </a>
          <ul class="sidenav-second-level collapse" id="klantenPagina">

        @if(Auth::user()->id == 7)
            <li>
              <a href="{{ route('klanten') }}">Alle klanten</a>
            </li>
            @endif
             <li>
              <a href="{{ route('newklant') }}">Nieuwe klant</a>
            </li>
          </ul>
        </li>

         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="klanten">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Uitloggen</a>
        </li>

{{-- 
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="bestelling">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#bestellingPagina" data-parent="#bestellingPagina">
            <i class="fa fa-fw fa-folder-open"></i>
            <span class="nav-link-text">RMA's</span>
          </a>
          <ul class="sidenav-second-level collapse" id="bestellingPagina">
            <li>
              <a href="{{ route('bestellingen') }}">Alle RMA's</a>
            </li>
            @can('create order')
            <li>
              <a href="{{ route('newbestelling') }}">Nieuwe RMA</a>
            </li>
            @endcan
          </ul>
        </li> --}}
      </ul>


      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">

        <li class="nav-item">


                    <form method="post" action="/zoekklant" id="formsearch" class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              @csrf
              <select onchange="this.form.submit()" name="q" class="search">
                <option value=""></option>
                @foreach($klanten as $klant)
                <option value="{{ $klant->id }}">{{ $klant->bedrijfsnaam }}</option>
                @endforeach
              </select>
                            <span class="zoek-knop input-group-append">
                <button class="btn btn-primary" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
      </ul>
    </div>
  </nav>