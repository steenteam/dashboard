<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Dashboard</title>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">

  <!-- Navigatie inladen -->
  @include('layout.nav') 

  <div class="content-wrapper">

    <!-- Content inladen -->
    @yield('content')

    <!-- Footer inladen -->
    @include('layout.footer')

    <!-- Logout modal inladen -->
    @include('modals.logout')

</body>

    <!-- JavaScript laden -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
$( document ).ready(function() {
   $('.search').selectize({
    onEnterKeypress: function(el) {
        jQuery('#searchform').submit();
    },
    placeholder: 'Zoek naar klanten..',
    sortField: 'text',
});
 });
    </script>

    <script>

    function getLogged()
    {

            $.ajax({
     type: 'GET',
     url: '/check/auth',
     data: "",
     async: false
   }).done(function(response) {
       if(response == 1) {
              window.location.replace("/login");
       }
     });

    }

    setInterval(function(){
    getLogged() // this will run after every 5 seconds
}, 5000);


    </script>
    
    @yield('js')
</html>
