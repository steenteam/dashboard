@extends('layout.app')

@section('content')

    <div class="container-fluid">

      <p><a class="btn btn-success" href="{{ route('newbestelling') }}">Nieuwe RMA</a></p>

    	<h3>Alle RMA's</h3>

            @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Bestellingen</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Ordernummer</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Bedrijfsnaam</th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <td>Datum</th>
                  <th>Ordernummer</th>
                  <th>Bekijken</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($retours as $retour)
                <tr>
                  <td><a href="{{ route('bekijkklant', ['id' => $retour->klant()->first()->id]) }}">{{ $retour->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>{{ $retour->status($retour->id) }}</td>
                  <td>{{ $retour->retour_total($retour->id) }}</td>
                  <td>{{ $retour->created_at }}</td>
                  <td>#{{ $retour->retourcode }}</td>
                  <td><a href="{{ route('bestelling', ['id' => $retour->id]) }}" class="btn btn-success">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>

@endsection