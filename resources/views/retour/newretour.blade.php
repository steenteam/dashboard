@extends('layout.app')

@section('content')
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.mobile.all.min.css" rel="stylesheet" type="text/css" />

    <div id="app" class="container-fluid">

    	<h3>Bestelling </h3>

      <form method="post" action="{{ action('OrderController@create') }}">

      @csrf
      <div class="row">
        <div class="col-md-4">

          <div class="card">
            <div class="card-header">
              Klantinformatie
            </div>
            <div class="card-body">
          <div class="form-group">
            <label for="klant">Kies een klant</label>
            <select onchange="getKrediet(this.value)" id="klant" class="form-control" name="klant">
              @foreach($klanten as $klant)
              <option value="{{ $klant->id }}">{{ $klant->bedrijfsnaam }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="status">Status bestelling</label>
            <select id="status" onchange="checkvoltooid(this)" class="form-control" name="status">
              <option value="0">Openstaand</option>
              <option value="1">On hold</option>
              <option value="2">Voltooid</option>
            </select>
          </div>

          <div class="form-group">
            <label for="verzendmethode">Verzendmethode</label>
            <select id="verzendmethode" class="form-control" name="verzendmethode">
              @foreach($shipping_methods as $shipping_method)
              <option value="{{ $shipping_method['id'] }}">{{ $shipping_method['name'] }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group" id="betaaldbedrag">
            <label for="betaald">Hoeveel is er betaald</label>
            <input id="betaald" name="betaald" class="form-control">
          </div>
          </div>
        </div><br />
        <div id="melding">
        </div>

        <div class="card">
          <div class="card-header">
            Notities
          </div>
          <div class="card-body">
            <div class="form-group">
            <textarea name="note" class="form-control" placeholder="Vul hier notities in met betrekking tot deze order."></textarea>
          </div>

          </div>
        </div>


        <br />

        </div>
        <div class="col-md-8">
          <div class="card">
            <div class="card-header">
              Producten
            </div>
            <div class="card-body">

            <table class="table table-hover" id="bestellingTable">
                    <thead>
                        <tr>
                            <th>Productnaam</th>
                            <th>Aantal</th>
                            <th>Prijs</th>
                            <th>Totaalprijs</th>
                            <th>Verwijderen</th>
                        </tr>
                    </thead>
                    <tr class="bestellingrow bestellingRow">
                        <td>

                          <select class="product-select product required-entry" name="products[]" id="product">

  @foreach($products as $product)
  <option data-stock="{{ $product->hoeveelheid }}" value="{{ $product->id }}">{{ $product->name }} <strong>({{ $product->hoeveelheid }})</strong></option>
  @endforeach

                         </select>
                        
                        </td>
                        <td>
                                                    <div class="form-group">
                          <input autocomplete="off" name="aantallen[]" id="aantal" type="number" max="5" onChange="calculateTotaal()" placeholder="Aantal" class="required-entry small-box aantal form-control">
                        </div>
                         <span></span>
                        </td>
                        <td>                          <div class="form-group">
                          <input autocomplete="off" name="prijzen[]" id="prijs" onChange="calculateTotaal()" type="number" placeholder="Prijs" class="required-entry small-box form-control">
                        </div>
                        </td>
                        <td>
                          € <span value="0" class="rowtotal" id="totalrow">0</span>
                        </td>
                        <td><button class="btn btn-danger btn-delete-row remove" type="button">Verwijderen</button></td>
                    </tr>
                    <tr id="addButtonRow">
                        <td><button class="btn btn-large btn-success add" type="button">Extra product</button></td>
                        <td></td>
                        <td></td>
                        <td>Totaal:</td>
                        <td>€ <span value="0" id="sumTotal">0</span></td>
                    </tr>
            </table>


            </div>
          </div>
          <br />

          <button type="submit" name="new" class="btn btn-primary">Bestelling plaatsen</button>

  
        </div>
      </div>

    </form>
    </div>

    <style>
.table.user-select-none {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.table-remove:first-child {
display: none;
}
  </style>



@endsection

@section('js')

<script>

$(".remove").hide();



make();

/* Variables */
var p = $("#bestellings").val();
var row = $(".bestellingRow");

/* Functions */
function getP(){
  p = $("#bestellings").val();
}

function addRow() {
  destroy();
  row.clone(true, true).appendTo("#bestellingTable");
  make();
}

function removeRow(button) {
  button.closest("tr").remove();
  lessTotal(button.closest("tr").find('#totalrow').val());
}
/* Doc ready */
$(".add").on('click', function () {
  getP();
  if($("#bestellingTable tr").length < 17) {
    addRow();
    var i = Number(p)+1;
    $("#bestellings").val(i);
  }
  $(this).closest("tr").appendTo("#bestellingTable");
  if ($("#bestellingTable tr").length === 3) {
    $(".remove").hide();
  } else {
    $(".remove").show();
  }
});
$(".remove").on('click', function () {
  getP();
  if($("#bestellingTable tr").length === 3) {
    //alert("Can't remove row.");
    $(".remove").hide();
  } else if($("#bestellingTable tr").length - 1 ==3) {
    $(".remove").hide();
    removeRow($(this));
    var i = Number(p)-1;
    $("#bestellings").val(i);
  } else {
    removeRow($(this));
    var i = Number(p)-1;
    $("#bestellings").val(i);
  }
});
$("#bestellings").change(function () {
  var i = 0;
  p = $("#bestellings").val();
  var rowCount = $("#bestellingTable tr").length - 2;
  if(p > rowCount) {
    for(i=rowCount; i<p; i+=1){
      addRow();
    }
    $("#bestellingTable #addButtonRow").appendTo("#bestellingTable");
  } else if(p < rowCount) {
  }
});

$('#aantal').bind('input', function() {
    

    var aantal = $(this).val();
    var that = $(this),
        prijs =  that.closest('tr').find('#prijs').val();
    var totaal = aantal * prijs;
    prijs =  that.closest('tr').find('#totalrow').html(totaal);

    prijs =  that.closest('tr').find('#totalrow').val(totaal);

});


$('#prijs').bind('input', function() {
   
    var aantal = $(this).val();
    var that = $(this),
        prijs =  that.closest('tr').find('#aantal').val();
    var totaal = aantal * prijs;

    prijs =  that.closest('tr').find('#totalrow').html(totaal);

    prijs =  that.closest('tr').find('#totalrow').val(totaal);
});

$('.rowtotal').val('0');

function calculateTotaal()
{

var sum = 0;

$('.rowtotal').each(function(){
    sum += parseInt($(this).val());
});

$('#sumTotal').html(sum);

$('#sumTotal').val(sum);

}

function lessTotal(less)
{
  var newp = $('#sumTotal').val() - less;

  $('#sumTotal').html(newp);

}

function addVal(totaal)
{

  $('.rowtotal').each(function(){

    if($(this).val() !== 0) {

    $(this).val(0);

  }

  });

}

var a = 0;

function destroy()
{


$('.product').each(function(){

 $(this).selectize()[0].selectize.destroy();

 $(this).removeClass('selectize');

 a++;

});

}


function make()
{
   $('.product').selectize({
    sortField: 'text'
});
}

var resp;

function setResp(amount) {
  resp = amount;
}

$('.product').change(function(){

   $.ajax({
  type: 'GET',
  url: '/getstock/'+$(this).val()+'',
  data: "",
  async: false
}).done(function(response) {
    setResp(response['response']);
  });

$(this).closest('tr').find('.aantal').attr('max', resp);

});

getKrediet($('#klant').find("option:first").attr("selected", true).val());

function getKrediet(id) {

  $('#melding').html('');

        $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

  $.ajax
({
  type: "POST",
  url: "/getkrediet/"+id+"",

  success: function(html)
  {
     $('#melding').html(html.html);
  }
});

} 

function checkvoltooid(that) {
        if (that.value == "2") {
            document.getElementById("betaaldbedrag").style.display = "block";
        } else {
            document.getElementById("betaaldbedrag").style.display = "none";
        }
    }


</script>

@endsection