@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Bestelling #{{ $order->ordercode }}</h3>

      <div class="row">
        <div class="col-6">

            <div class="card">
              <div class="card-header">
                Bestelling van <strong>{{ $order->klant()->first()->bedrijfsnaam }}</strong> op {{ $order->created_at }}
              </div>

              <div class="card-body">
                <table>
                  <tr>
                    <td><b>Status:</b></td>
                    <td>{{ $order->status($order->id) }}</td>
                  </tr>
                  <tr>
                    <td><b>Totaal:</b></td>
                    <td>€ {{ $order->order_total($order->id) }} euro</td>
                  </tr>
                </table> <br />
                <p>
                  <a href="{{ route('bekijkklant', ['id' => $order->klant()->first()->id]) }}" class="btn btn-warning">Bekijk het account van de deze klant</a>
                </p>
                @if($order->status === 1)
                  <form action="{{ action('OrderController@cancel', ['id' => $order->id]) }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">Order annuleren</button>
                  </form>
                @endif
              </div>
            </div>
           <br />
        </div>
        
        <div class="col-6">
        
            <div class="card">
              <div class="card-header">
              </div>

              <div class="card-body">
                
              </div>
            </div>

        </div>
      </div>

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle producten</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Korte omschrijving</th>
                  <th>Aantal</th>
                  <td>Stukprijs</td>
                  <th>Totaal</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Totaal:</th>
                  <th>€ {{ $order->order_total($order->id) }}</th>
                </tr>
              </tfoot>
              <tbody>
               @foreach($order->products()->get() as $product)
                <tr>
                  <td>{{ $product->sku }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->shortdescription }}</td>
                  <td>{{ $order->aantal($product->id, $order->id) }}</td>
                  <td>€ {{ $order->stukprijs($product->id, $order->id) }}
                  <td>€ {{ $order->prijs($product->id, $order->id) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection