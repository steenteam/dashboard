@extends('layout.app')
@section('content')
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />
<link href="http://cdn.kendostatic.com/2013.1.319/styles/kendo.mobile.all.min.css" rel="stylesheet" type="text/css" />
<div id="app" class="container-fluid">
   <h3>Bestelling #{{ $order->ordercode }} - On hold</h3>
   <form method="post" id="orderform" action="/onholdupdate/{{$order->id}}">
      @csrf
      <div class="row">
         <div class="col-md-4">
            <div class="card">
               <div class="card-header">
                  Klantinformatie
               </div>
               <div class="card-body">
                <a class="btn btn-danger" href="/cancelonhold/{{$order->id}}">Cancel on-hold</a>
                  <div class="form-group">
                     <label for="klant">Kies een klant</label>
                     <select onchange="getKrediet(this.value)" id="klant" class="klantkies" name="klant" required>
                        <option value=""></option>
                        @foreach($klanten as $klant)
                        <option @if($order->klant_id === $klant->id) selected="selected" @endif value="{{ $klant->id }}">{{ $klant->bedrijfsnaam }}</option>
                        @endforeach
                     </select>
                  </div>

                  <div class="form-group">
                     <label for="status">Status bestelling</label>
                     <select id="status" onchange="checkvoltooid(this)" class="form-control" name="status">
                        <option value="0">Openstaand</option>
                        <option value="1" selected>On hold</option>
                        <option value="2">Voltooid</option>
                     </select>
                  </div>
                  <div class="form-group" id="">
                     <label for="verzendkosten">Hoeveel verzendkosten worden er gerekend</label>
                     <input id="verzendkosten" name="verzendkosten" value="{{ $order->verzendkosten }}" class="form-control">
                  </div>
                  <div id="verzendmethodegroup" class="form-group">
                     <label for="verzendmethode">Verzendmethode</label>
                     <select id="verzendmethode" class="form-control" name="verzendmethode">
                        {{--               @foreach($shipping_methods as $shipping_method)
                        <option value="{{ $shipping_method['id'] }}" @if($shipping_method['id'] == 326) selected="selected" @endif>{{ $shipping_method['name'] }}</option>
                        @endforeach --}}
                        <option value="137">UPS Standard</option>
                        <option value="326">PostNL Fulfilment met handtekening 0-30KG</option>
                        <option value="320">PostNL Fulfilment abroad</option>
                        <option value="ophalen">Bestelling afhalen</option>
                     </select>
                  </div>

                                      <a class="" data-toggle="collapse" href="#extra" role="button" aria-expanded="false" aria-controls="extra">
    Extra instellingen laden..
  </a>

    <a class="pull-right" data-toggle="collapse" href="#notes" role="button" aria-expanded="false" aria-controls="notes">
    Notities
  </a>
            <div class="form-group" id="betaaldbedrag">
                     <label for="betaald">Hoeveel is er betaald</label>
                     <input id="betaald" name="betaald" class="form-control">
                  </div>
                  <div class="form-group" id="passwordbevestig">
                     <b>Bevestigingswachtwoord:</b> 
                     <input type="password" placeholder="Password" class="form-control" id="password">
                     <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password">
                  </div>
               </div>
        </div>

        <div class="collapse" id="extra">
              <div class="card card-body">
                  <div id="productenvoorraadgroup" style="margin-left: 25px;" class="form-group">
                     <input type="checkbox" name="voorraadaf" class="form-check-input" id="voorraadaf" checked>
                     <label for="voorraadaf">Producten niet van de voorraad afhalen</label>
                  </div>
                  <div id="pakbongroup" style="margin-left: 25px;" class="form-group">
                     <input type="checkbox" name="geenpakbon" class="form-check-input" id="geenpakbon">
                     <label for="geenpakbon">Geen pakbon printen</label>
                  </div>

                            <div class="form-group">
          <input type="checkbox"  value="1" class="" @if(is_null($klant->factureren)) @else checked="checked" @endif name="checkfactuur" id="checkfactuur">
          <label class="" id="checkfactuur" for="checkfactuur">De bestelling direct factureren</label>
          </div>

                                    <div id="adresandersgroup" class="form-group">
                     <label for="anderadres">Naar een ander adres versturen</label>
                     <select id="" onchange="checkadres(this)" class="form-control" name="anderadres">
                        <option value="0">Nee</option>
                        <option value="2">Ja</option>
                     </select>
                  </div>

                </div>

</div>

                             <div style="display: none;" id="anderadres">
                            <div class="card">
                              <div class="card-body">
                     <div class="form-group">
                        <label for="adres">Adres</label>
                        <input name="adres" class="form-control" placeholder="Adres van de klant">
                     </div>
                     <div class="form-group">
                        <label for="adres">Huisnummer</label>
                        <input name="huisnummer" class="form-control" placeholder="Huisnummer van de klant">
                     </div>
                     <div class="form-group">
                        <label for="stad">Stad</label>
                        <input name="stad" class="form-control" placeholder="Stad van de klant">
                     </div>
                     <div class="form-group">
                        <label for="postcode">Postcode</label>
                        <input name="postcode" class="form-control" placeholder="Postcode van de klant">
                     </div>
           <div class="form-group">
            <label for="country">Land</label>
         <select id="country" class="form-control" name="country">
            <option value="Netherlands">Nederland</option>
            <option value="Belgie">België</option>
            <option value="France">Frankrijk</option>
            <option value="Duitsland">Duitsland</option>
      </select>
       </div>
                   </div>
                 </div>
                  </div>

            
            <div id="melding">
            </div>
                <div class="collapse" id="notes">
            <div class="card">
               <div class="card-header">
                  Notities
               </div>
               <div class="card-body">
                  <div class="form-group">
                     <textarea name="note" class="form-control" placeholder="Vul hier notities in met betrekking tot deze order.">{{ $order->note }}</textarea>
                  </div>
               </div>
            </div>
          </div>
            <br />
         </div>
         <div class="col-md-8">
          <div id="stockstock" class="card">
            <div class="card-body">
              <span id="productnaam"></span> : 
              <span id="stockhoeveelheid"></span>
            </div>
          </div>
            <div class="card">
               <div class="card-header">
                  Producten
               </div>
               <div class="card-body">
                  <table class="table table-hover" id="bestellingTable">
                     <thead>
                        <tr>
                           <th>Productnaam</th>
                           <th>Aantal</th>
                           <th>Prijs</th>
                           <th>Totaalprijs</th>
                           <th>X</th>
                        </tr>
                     </thead>

                     @foreach($orderproducts as $oproducts)
                     <tr class="bestellingrow bestellingRow">
                        <td>
                           <select class="product-select product required-entry" name="products[]" id="product">
                              @foreach($products as $product)
                              <option data-stock="{{ $product->hoeveelheid }}" @if($oproducts->product_id === $product->id) selected="selected" @endif value="{{ $product->id }}">{{ $product->name }} </option>
                              @endforeach
                           </select>
                        </td>
                        <td>
                           <div class="form-group">
                              <input autocomplete="off" name="aantallen[]" id="aantal" value="{{ $oproducts->aantal }}" type="number" onChange="calculateTotaal()" placeholder="Aantal" class="required-entry small-box aantal form-control" required>
                           </div>
                           <span></span>
                        </td>
                        <td>
                           <div class="form-group">
                              <input autocomplete="off" onkeyup="this.value=this.value.replace(',','')" name="prijzen[]" id="prijs" @if(Auth::user()->id == '6' || Auth::user()->id == '7' || Auth::user()->id == '1' ) @else min="0" @endif onChange="calculateTotaal()" value="1" value="{{ $oproducts->nieuwprijs }}" type="text" placeholder="Prijs" class="required-entry prijs small-box form-control" required>
                           </div>
                        </td>
                        <td>
                           € <span value="0" class="rowtotal" id="totalrow">{{ $oproducts->aantal * $oproducts->nieuwprijs }}</span>
                        </td>
                        <td><button onclick="calculateTotaal();" class="btn btn-danger btn-delete-row remove" type="button">X</button></td>
                     </tr>

                     @endforeach
                  </table>
                  <table>
                     <tr id="addButtonRow">
                        <td>{{-- <button class="btn btn-large btn-success add" type="button">Extra product</button> --}}</td>
                        <td></td>
                        <td></td>
                        <td>Totaal:</td>
                        <td>€ <span value="0" id="sumTotal">0.00</span></td>
                     </tr>
                  </table>
                  <div class="text-center">
                     <button type="button" class="btn center-block btn-success" onclick="test();">Extra product</button>
                  </div>
               </div>
            </div>
            <br />
            <button type="submit" id="neworder" name="new" class="btn btn-primary">Bestelling on-hold zetten</button>
         </div>
      </div>
   </form>
</div>
<style>
   .table.user-select-none {
   -webkit-touch-callout: none;
   -webkit-user-select: none;
   -khtml-user-select: none;
   -moz-user-select: none;
   -ms-user-select: none;
   user-select: none;
   }
   .table-remove:first-child {
   display: none;
   }
</style>
@endsection
@section('js')
<script>
   // $(".remove").hide();
   

   function test()
   {
   
   
     $('#appendrow').appendTo("#bestellingTable");
   
     $('#bestellingTable').append(' <tr class="bestellingrow bestellingRow clonerow"><td><select class="product-select product required-entry" name="products[]" id="product">@foreach($products as $product) <option data-stock="{{ $product->hoeveelheid }}" value="{{ $product->id }}">{{ $product->name }}</option> @endforeach </select></td><td><div class="form-group"><input autocomplete="off" name="aantallen[]" id="aantal" type="number" max="1000" onChange="calculateTotaal()" placeholder="Aantal" class="required-entry small-box aantal form-control" required> </div><span></span></td>  <td><div class="form-group"> <input autocomplete="off" name="prijzen[]" @if(Auth::user()->id == '6' || Auth::user()->id == '7' || Auth::user()->id == '1' ) @else min="0" @endif value="" id="prijs" onChange="calculateTotaal()" type="text" placeholder="Prijs" class="required-entry small-box prijs form-control" required> </div> </td> <td> € <span value="0" class="rowtotal" id="totalrow">0.00</span> </td> <td><button onclick="calculateTotaal();" class="btn btn-danger btn-delete-row remove" type="button">X</button></td></tr>');
   
        $('.product:last').selectize({
       sortField: 'text'
   });
   
   }
   
   




</script>
 <script src="{{ asset('js/bestelling.js') }}"></script>

 <script>
    calculateTotaal();
 </script>
@endsection