@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Bestelling #{{ $order->ordercode }}</h3>

      <div class="row">
        <div class="col-6">

            <div class="card">
              <div class="card-header">
                Bestelling van <strong>{{ $order->klant()->first()->bedrijfsnaam }}</strong> op {{ $order->created_at->format('m/d/Y h:i:s') }}
              </div>

              <div class="card-body">
                <table>
                  <tr>
                    <td><b>Status:</b></td>
                    <td>{{ $order->status($order->id) }}</td>
                  </tr>
                  <tr>
                    <td><b>Totaal:</b></td>
                    <td>€ {{ $order->order_total($order->id)  + $order->verzendkosten }} euro</td>
                  </tr>
                </table> <br />
                <p>
                  <a href="{{ route('bekijkklant', ['id' => $order->klant()->first()->id]) }}" class="btn btn-warning">Bekijk het account van de deze klant</a>

                  <button data-target="#pakbonprint" data-toggle="modal" type="" class="btn btn-success"> <i class="fa fa-fw fa-print"></i> Print pakbon</button>

                                      @if($order->getTracking($order->id) != 'null')
                     <a href="{{ $order->getTracking($order->id) }}" target="_blank" class="btn btn-warning">Traceren</a> &nbsp;

                     @endif
                </p>

                @if($order->status == 1)
                  <form action="{{ action('OrderController@cancel', ['id' => $order->id]) }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-danger">Order annuleren</button>
                  </form>
                @endif
              </div>
            </div>
           <br />
        </div>
        
        <div class="col-6">
        @if($checkorders)
            <div class="card">
              <div class="card-header">
                Verekening
              </div>

              <div class="card-body">

                <a class="btn btn-success" href="/bekijkverekening/{{ $checkorders->verekening_id }}">Bekijk de verekening bij deze order</a>
                
              </div>
            </div>

            @endif

            @if(!is_null($order->factuur))

                        <div class="card">
              <div class="card-header">
                Factureren
              </div>

              <div class="card-body">

                @if($order->factuur == 0)

                <form action="/voltooifactuur/{{ $order->id }}">
                  @csrf
                  <div class="form-group">
                    <input type="text" name="factuurnr" class="form-control" placeholder="Factuur nummer" required>
                  </div>
                    <button type="submit" class="btn btn-success">Factuur voltooien</button>
                  </div>
                </form>

                @endif

                @if($order->factuur == 1)

                Deze order is gefactureerd en voltooid. <br />
                Factuurnummer: <b>{{ $order->factuurnr }}</b>

                @endif
        
              </div>
            </div>

            @endif

        </div>
      </div>

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle producten</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Korte omschrijving</th>
                  <th>Aantal</th>
                  <td>Stukprijs</td>
                  <th>Totaal</th>
                </tr>
              </thead>
              <tfoot>
                 <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Verzendkosten:</th>
                  <th>€ {{ $order->verzendkosten }}</th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Totaal:</th>
                  <th>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</th>
                </tr>
              </tfoot>
              <tbody>
               @foreach($order->products()->get() as $product)
                <tr>
                  <td>{{ $product->sku }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->shortdescription }}</td>
                  <td>{{ $order->aantal($product->id, $order->id) }}</td>
                  <td>€ {{ $order->stukprijs($product->id, $order->id) }}
                  <td>€ {{ $order->prijs($product->id, $order->id) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>


    </div>

    <!-- Modal -->-
<div class="modal fade" id="pakbonprint" tabindex="-1" role="dialog" aria-labelledby="pakbonprintTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">De pakbon printen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Weet je zeker dat je deze pakbon wilt afdrukken?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <button type="button" id="printnu" class="btn btn-primary">Nu printen</button>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')

<script>

  $('#printnu').click(function() {

    $(this).hide();

  });

$('#printnu').click(function() {
 $('#pakbonprint').modal('toggle');
   $.ajax({
  type: 'GET',
  url: '/printpakbon/{{ $order->ordercode }}',
  data: "",
  async: false
}).done(function(response) {
   
  });

});

</script>

@endsection