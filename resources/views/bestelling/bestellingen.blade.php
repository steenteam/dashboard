@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Alle bestellingen</h3>

            @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Bestellingen</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                <tr>
                  <td><a href="{{ route('bekijkklant', ['id' => $order->klant()->first()->id]) }}">{{ $order->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>{{ $order->status($order->id) }}</td>
                  <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                  @if($order->status === '1')
                  <td><a href="{{ route('checkonhold', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                  @else
                  <td><a href="{{ route('bestelling', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                  @endif
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection