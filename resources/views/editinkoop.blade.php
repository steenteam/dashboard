@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Locatie bewerken</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="/editinkoop/{{ $product->id }}">

    @csrf 

    <div class="form-group">
      <label for="name">Inkoop</label>
      <input name="price" value="{{ $product->price }}" class="form-control" required>
    </div>


    <div class="form-group">
      <button class="btn btn-success" type="submit">Aanpassen</button>
    </div>


  </form>
  

</div>
@endsection