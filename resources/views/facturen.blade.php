@extends('layout.app')

@section('content')

    <div class="container-fluid">


                 @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif


      <div class="row">
        <div class="col-lg-8">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="facturentodo-tab" data-toggle="tab" href="#facturentodo" role="tab" aria-controls="facturentodo" aria-selected="true">Facturen to-do</a>
  </li>
    <li class="nav-item">
    <a class="nav-link" id="facturenaf-tab" data-toggle="tab" href="#facturenaf" role="tab" aria-controls="facturenaf" aria-selected="true">Facturen af</a>
  </li>
</ul>

<br />

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="facturentodo" role="tabpanel" aria-labelledby="facturentodo-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Facturen to-do</div>
        <div class="card-body">

          @if($ordersopen->isNotEmpty())
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <td>Klant</td>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Afmaken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ordersopen as $order)
                <tr>
                  <td><a href="/bekijkklant/{{ $order->klant()->first()->id }}">{{ $order->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                  <td><a href="{{ route('bestelling', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          Er zijn geen onafgemaakte facturen.
          @endif
        </div>
      </div>

    </div>

      <div class="tab-pane fade" id="facturenaf" role="tabpanel" aria-labelledby="facturenaf-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Facturen af</div>
        <div class="card-body">

          @if($ordersdone->isNotEmpty())
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <td>Klant</td>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Factuur nr</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ordersdone as $order)
                <tr>
                  <td><a href="/bekijkklant/{{ $order->klant()->first()->id }}">{{ $order->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                  <td>{{ $order->factuurnr }}</td>
                  <td><a href="{{ route('bestelling', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          Er zijn geen afgemaakte facturen.
          @endif
        </div>
      </div>

    </div>


  </div>
   
</div>
      </div>
    </div>



@endsection