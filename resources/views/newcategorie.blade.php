@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Nieuwe categorie maken</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="{{ action('CategorieController@create') }}">

    @csrf 

    <div class="form-group">
      <label for="name">Naam van categorie</label>
      <input name="name" placeholder="Naam van categorie" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="parentid">Hoofdcategorie</label>
      
      <select name="parentid" class="form-control">
        <option value="0">Kies een hoofdcategorie</option>
        @foreach($allcategories as $categorie)
        <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <input type="file" name="image" required>
    </div>

    <div class="form-group">
      <button class="btn btn-success" type="submit">Nieuwe categorie</button>
    </div>


  </form>
  

</div>
@endsection