@extends('layout.app')

@section('content')

 <div class="container-fluid">

      <h2>Voorraad beheer</h2>
     <div class="card">

        <div class="card-body">

<div class="bd-example bd-example-tabs">
  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active show" id="uitvoerend-tab" data-toggle="tab" href="#uitvoerend" role="tab" aria-controls="uitvoerend" aria-selected="true">Uitvoerend</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="voltooid-tab" data-toggle="tab" href="#voltooid" role="tab" aria-controls="voltooid" aria-selected="false">Voltooid</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade active show" id="uitvoerend" role="tabpanel" aria-labelledby="uitvoerend-tab">

                <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Tracking / Status</th>
                  <th>Gecontroleerd</th>
                  <th>Datum</th>
                </tr>
              </thead>
              <tbody>
                @foreach($inkopentodo as $todo)
                <tr>
                  <td>{{ $todo->id }}</td>
                  <td>@if( $todo->getStatus($todo->tracking) == 1) <span id="bezorgd">Bezorgd</span> @else <span id="onderweg">Onderweg</span> @endif</td>
                  <td>@if( $todo->gecontroleerd == 0) <span id="onderweg"><a href="/voorraad/controleer/{{ $todo->id }}"> Nog niet gecontroleerd </a></span> @else <span id="bezorgd">Gecontroleerd</span> @endif</td>
                  <td>{{ $todo->created_at }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>



    </div>
    <div class="tab-pane fade" id="voltooid" role="tabpanel" aria-labelledby="voltooid-tab">

                <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Tracking / Status</th>
                  <th>Gecontroleerd</th>
                  <th>Datum</th>
                </tr>
              </thead>
              <tbody>
                @foreach($inkopenaf as $af)
                <tr>
                  <td>{{ $af->id }}</td>
                  <td>@if( $af->getStatus($af->tracking) == 1) <span id="bezorgd">Bezorgd</span> @else <span id="onderweg">Onderweg</span> @endif</td>
                  <td>@if( $af->gecontroleerd == 0) <span id="onderweg">Nog niet gecontroleerd</span> @else <span id="bezorgd">Gecontroleerd</span> @endif</td>
                  <td>{{ $af->created_at }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
    </div>
  </div>
</div>
 

</div>
</div>

<br />

<a href="{{route('newinkoop')}}" class="btn btn-success">Nieuwe inkoop</a>


</div>
  
@endsection