@extends('layout.app')

@section('content')

 <div class="container-fluid">

  <div class="col-6">

      <h1>Nieuwe inkoop</h1>
        <div class="card">

           <div class="card-body">

            <form enctype='multipart/form-data' action="{{ action('VoorraadController@create')}}" method="post">

              @csrf

              <div class="form-group">
                Excel import bestand <br />
                <input name="excelfile" type="file">
              </div>

              <div class="form-group">
                <input placeholder="Tracking nummer" name="tracking" class="form-control">
              </div>



              <button type="submit" class="btn btn-success">Toevoegen</button>
            </form>

           </div>
      </div>

    </div>


</div>
  
@endsection