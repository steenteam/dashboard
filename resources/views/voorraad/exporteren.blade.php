@extends('layout.app')

@section('content')

    <div class="container-fluid">

      <h1>Voorraad analyse</h1>

      <br />

      <div class="col-6">

      <div class="card">
        <div class="card-header">
          Kies wat je wilt exporteren
        </div>

        <div class="card-body">

          <form method="post" action="{{ action('VoorraadController@export') }}">
            @csrf
          <div class="form-group">
            <label for="aantal">Vanaf welke productrange</label>
            <input name="aantal" class="form-control" placeholder="X of minder in voorraad">
          </div>
          <div class="form-group">
            <label for="device">Apparaat</label>
            <select name="device" id="device" class="form-control">
              <option value="none">Geen voorkeur</option>
              <option value="smartphone">Smartphone</option>
              <option value="tablet">Tablet</option>
            </select>
          </div>

          <div class="form-group">
            <label for="categorie">Categorie</label>
            <select name="categorie" id="categorie" class="form-control">
              <option value="none">Geen voorkeur</option>
              <option value="displays">Displays</option>
              <option value="batteries">Batteries</option>
              <option value="parts">Parts</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="merk">Merk</label>
            <select name="merk" id="merk" class="form-control">
              <option value="none">Geen voorkeur</option>
              <option value="apple">Apple</option>
              <option value="sony">Sony</option>
              <option value="samsung">Samsung</option>
              <option value="huawei">Huawei</option>
              <option value="lg">LG</option>
              <option value="htc">HTC</option>
              <option value="motorola">Motorola</option>
            </select>
          </div>
        <button type="submit" class="btn btn-success">Exporteren</button>

                  </div>
          </div>
          </form>

 
        </div>
      </div>



</div>
@endsection