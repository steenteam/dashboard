@extends('layout.app')

@section('content')

    <div class="container-fluid">

      <h1>Controleren</h1>

      <br />

      <div class="col-6">

      <div class="card">
        <div class="card-header">
          Controleer de inkoopbestelling.
        </div>

        <br />
        <div class="card-body">

           <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Hoeveelheid</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>SKU</th>
                  <th>Hoeveelheid</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($results as $result)
                <tr>
                  <td>{{ $result->sku }}</td>
                  <td>{{ $result->quantity }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
                <br />
                 <form action="/voorraad/controleer/{{ $inkoop->id }}" method="post">
        @csrf

          <div class="form-check">
    <input type="checkbox" class="form-check-input" id="check" required>
    <label class="form-check-label" for="check">De bestelling is gecontroleerd een aangepast waar nodig.</label>
       </div> <br />

        <button class="btn btn-success" type="submit">Bestelling bevestigen</button>
       </form>
      
        </div>
      </div>



</div>
@endsection