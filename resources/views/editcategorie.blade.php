@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Categorie aanpassen: {{ $categorie->name }}</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="{{ action('CategorieController@update', ['id' => $categorie->id]) }}">

    @csrf 

    <div class="form-group">
      <label for="name">Naam van categorie</label>
      <input name="name" placeholder="Naam van categorie" class="form-control" value="{{ $categorie->name }}" required>
    </div>

    <div class="form-group">
      <label for="parentid">Hoofdcategorie</label>
      <select name="parentid" class="form-control">
        <option value="0">Kies een hoofdcategorie</option>
        @foreach($allcategories as $categorielist)
        <option value="{{ $categorielist->id }}" @if($categorie->parentid === $categorielist->id) selected @endif>{{ $categorielist->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <input type="file" name="image" value="{{ $categorie->image }}">
    </div>

    <div class="form-group">
      <button class="btn btn-success" type="submit">Categorie aanpassen</button>
    </div>

    <p> <b>Huidige afbeelding: </b> </p>

    <img class="medium_categorie_image" alt="Afbeelding van {{ $categorie->name }}" src="/storage/{{ $categorie->image }}">


  </form>
  

</div>
@endsection