@extends('layout.app')

@section('content')

    <div class="container-fluid">


                 @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif


      <div class="row">
        <div class="col-lg-8">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="allebestellingen-tab" data-toggle="tab" href="#allebestellingen" role="tab" aria-controls="allebestellingen" aria-selected="true">Alle bestellingen</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="newproducts-tab" data-toggle="tab" href="#newproducts" role="tab" aria-controls="newproducts" aria-selected="false">Nieuwe producten</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="openstaand-tab" data-toggle="tab" href="#openstaand" role="tab" aria-controls="openstaand" aria-selected="false">Openstaand</a>
  </li>
</ul>

<br />

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="allebestellingen" role="tabpanel" aria-labelledby="allebestellingen-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Laatste bestellingen</div>
        <div class="card-body">

          @if($orders->isNotEmpty())
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <td>Klant</td>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                <tr>
                  <td><a href="/bekijkklant/{{ $order->klant()->first()->id }}">{{ $order->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>{{ $order->status($order->id) }}</td>
                  <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                  <td><a href="{{ route('bestelling', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          Er zijn vandaag geen bestellingen gemaakt.
          @endif
        </div>
      </div>

       <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> On-hold bestellingen</div>
        <div class="card-body">
          @if($ordershold->isNotEmpty())
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Status</th>
                  <th>Bedrag</th>
                  <th>Datum</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ordershold as $order)
                <tr>
                  <td><a href="/bekijkklant/{{ $order->klant()->first()->id }}">{{ $order->klant()->first()->bedrijfsnaam }}</a></td>
                  <td>{{ $order->status($order->id) }}</td>
                  <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                  <td><a href="{{ route('checkonhold', ['id' => $order->id]) }}" class="btn btn-success">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else 
          Er zijn geen on-hold bestellingen gevonden.
          @endif
        </div>
      </div>

    </div>



  <div class="tab-pane fade" id="openstaand" role="tabpanel" aria-labelledby="openstaand-tab">
  <br />
              <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">

               <thead>
                <tr>
                  <th>Bedrijfsnaam </th>
                  <th>Bekijken</th>
                </tr>
              </thead>

                <tbody>
                @foreach($waardig as $waardigs)
                <tr>
                  <td>{{ $waardigs->bedrijfsnaam }}</td>
                  <td><a class="btn btn-success" href="/bekijkklant/{{ $waardigs->id }}">Bekijken</a></td>
                </tr>
                @endforeach
              </tbody>

            </table>
          </div>

  </div>

  <div class="tab-pane fade" id="newproducts" role="tabpanel" aria-labelledby="newproducts-tab">

          <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Producten zie vandaag zijn toegevoegd.</div>
        <div class="card-body">

          @if($productstoday->isNotEmpty())
          <div class="table-responsive">
            <table class="table dataTable table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Productnaam</th>
                  <th>Korte omschrijving</th>
                  <td>Categorie</th>
                  <th>Aanpassen</th>
                </tr>
              </thead>
              <tbody>
                @foreach($productstoday as $product)
                <tr>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->shortdescription }}</td>
                  <td>-</td>
                  <td></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          Er zijn vandaag geen producten toegevoegd.
          @endif
        </div>
      </div>
    </div>
      </div>


  </div>

          <div class="card mb-3" style="display: none; visibility: hidden;">
           <div class="card-header">
              <i class="fa fa-bell-o"></i> Activiteiten log</div>
            <div class="list-group list-group-flush small">

              @foreach($logs as $log)
              <a class="list-group-item list-group-item-action" href="#">
                <div class="media">
                  <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
                  <div class="media-body">
                    <strong></strong> heeft {{ $log->description }}.
                    <div class="text-muted smaller">{{ $log->created_at->format('m/d/Y h:i:s') }}</div>
                  </div>
                </div>
              </a>
              @endforeach
              <a class="list-group-item list-group-item-action" href="#">View all activity...</a>
            </div>
          </div>
   

          <div class="mb-3 col-lg-4">
            <div class="card">
            <div class="card-header">
              <i class="fa fa-bell-o"></i> Agenda <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modalAgenda">Nieuw item</button></div>
            <div class="list-group list-group-flush small">

             @foreach($kalender as $kalender)
              <a class="list-group-item list-group-item-action" href="/deletekalender/{{ $kalender->id }}">
                <div class="media">
                  <div class="media-body">
                    <strong>{{ $kalender->content }}</strong>.
                    <div class="text-muted smaller">{{ $kalender->created_at->format('m/d/Y h:i:s') }}</div>
                    <div onclass="delete-agenda">Verwijderen</div>
                  </div>
                </div>
              </a>
              @endforeach

            </div>
          </div>
     </div>
</div>
      </div>
    </div>


      <!-- Modal -->
  <div class="modal fade" id="modalAgenda" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ action('DashboardController@agendanew') }}" class="" method="post">

            @csrf

            <div class="form-group">
              <input class="form-control" name="content" placeholder="Bericht" type="text">
            </div>

            <div class="form-group">
                <input class="form-control" name="tijd" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
            </div>

            <button class="btn btn-default" type="submit">Opslaan</button>

          </form>

         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Sluiten</button>
        </div>
      </div>
    </div>
  </div>


@endsection