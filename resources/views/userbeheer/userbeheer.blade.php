@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Gebruikersbeheer</h3>

      @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

      @if (session('error'))
        <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('error') }}
        </div>
      @endif

     <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Alle gebruikers</div>
        <div class="card-body">

          <p><a class="btn btn-success" href="{{ route('newuser') }}">Nieuwe gebruiker</a></p>

          <div class="table-responsive">
            <table class="table dataTable table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Naam</th>
                  <th>Email</th>
                  <th>Bekijken</th>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td>{{ $user->name }}</td>
                  <td>{{ $user->email }} </td>
                  <td><a class="btn btn-success" href="{{ route('bekijkuser', ['id' => $user->id])}}">Gebruiker bekijken</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

@endsection

@section('js')
@endsection