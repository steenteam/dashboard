@extends('layout.app')

@section('content')

    <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 text-center ">
        <div class="panel panel-default">
          <div class="userprofile social ">
            <div class="userpic"> <img src="{{ $user->avatar }}" alt="" class="userpicimg"> </div>
            <h3 class="username">{{ $user->name }}</h3>
            <p>{{ $user->email }}</p>
          </div>
          <div class="col-md-12 border-top border-bottom">
            <ul class="nav nav-pills pull-left countlist" role="tablist">
              <li role="presentation">
                <h3>{{ $activity->count() }}<br>
                  <small>Activiteiten</small> </h3>
              </li>
            </ul>
            <a href="#deleteModal" class="btn btn-danger followbtn" data-toggle="modal">Verwijderen</a>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
      <!-- /.col-md-12 -->

           <div class="row">
        <div class="col-lg-8">
          <div class="alert alert-success" style="display:none"></div>
          <div class="card">
            <div class="card-header">
              Groepen
            </div>
            <div class="card-body">
              <form id="roleForm">
              <select id="select-role" multiple>
                @foreach($roles as $role)
                <option value="{{ $role->id }}" @if($user->hasRole($role->name)) selected @endif>{{ $role->name }}</option>
                @endforeach
              </select>
              <button class="btn btn-primary" id="saveRole">Opslaan</button>
            </form>
            </div>
          </div>

          <br />

         <div class="card">
            <div class="card-header">
              Rechten
            </div>
            <div class="card-body">
              <form id="permissionForm">
              <select id="select-permission" multiple>
                @foreach($permissions as $permission)
                <option value="{{ $permission->name }}" @if($user->hasPrivatePermission($permission->name, $user->id)) selected @endif>{{ $permission->name }}</option>
              @endforeach
              </select>
              <button class="btn btn-primary" id="savePermission">Opslaan</button>
            </form>
            </div>
          </div>

          <br />

        </div>
        <div class="col-lg-4">
          <div class="card mb-3">
            <div class="card-header">
              <i class="fa fa-bell-o"></i> Activiteiten log</div>
            <div class="list-group list-group-flush small">

              @foreach($logs as $log)
              <a class="list-group-item list-group-item-action" href="#">
                <div class="media">
                  <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
                  <div class="media-body">
                    <strong>{{ App\User::find( $log->causer_id )->name }}</strong> heeft {{ $log->description }}.
                    <div class="text-muted smaller"></div>
                  </div>
                </div>
              </a>
              @endforeach
              <a class="list-group-item list-group-item-action" href="#">View all activity...</a>
            </div>
            <div class="card-footer small text-muted"></div>

          </div>
        </div>
      </div>

    <!-- Modal Delete -->
    <div id="deleteModal" class="modal fade">
      <div class="modal-dialog modal-confirm">
        <div class="modal-content">
          <div class="modal-header">    
            <h4 class="modal-title">Zeker weten?</h4>  
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Weet je zeker dat je deze gebruiker wilt verwijderen?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            <a href="{{ route('deleteuser', ['id' => $user->id])}}" id="deleteButton" class="btn btn-danger">Delete</a>
          </div>
        </div>
      </div>
    </div>  

@endsection

@section('js')
<script>
$('#select-role').selectize({
  placeholder: 'Stel een groep in voor deze gebruiker',
});

$('#select-permission').selectize({
  placeholder: 'Stel een recht in voor deze gebruiker',
});

         $(document).ready(function(){
            $('#saveRole').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               $.ajax({
                  url: "{{ action('BeheerUserController@saveRoles', ['id' => $user->id]) }}",
                  method: 'post',
                  data: {
                     roles: $('#select-role').val()
                  },
                  success: function(result){
                     $('.alert').show();
                     $('.alert').html(result.success);
                     $(".alert-success").delay(1000).fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-success").slideUp(500);
});
                  }});
               });

            $('#savePermission').click(function(e){
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
               $.ajax({
                  url: "{{ action('BeheerUserController@savePermissions', ['id' => $user->id]) }}",
                  method: 'post',
                  data: {
                     permissions: $('#select-permission').val()
                  },
                  success: function(result){
                     $('.alert').show();
                     $('.alert').html(result.success);
                     $(".alert-success").delay(1000).fadeTo(2000, 500).slideUp(500, function(){
    $(".alert-success").slideUp(500);
});
                  }});
               });
            });



</script>

@endsection