@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Een nieuwe gebruiker maken</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="{{ route('createuser') }}">

    @csrf 

    <div class="form-group">
      <label for="name">Naam van gebruiker</label>
      <input name="name" placeholder="Naam van gebruiker" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="email">Email van gebruiker</label>
      <input type="email" name="email" placeholder="Email van gebruiker" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="password">Wachtwoord van gebruiker</label>
      <input type="password" name="password" placeholder="Vul hier een wachtwoord in." class="form-control" required>
    </div>


    <div class="form-group">
      <button class="btn btn-success" type="submit">Nieuwe gebruiker</button>
    </div>


  </form>
  

</div>
@endsection