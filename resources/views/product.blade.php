@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Online product</h3>

      <p><a class="btn btn-success" href="{{ route('productnew') }}">Nieuwe product</a></p>

<div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle producten</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Name</th>
                  <th>Location</th>
                  <th>Aantal</th>
                  <th>Verkoopprijs</th>
                </tr>
              </thead>
              <tbody>
                @foreach($product as $product)
                <tr>
                  <td>{{ $product->sku }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->locatie }}</td>
                  <td>{{ $product->getStock($product->id) }}</td>
                  <td><a href="/editinkoop/{{ $product->id}}">{{ $product->getInkoopRaw($product->id) }}</a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>

@endsection