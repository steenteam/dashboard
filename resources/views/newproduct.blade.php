@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Nieuwe Product maken</h3> <br />

  <form class="" method="post" enctype="multipart/form-data" action="{{ action('ProductController@create') }}">

    @csrf 

    <div class="form-group">
      <label for="name">Naam van Product</label>
      <input name="name" placeholder="Naam van Product" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="parentid">Categorie van artikel</label>
      <select name="parentid" class="form-control" required>
        <option value="0">Kies een categorie</option>
        @foreach($allcategories as $categorie)
        <option value="{{ $categorie['id'] }}">{{ $categorie['name'] }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="description">Omschrijving van Product</label>
      <textarea name="description" placeholder="Omschrijving van Product" class="form-control" required></textarea>
    </div>

    <div class="form-group">
      <label for="shortdescription">Korte omschrijving van Product</label>
      <textarea name="shortdescription" placeholder="Korte omschrijving van Product" class="form-control" required></textarea>
    </div>

     <div class="form-group">
      <label for="description">Prijs</label>
      <input name="price" placeholder="Prijs van product" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="description">SKU</label>
      <input name="sku" placeholder="Het SKU nummer" class="form-control" required>
    </div>

    <div class="form-group">
      <label for="file">Afbeeldingen</label>
      <input type="file" name="images[]" multiple="multiple">
    </div>

    <div class="form-group">
      <button class="btn btn-success" type="submit">Nieuwe Product</button>
    </div>


  </form>
  

</div>
@endsection