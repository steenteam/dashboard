@extends('layout.app')

@section('content')

    <div class="container-fluid">


                 @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif


      <div class="row">
        <div class="col-lg-8">

<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="allebestellingen-tab" data-toggle="tab" href="#allebestellingen" role="tab" aria-controls="allebestellingen" aria-selected="true">Alle openstaande debiteuren</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="hold-tab" data-toggle="tab" href="#hold" role="tab" aria-controls="hold" aria-selected="false">Voorraad</a>
  </li>
</ul>

<br />

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="allebestellingen" role="tabpanel" aria-labelledby="allebestellingen-tab">          
    <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Alle openstaande debiteuren</div>
        <div class="card-body">
  
          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam</th>
                  <th>Openstaand</th>
                </tr>
              </thead>
              <tbody>

                @foreach($klanten as $klant)

                <tr>
                  <td><a href="/bekijkklant/{{ $klant->id }}">{{ $klant->bedrijfsnaam }}</a></td>
                  <td>€ {{ $klant->openstaand($klant->id) }}</td>
                </tr>

                @endforeach
              </tbody>

              <tfoot>
                <tr>
                  <th>Totaal openstaand:</th>
                  <th>€ {{ $totaalopenstaand }}</th>
                </tr>
              </tfoot>
            </table>
          </div>

        </div>
      </div>

    </div>

  <div class="tab-pane fade" id="hold" role="tabpanel" aria-labelledby="hold-tab">

 <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Voorraad</div>
        <div class="card-body">

Totale waarde voorraad: € {{ $totalevoorraad }} <br />
Totaal aantal producten op voorraad:  {{ $totaleaantal }}

        </div>
      </div>


  </div>




</div>
      </div>
    </div>



@endsection