       <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

       <form action="{{ action('OrderController@update', ['id' => $id]) }}" method="post">
        @csrf
            <table class="table table-hover" id="bestellingTable{{ $id }}">
                    <thead>
                        <tr>
                            <th width="50%">Productnaam</th>
                            <th width="10%">Aantal</th>
                            <th width="10%">Prijs</th>
                            <th width="10%">Totaalprijs</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <div id="bestellingBody{{$id}}">

                      @foreach($order as $order)
                    <tr class="bestellingrow bestellingRow{{$id}}">
                        <td>

                          {{ $klant->getname($order->product_id) }}

                          <input name="products[]" value="{{ $order->product_id }}" style="display: none; visibility: hidden;" >

                        
                        </td>
                        <td>
                                                    <div class="form-group">
                          <input autocomplete="off" value="{{$order->aantal}}" name="aantallen[]" id="aantal" type="number" onChange="calculateTotaal();" placeholder="Aantal" class="required-entry aantal small-box form-control aantal">
                        </div>
                         <span></span>
                        </td>
                        <td>                          <div class="form-group">
                          <input autocomplete="off" name="prijzen[]" value="{{$order->nieuwprijs}}" id="prijs" onChange="calculateTotaal()" type="text" placeholder="Prijs" class="required-entry small-box form-control prijs">
                        </div>
                        </td>
                        <td>
                          € <span value="{{ $order->nieuwprijs * $order->aantal }}" class="rowtotal{{$id}}" id="totalrow{{$id}}">{{ $order->nieuwprijs * $order->aantal }}</span>
                        </td>
                        <td><button class="btn btn-danger btn-delete-row remove{{$id}}" type="button">X</button></td>
                    </tr>

                    @endforeach
                  </div>
                    <tr id="add{{$id}}ButtonRow">
                        <td></td>
                        <td></td>
                        <td>Totaal:</td>
                        <td>€ <span value="{{ $totaal }}" id="sumTotal">{{ $totaal }}</span></td>
                        <td></td>
                    </tr>
            </table>   

            <button type="submit" id="save" class="btn btn-warning">Opslaan</button>

          </form>
    <script src="{{ asset('js/app.js') }}"></script>
            <script>
              


make();

/* Variables */
var p = $("#bestellings{{ $id }}").val();
var row{{$id}} = $(".bestellingRow{{$id}}:first");

/* Functions */
function getP(){
  p = $("#bestellings{{ $id }}").val();
}

function add{{$id}}Row() {
  destroy();
  row{{$id}}.clone(true, true).appendTo("#bestellingTable{{ $id }}");
  make();
  calculateTotaal();
}

function remove{{$id}}Row(button) {
  button.closest("tr").remove();
  lessTotal(button.closest("tr").find('#totalrow{{$id}}').val());
}
/* Doc ready */
$(".add{{$id}}").on('click', function () {
  getP();
  if($("#bestellingTable{{ $id }} tr").length < 17) {
    add{{$id}}Row();
    var i = Number(p)+1;
    $("#bestellings{{ $id }}").val(i);
  }
  $(this).closest("tr").appendTo("#bestellingTable{{ $id }}");
  if ($("#bestellingTable{{ $id }} tr").length === 3) {
    $(".remove{{$id}}").hide();
  } else {
    $(".remove{{$id}}").show();
  }
});
$(".remove{{$id}}").on('click', function () {
  getP();
  if($("#bestellingTable{{ $id }} tr").length === 1) {
    //alert("Can't remove{{$id}} row.");
    $(".remove{{$id}}").hide();
  } else if($("#bestellingTable{{ $id }} tr").length - 1 ==1) {
    $(".remove{{$id}}").hide();
    remove{{$id}}Row($(this));
    var i = Number(p)-1;
    $("#bestellings{{ $id }}").val(i);
  } else {
    remove{{$id}}Row($(this));
    var i = Number(p)-1;
    $("#bestellings{{ $id }}").val(i);
  }

  if($("#bestellingTable{{ $id }} tr").length === 2) {
    $('#save').hide();
  }
});
$("#bestellings{{ $id }}").change(function () {
  var i = 0;
  p = $("#bestellings{{ $id }}").val();
  var rowCount = $("#bestellingTable{{ $id }} tr").length - 2;
  if(p > rowCount) {
    for(i=rowCount; i<p; i+=1){
      add{{$id}}Row();
    }
    $("#bestellingTable{{ $id }} #add{{$id}}ButtonRow").appendTo("#bestellingTable{{ $id }}");
  } else if(p < rowCount) {
  }
});

$('.aantal').bind('input', function() {
    

    var aantal = $(this).val();
    var that = $(this),
        prijs =  that.closest('tr').find('#prijs').val();
    var totaal = aantal * prijs;
    prijs =  that.closest('tr').find('#totalrow{{$id}}').html(totaal);

    prijs =  that.closest('tr').find('#totalrow{{$id}}').val(totaal);

});




function calcprijstotaal() {
  var aantal = $('#prijs').val();
    var that = $('#prijs'),
        prijs =  that.closest('tr').find('#aantal').val();
    var totaal = aantal * prijs;

    prijs =  that.closest('tr').find('#totalrow{{$id}}').html(totaal);

    prijs =  that.closest('tr').find('#totalrow{{$id}}').val(totaal);
}


$('.prijs').bind('input', function() {
   
    var aantal = $(this).val();
    var that = $(this),
        prijs =  that.closest('tr').find('#aantal').val();
    var totaal = aantal * prijs;

    prijs =  that.closest('tr').find('#totalrow{{$id}}').html(totaal);

    prijs =  that.closest('tr').find('#totalrow{{$id}}').val(totaal);
});

$('.rowtotal{{$id}}').val('0');

function calculateTotaal()
{

var sum = 0;

$('.rowtotal{{$id}}').each(function(){
    sum += parseFloat($(this).val());
});

$('#sumTotal').html(sum);

$('#sumTotal').val(sum);

}

function lessTotal(less)
{
  var newp = $('#sumTotal').val() - less;

  $('#sumTotal').html(newp);

}

function add{{$id}}Val(totaal)
{

  $('.rowtotal{{$id}}').each(function(){

    if($(this).val() !== 0) {

    $(this).val(0);

  }

  });

}

var a = 0;

function destroy()
{


$('.product').each(function(){

 $(this).selectize()[0].selectize.destroy();

 $(this).removeClass('selectize');

 a++;

});

}


function make()
{
   $('.product').selectize({
    sortField: 'text'
});
}

var resp;

function setResp(amount) {
  resp = amount;
}

$('.product').change(function(){

   $.ajax({
  type: 'GET',
  url: '/getstock/'+$(this).val()+'',
  data: "",
  async: false
}).done(function(response) {
    setResp(response['response']);
  });

$(this).closest('tr').find('.aantal').attr('max', resp);

});

</script>
