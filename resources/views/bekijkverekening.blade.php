@extends('layout.app')

@section('content')

    <div class="container-fluid">


      @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

      <br />

      <div class="row">

        <div class="col-8">

                          <div class="card">
                <div class="card-header">
                  Verekening van <a href="/bekijkklant/{{$klant->id}}"> {{ $klant->bedrijfsnaam }}</a> - {{ $verekening->created_at->format('m/d/Y h:i:s') }}
                  @if($verekening->prijs == 0)
                  <a class="btn btn-danger pull-right" href="/deletepakbon/{{ $verekening->id }}">Verwijderen</a> 
                  @endif
                </div>

                 @if($verekening->orders()->get()->isNotEmpty())

                <div class="card-body">

                <table class="dataTable table table-bordered order" id="dataTable" width="100%" cellspacing="0">
                  <tr>
                    <th>Datum</th>
                    <th>Totale prijs</th>
                    <th>Bekijk order</th>
                  </tr>             

                @foreach($verekening->orders()->get() as $order)

                <tr>
                    <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                    <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                    <td><a href="{{ route('bestelling', ['id' => $order->id]) }}">Bekijk order</a></td>
                  </tr>

               @endforeach


               </table> 


                </div>

                @endif

                <div class="card-footer">
                  <strong>Openstaand:</strong> € {{ $verekening->prijs }}
                </div>
              </div> 


      </div>

                    <div class="col-4">
      <div class="card">

          <div class="card-header">

            Geschiedenis

          </div>

          <div class="card-body">

            <table class="dataTable table">
              <thead>
                <th>Bedrag</th>
                <th>Datum</th>
              </thead>
              <tfoot>
                <th>Bedrag</th>
                <th>Datum</th>
              </tfoot>
              <tbody>
                @foreach($history as $histo)
                <tr>
                  <td>€{{ $histo->bedrag }}</td>
                  <td>{{ $histo->created_at->format('m/d/Y h:i:s') }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

          </div>

      </div>

      <div class="card">

        <div class="card-body">
            <strong>Openstaand:</strong> € {{ $verekening->prijs }}
        </div>

      </div>
              </div>

    </div>
 
@endsection

@section('js')


@endsection