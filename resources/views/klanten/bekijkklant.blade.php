@extends('layout.app')

@section('content')

    <div class="container-fluid">



  	<h3>Klant bekijken: {{ $klant->voornaam }} {{ $klant->achternaam }}</h3>


      <a href="/editklant/{{ $klant->id }}" class="btn btn-success">Klant aanpassen</a> &nbsp;

    @if($klant->hasAnything($klant->id))

                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
                  Verwijderen
                </button>

                     @endif

      <button type="button" data-val="{{ $klant->id }}" data-max="" class="btn pull-right btn-success" data-toggle="modal" data-target="#pakbonModal">Nieuwe pakbon</button>
      
      <p>

      @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

    </p>

      <br />

      <div class="row">

        <div class="col-sm-6">

      <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Klantgegevens</div>
        <div class="card-body">

          <div class="row">
            <div class="col-sm-6">
              <table>
              <tr>
                <td><b> Volledige naam: </b></td>
                <td>{{ $klant->voornaam }} {{ $klant->achternaam }}</td>
              </tr>
              <tr>
                <td><b> Bedrijfsnaam: </b></td>
                <td>{{ $klant->bedrijfsnaam }}</td>
              </tr>
              <tr>
                <td><b> Adres: </b></td>
                <td>{{ $klant->adres }} {{ $klant->huisnummer }}</td>
              </tr>
              <tr>
                <td><b> Stad: </b></td>
                <td>{{ $klant->stad }}</td>
              </tr>
              <tr>
                <td><b> Postcode: </b></td>
                <td>{{ $klant->postcode }}</td>
              </tr>
              <tr>
                <td><b> Telefoon: </b></td>
                <td>{{ $klant->telefoon }}</td>
              </tr>
              <tr>
                <td><b> E-mail: </b></td>
                <td><a href="mailto: {{ $klant->email }}">{{ $klant->email }}</a></td>
              </tr>

              </table>

            </div>
            <div class="col-sm-6">

            </div>
          </div>

        </div>
      </div>
@can('see credit')
            <div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-credit-card"></i> Kredietplafon</div>
        <div class="card-body">

          @if( ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100 >= 100)
           <div class="alert alert-danger" role="alert">
  Het limiet is bereikt. Er kan niet meer op krediet worden besteld.
</div>
          @else
          @if($klant->openstaand($klant->id) != 0)
          <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: {{ ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100 }}%;" aria-valuenow="{{ ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100 }}" aria-valuemin="0" aria-valuemax="100">{{ ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100 }}%</div>
          </div>
          @else
                    <div class="progress">
              <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
          </div>
          @endif

          @endif

          <p>
          <b>Kredietplafon: </b>€ {{ $klant->kredietplafon }} <br />
          <b>Openstaand: </b> € {{ $klant->openstaand($klant->id)}} <br />
          <b>Bestedingsruimte: </b> € {{ $klant->kredietplafon - $klant->openstaand($klant->id) }} 
          </p>


        </div>
      </div>
      @endcan

      <div class="card">

          <div class="card-header">

            Geschiedenis

          </div>

          <div class="card-body">

            <table class="dataTable table">
              <thead>
                <th>Bedrag</th>
                <th>Datum</th>
                <th>Bekijken</th>
              </thead>
              <tbody>
                @foreach($done as $verekening)

    
                <tr>
                  <td>€ {{ $verekening->verekeningTotaal($verekening->id) }}</td>
                  <td>{{ $verekening->created_at }}</td>
                  <td><a href="/bekijkverekening/{{ $verekening->id }}">Bekijk</a></td>
                </tr>

                @endforeach

                @foreach($history as $histo)

    
                <tr>
                  <td>€ {{ $histo->bedrag }}</td>
                  <td>{{ $histo->created_at }}</td>
                  <td><a href="/bestelling/{{ $histo->order_id }}">Bekijk</a></td>
                </tr>

                @endforeach

              </tbody>
            </table>

          </div>

      </div>

                  <div class="card">
            <div class="card-header">
              <i class="fa fa-bell-o"></i> Agenda <button type="button" class="btn btn-info btn-sm pull-right" data-toggle="modal" data-target="#modalAgenda">Nieuw item</button></div>
            <div class="list-group list-group-flush small">

             @foreach($agenda as $agenda)
              <a class="list-group-item list-group-item-action" href="/deleteagenda/{{ $agenda->id }}">
                <div class="media">
                  <div class="media-body">
                    <strong>{{ $agenda->content }}</strong>.
                    <div class="text-muted smaller">{{ $agenda->created_at->format('m/d/Y h:i:s') }}</div>
                    <div onclass="delete-agenda">Verwijderen</div>
                  </div>
                </div>
              </a>
              @endforeach

            </div>
          </div>

      <br />

    </div>
            <div id="klantorders" class="col-sm-6">

    @foreach($verekeningen as $verekening)
                  <div class="card">
                <div class="card-header">
                  <span style="font-weight: 600;">Verekening</span> van {{ $klant->bedrijfsnaam }} - {{ $verekening->created_at->format('m/d/Y h:i:s') }}
                  <a class="btn btn-success btn-small-height pull-right" href="/bekijkverekening/{{ $verekening->id }}">Bekijk verekening</a> &nbsp;
                  @if($verekening->prijs === 0)
                  <a class="btn btn-danger" href="/deletepakbon/{{ $verekening->id }}">Verwijderen</a>  &nbsp;
                  @endif
                </div>
                @if($verekening->orders()->get()->isNotEmpty())

                <div class="card-body">

                 <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <tr>
                    <th>Ordernummer</th>
                    <th>Totale prijs</th>
                    <th>Bekijk order</th>
                  </tr>             

                @foreach($verekening->orders()->get() as $order)
                            <tr>
                    <td>{{ $order->id }}</td>
                    <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                    <td><a href="{{ route('bestelling', ['id' => $order->id]) }}">Bekijk order</a>
                  </tr>
               @endforeach
               </table>  

             </div>

                   @endif
                <div class="card-footer">
                  <strong>Status:</strong> {{ $verekening->status($verekening->id) }} ( €{{ $verekening->prijs }} )
                <button type="button" data-val="{{ $verekening->id }}" data-max="{{ $verekening->prijs }}" class="btn pull-right btn-small-height btn-success" data-toggle="modal" data-target="#voltooiModal">
  Voltooien
</button>
                </div>
              </div> <br />
    @endforeach

    @if($orders->isEmpty())

    <div class="alert alert-primary" role="alert">
  Er zijn geen actieve bestellingen gevonden.
</div>

    @endif



    @foreach($orders as $bestelling)

              <div class="card">
                <div class="card-header">
                  <span style="font-weight: 700">Bestelling</span> van {{ $bestelling->klant()->first()->bedrijfsnaam }} - {{ $bestelling->created_at->format('m/d/Y h:i:s') }}
                  @can('edit order')
                  <button onclick="edit({{ $bestelling->id }});" class="pull-right btnorder btnorder{{$bestelling->id}}  btn btn-small-height btn-success">Order aanpassen</button>
                  @endcan
                </div>
                <div class="card-body">

                  <div class="edit edit{{$bestelling->id}}">
                    <div id="external{{$bestelling->id}}"></div>
  
                  </div>

     


                           

 <table class="dataTable table table-bordered order order{{$bestelling->id}}" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Aantal</th>
                  <th>Stukprijs</tth>
                  <th>Totaal</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Verzendkosten:</th>
                  <th>€ {{ $bestelling->verzendkosten }}</th>
                </tr>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>Totaal:</th>
                  <th>€ {{ $bestelling->order_total($bestelling->id) + $bestelling->verzendkosten }}</th>
                </tr>
              </tfoot>
              <tbody>
               @foreach($bestelling->products()->get() as $product)
                <tr>
                  <td>{{ $product->sku }}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $bestelling->aantal($product->id, $bestelling->id) }}</td>
                  <td>€ {{ $bestelling->stukprijs($product->id, $bestelling->id) }}</td>
                  <td>€ {{ $bestelling->prijs($product->id, $bestelling->id) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>

            @if(!empty($bestelling->note))

            <div id="noteinklant">

            {{ $bestelling->note}}

          </div>

          @endif


                </div>
                <div class="card-footer">
                  <strong>Status:</strong> {{ $bestelling->status($bestelling->id) }} ( €{{ $bestelling->totalprijs($product->id, $bestelling->id) + $bestelling->verzendkosten - $bestelling->betaald }} ) @if(!empty($bestelling->betaald)) - Reeds betaald: € @endif {{ $bestelling->betaald }}
                  <span class="float-right">

                    @if($bestelling->getTracking($bestelling->id) != 'null')
                     <a href="{{ $bestelling->getTracking($bestelling->id) }}" target="_blank" class="btn btn-small-height btn-warning">Traceren</a> &nbsp;

                     @endif

                  @if(is_null($bestelling->betaald))
                     <a class="btn btn-small-height  btn-warning" href="/factureren/{{ $bestelling->id }}">Factureren</a>
                     @endif

                    <a href="{{ route('bestelling', ['id' => $bestelling->id]) }}" class="btn btn-small-height btn-success">Bekijk bestelling</a>  &nbsp;

                                    <button type="button" data-val="{{ $bestelling->id }}" data-max="{{ $bestelling->totalprijs($product->id, $bestelling->id) - $bestelling->betaald }}" class="btn btn-small-height pull-right btn-success" data-toggle="modal" data-target="#voltooiOrderModal">
  Voltooien
</button>
                  </span>
                </div>
              </div>

              <br />

              @endforeach


              @if($orders->isNotEmpty() || $verekeningen->isNotEmpty() )
              <div>
                <br />
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#verekenModal">
                  Deze bestellingen verekenen
                </button> 

                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#selectModal">
                  Selecteren
                </button>
              </div> <br />
              @endif




    </div>

  </form>

  </div>

  <!-- Modal -->
<div class="modal fade" id="selectModal" tabindex="-2" role="dialog" aria-labelledby="selectModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Selecteer orders</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form method="post" action="{{ action('VerekeningenController@voltooiSelected', ['som' => $verekensum, 'id' => $klant->id]) }}">
          @csrf

                           <table class="dataTable table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <tr>
                    <th>Check</th>
                    <th>Datum</th>
                    <th>Bedrag</th>
                  </tr>             
                    @foreach($ordersclean as $order) 
                  <tr>
                    <td><input id="verekeningselect" class="verekeningselect big-checkbox" name="verekeningselect[]" data-bedrag="{{ $order->order_total($order->id) + $order->verzendkosten }}" type="checkbox" value="{{ $order->id }}"></td>
                    <td>{{ $order->created_at->format('m/d/Y h:i:s') }}</td>
                    <td>€ {{ $order->order_total($order->id) + $order->verzendkosten }}</td>
                  </tr>
               @endforeach
               <tr>
                <td></td>
                <td>Totaal:</td>
                <td>€ <span class="totalsumselect"></span></td>
               </tr>
               </table>
      
          <input style="visibility: hidden" name="verekeningid" class="verekeningid" id="verekeningid" value="">
          <div class="form-group"> <br /><p>
            <input class="form-control" type="text" name="verekengetal" onkeyup="this.value=this.value.replace(',',''); this.value=this.value.replace('/','');" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid"><p></p><br>
                        <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-2" role="dialog" aria-labelledby="deleteModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Klant verwijderen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ action('KlantController@deleteKlant', ['id' => $klant->id]) }}"> <br />
          @csrf
          <input style="visibility: hidden" name="verekeningid" class="verekeningid" id="verekeningid" value="">
          <div class="form-group">
        <input type="password" placeholder="Password" class="form-control" id="password1" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password1">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Verwijder nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="verekenModal" tabindex="-1" role="dialog" aria-labelledby="verekenModalTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="verekenModalTitle">Bestellingen verekenen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Sluiten">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <b>Totaal openstaand: € {{ $verekensum }} </b> <br /> <br />
        <form method="post" action="{{ action('VerekeningenController@verekenen', ['som' => $verekensum, 'id' => $klant->id]) }}">
          @csrf
          <div class="form-group">
            <input class="form-control" type="text" max="{{ $verekensum }}" name="verekengetal" placeholder="Hoeveel moet er worden verekend">
          </div>
                      <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password2" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password2">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Vereken nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="voltooiModal" tabindex="-2" role="dialog" aria-labelledby="voltooiModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verekening voltooien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ action('VerekeningenController@voltooiVerekening', ['som' => $verekensum, 'id' => $klant->id]) }}"> <br />
          @csrf
          <input style="visibility: hidden" name="verekeningid"  class="verekeningid" id="verekeningid" value="">
          <div class="form-group">
            <input class="form-control" type="text" onkeyup="this.value=this.value.replace(',','.')" max="0" name="verekengetal" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid"> <br />
                        <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password3" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password3">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="voltooiOrderModal" tabindex="-2" role="dialog" aria-labelledby="voltooiOrderModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Verekening voltooien</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ action('VerekeningenController@voltooiOrder', ['som' => $verekensum, 'id' => $klant->id]) }}">
          @csrf
          <input style="visibility: hidden" name="orderid" class="orderid" id="orderid" value="">
            <input style="visibility: hidden" name="klantid" class="klantid" id="klantid" value="{{ $klant->id }}">
          <div class="form-group">
            <input class="form-control" type="text" max="0" onkeyup="this.value=this.value.replace(',','.'); this.value=this.value.replace('/','');" name="verekengetal" id="voltooigetal" class="voltooigetal" placeholder="Hoeveel moet er worden voltooid" required>
            <b>Bevestigingswachtwoord:</b> 
            <br />
        <input type="password" placeholder="Password" class="form-control" id="password4" required>
        <input type="password" style="visibility: hidden;"  placeholder="Confirm Password" id="confirm_password4">
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Voltooi nu</button>
        </form>
      </div>
    </div>
  </div>
</div>

</div>

      <!-- Modal -->
  <div class="modal fade" id="modalAgenda" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="{{ action('KlantController@newagendaklant') }}" class="" method="post">

            @csrf

            <div style="display: none;" class="form-group">
              <input class="form-control" name="klantid" value="{{ $klant->id }}" type="text">
            </div>

            <div class="form-group">
              <input class="form-control" name="content" placeholder="Bericht" type="text">
            </div>

            <div class="form-group">
                <input class="form-control" name="tijd" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
            </div>

            <button class="btn btn-default" type="submit">Opslaan</button>

          </form>

         </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Sluiten</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal -->
<div class="modal fade" id="pakbonModal" tabindex="-2" role="dialog" aria-labelledby="pakbonModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Pakbon toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ action('VerekeningenController@newPakbon', ['id' => $klant->id]) }}">
          @csrf
          <div class="form-group">
            <input class="form-control" type="text" name="bedrag" id="bedrag" class="bedrag" placeholder="Bedrag pakbon"> <br />
                      <input class="form-control" type="date" max="0" name="created_at" id="created_at" class="created_at">

          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Toevoegen</button>
        </form>
      </div>
    </div>
  </div>
</div>
 
@endsection

@section('js')

<script>

$('#voltooiOrderModal').on('show.bs.modal', function (event) {
  var max = $(event.relatedTarget).data('max');
  var myVal = $(event.relatedTarget).data('val');
  $(this).find(".orderid").val(myVal);
  $(this).find("#voltooigetal").attr('max', max);

});

</script>

<script>

$('#voltooiModal').on('show.bs.modal', function (event) {
  var max = $(event.relatedTarget).data('max');
  var myVal = $(event.relatedTarget).data('val');
  $(this).find(".verekeningid").val(myVal);

  $(this).find("#voltooigetal").attr('max', max);

});

</script>
<script>
  

  $('.')

  $('.edit').each(function() {
    $(this).hide();
  });

function edit(id)
{

  $('.edit').each(function() {
    $(this).hide();
  });

  // $('.dataTables_wrapper').each(function() {
  //  $(this).parents('div.dataTables_wrapper').first().show();
  //  $(this).show();
  // });

  $('.order'+id+'').hide();
  $('.btnorder'+id+'').hide();
  $('.edit'+id+'').show();
  $('#external'+id+'').load("/editorder/"+id+"");
  $('.btnorder').hide();

}

</script>

<script>
var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password1")
  , confirm_password = document.getElementById("confirm_password1");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password2")
  , confirm_password = document.getElementById("confirm_password2");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password3")
  , confirm_password = document.getElementById("confirm_password3");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>
var password = document.getElementById("password4")
  , confirm_password = document.getElementById("confirm_password4");

function validatePassword(){
  if(password.value != '67775888') {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;
</script>

<script>

  var sum = 0;

  $('.verekeningselect').on('change', function(){ // on change of state
   if(this.checked) // if changed state is "CHECKED"
    {

      sum = parseFloat(sum) + parseFloat($(this).attr("data-bedrag"));

        // do the magic here
    } else {

       sum = parseFloat(sum) - parseFloat($(this).attr("data-bedrag"));

    }

    $('.totalsumselect').text('');

    $('.totalsumselect').text(parseFloat(sum).toFixed(2));
});

     
     $('body').on('change', '.product', function () {
   
           $.ajaxSetup({
               headers:
               { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
           });
   
     $.ajax
   ({
     type: "POST",
     url: "/getprice/"+$(this).val()+"",
     async: false,
   
     success: function(response)
     {
        setPrice(response);
   
            calculateTotaal();
   
     }
   });
   
   
   // $(this).closest('td').find('.prijs').attr('value', price);
   $(this).closest('td').nextAll(':lt(2)').slice(1).find('div > input').val(price);

   @if(Auth::user()->id != 6 || Auth::user()->id != 7)

   $(this).closest('td').nextAll(':lt(2)').slice(1).find('div > input').attr('min', price);
   
   @endif

   });

</script>


@endsection