@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Klanten</h3>

      @if (session('status'))
        <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('status') }}
        </div>
      @endif

<div class="card mb-3">

      <div class="card-header">
        <i class="fa fa-table"></i> Alle klanten</div>
        <div class="card-body">

          <p><a class="btn btn-success" href="{{ route('newklant') }}">Nieuwe klant</a></p>

          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Bedrijfsnaam</th>
                  <th>E-mail adres</th>
                  <td>Bekijken</td>
                </tr>
              </thead>
              <tbody>
                @foreach($klanten as $klant)
                <tr>
                  <td>{{ $klant->bedrijfsnaam }}</td>
                  <td>{{ $klant->email }}</td>
                  <td><a href="{{ route('bekijkklant', ['id' => $klant->id]) }}" class="btn btn-success">Bekijken</a>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Delete -->
    <div id="deleteModal" class="modal fade">
      <div class="modal-dialog modal-confirm">
        <div class="modal-content">
          <div class="modal-header">    
            <h4 class="modal-title">Zeker weten?</h4>  
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
            <p>Weet je zeker dat je deze categorie wilt verwijderen?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
            <a href="" id="deleteButton" class="btn btn-danger">Delete</a>
          </div>
        </div>
      </div>
    </div>  

@endsection

@section('js')
<script>

// Delete modal logica

var delUrl;

$('.deletemodal').click(function() {
   delUrl = $(this).attr('data-url'); 
   alert(delUrl);
});

$('#deleteModal').on('show.bs.modal', function (e) {
    $(this).find('#deleteButton').prop("href", delUrl)
});

</script>
@endsection