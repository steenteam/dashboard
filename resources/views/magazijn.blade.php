@extends('layout.app')

@section('content')

    <div class="container-fluid">

    	<h3>Magazijn</h3>

      <div class="row">

<div class="card card-magazijn mb-3 col-sm-6">
      <div class="card-header">
        <i class="fa fa-table"></i>Voorraad</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table dataTable table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>SKU</th>
                  <th>Productnaam</th>
                  <th>Locatie</th>
                  <th>Aantal op voorraad</th>
                </tr>
              </thead>
              <tbody>
                @foreach($product as $product)
                <tr>
                  <td>{{ $product->sku }}</td>
                  <td>{{ $product->name }}</td>
                  <td><a href="/edithetmagazijn/{{ $product->id }}">{{ $product->locatie }}</a></td>
                  <td>{{ $product->getStock($product->id) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted"></div>
      </div>

<div class="card card-magazijn mb-3 col-sm-6">
      <div class="card-header">
        <i class="fa fa-table"></i>Magazijnkaart</div>
        <div class="card-body">

          <img src="https://picqer.com/images/blog/locatienummer-magazijn-voorbeeld-2@2x.png">


        </div>
        <div class="card-footer small text-muted"></div>
      </div>
      
    </div>

  </div>

@endsection