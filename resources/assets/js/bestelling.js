
   
   make();
   
   /* Variables */
   var p = $("#bestellings").val();
   var row = $(".bestellingRow");
   
   /* Functions */
   function getP(){
     p = $("#bestellings").val();
   }
   
   function addRow() {
     destroy();
   
     row.clone(true, true).appendTo("#bestellingTable");
   
     make();
   }
   
   function removeRow(button) {
     button.closest("tr").remove();
     lessTotal(button.closest("tr").find('#totalrow').val());
   }
   /* Doc ready */
   $(document.body).on('click', '.add' ,function(){
     getP();
     if($("#bestellingTable tr").length < 17) {
       addRow();
       var i = Number(p)+1;
       $("#bestellings").val(i);
     }
     $(this).closest("tr").appendTo("#bestellingTable");
     if ($("#bestellingTable tr").length === 3) {
       $(".remove").hide();
     } else {
       $(".remove").show();
     }
   });
   $(document.body).on('click', '.remove' ,function(){
     getP();
     if($("#bestellingTable tr").length === 1) {
       //alert("Can't remove row.");
       $(".remove").hide();
     } else if($("#bestellingTable tr").length - 1 ==1) {
       $(".remove").hide();
       removeRow($(this));
       var i = Number(p)-1;
       $("#bestellings").val(i);
     } else {
       removeRow($(this));
       var i = Number(p)-1;
       $("#bestellings").val(i);
     }
   });
   $("#bestellings").change(function () {
     var i = 0;
     p = $("#bestellings").val();
     var rowCount = $("#bestellingTable tr").length - 2;
     if(p > rowCount) {
       for(i=rowCount; i<p; i+=1){
         addRow();
       }
     //  $("#bestellingTable #addButtonRow").appendTo("#bestellingTable");
     } else if(p < rowCount) {
     }
   });
   
   $('body').on('change', '#aantal', function () {
   
   
       var aantal = $(this).val();
       var that = $(this),
           prijs =  that.closest('tr').find('#prijs').val();
       var totaal = aantal * prijs;
   
       var totaalnew = parseFloat(totaal).toFixed(2);
   
       prijs =  that.closest('tr').find('#totalrow').html(totaalnew);
   
       prijs =  that.closest('tr').find('#totalrow').val(totaalnew);
   
       calculateTotaal();
   
   });
   
     $('body').on('change', '#prijs', function () {
      
       var aantal = $(this).val();
       var that = $(this),
           prijs =  that.closest('tr').find('#aantal').val();
       var totaal = aantal * prijs;
   
       var totaalnew = parseFloat(totaal).toFixed(2);
   
       prijs =  that.closest('tr').find('#totalrow').html(totaalnew);
   
       prijs =  that.closest('tr').find('#totalrow').val(totaalnew);
   
       calculateTotaal();
   });
   
   
   $('.rowtotal').val('0');
   
   function calculateTotaal()
   {
   
   var sum = 0;
   
   $('.rowtotal').each(function(){
       sum += parseFloat($(this).val());
   });
   
   var sumtest = parseFloat(sum).toFixed(2);
   
   $('#sumTotal').html(sumtest);
   
   $('#sumTotal').val(sumtest);
   
   }
   
   function lessTotal(less)
   {
     var newp = $('#sumTotal').val() - less;
   
     $('#sumTotal').html(newp);
   
   }
   
   function addVal(totaal)
   {
   
     $('.rowtotal').each(function(){
   
       if($(this).val() !== 0) {
   
       $(this).val(0);
   
     }
   
     });
   
   }
   
   var a = 0;
   
   function destroy()
   {
   
   
   $('.product').each(function(){
   
    // $(this).selectize()[0].selectize.destroy();
   
    // $(this).removeClass('selectize');
   
    a++;
   
   });
   
   }
   
      $('.klantkies').selectize({
       sortField: 'text',
       placeholder: 'Zoek naar klanten..'
   });
   
   
   function make()
   {
      $('.product').selectize({
       sortField: 'text'
   });
   }

   var resp;
   
   function setResp(amount, name) {
     resp = amount;
     $('#stockstock').show();
     $('#productnaam').html(name);
     $('#stockhoeveelheid').html(resp);
      $('#stockhoeveelheid').removeClass();
     if(resp < 10) {
      $('#stockhoeveelheid').addClass('redalert');
     } else if(resp < 50) {
      $('#stockhoeveelheid').addClass('orangealert');
     } else {
      $('#stockhoeveelheid').addClass('greenalert');
     } 
   }
   
   var inkoop;
   
   function setInkoop(amount) {
     inkoop = amount;
   }
   

    $('body').on('change', '.product', function () {
   
      $.ajax({
     type: 'GET',
     url: '/getstock/'+$(this).val()+'',
     data: "",
     async: false
   }).done(function(response) {
       setResp(response['response'], response['name']);
     });
   
   // $(this).closest('tr').find('.aantal').attr('max', resp);
   
   
      $.ajax({
     type: 'POST',
     url: '/getinkoop/'+$(this).val()+'',
     data: "",
     async: false
   }).done(function(response) {
       setInkoop(response['response']);
     });
   
      $(this).closest('tr').find('.prijs').attr('value', inkoop);
   
   });
   
   getKrediet($('#klant').find("option:first").attr("selected", true).val());
   
   function getKrediet(id) {
   
     $('#melding').html('');
   
           $.ajaxSetup({
               headers:
               { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
           });
   
     $.ajax
   ({
     type: "POST",
     url: "/getkrediet/"+id+"",
   
     success: function(html)
     {
        $('#melding').html(html.html);
     }
   });
   
   } 
   
   
   function checkadres(that) {
           if (that.value == "2") {
               document.getElementById("anderadres").style.display = "block";
           } else {
               document.getElementById("anderadres").style.display = "none";
           }
       }
   
   function checkvoltooid(that) {
   
           if (that.value == "1") {
   
               document.getElementById("verzendmethodegroup").style.display = "none";
               document.getElementById("productenvoorraadgroup").style.display = "none";
               document.getElementById("pakbongroup").style.display = "none";
               document.getElementById("adresandersgroup").style.display = "none";
   
          }
   
           if (that.value == "2") {
   
               document.getElementById("betaaldbedrag").style.display = "block";
               document.getElementById("passwordbevestig").style.display = "block";
               document.getElementById("verzendmethodegroup").style.display = "block";
               document.getElementById("productenvoorraadgroup").style.display = "block";
               document.getElementById("pakbongroup").style.display = "block";
               document.getElementById("adresandersgroup").style.display = "block";
   
           } 
   
            if (that.value == "0") {
   
               document.getElementById("betaaldbedrag").style.display = "none";
               document.getElementById("passwordbevestig").style.display = "none";
               document.getElementById("verzendmethodegroup").style.display = "block";
               document.getElementById("productenvoorraadgroup").style.display = "block";
               document.getElementById("pakbongroup").style.display = "block";
               document.getElementById("adresandersgroup").style.display = "block";
   
           }
       }
   
   var price;
   
   function setPrice(amount) {
     price = amount;
   }

   
   
     $('body').on('change', '#product', function () {
   
   
       var that = $(this),
           prijs =  that.closest('tr').find('#prijs').val();
   
       var aantal = that.closest('tr').find('#aantal').val();
   
       var totaalpre = aantal * prijs;
   
       var totaal = parseFloat(Math.round((totaalpre * 100) / 100)).toFixed(2);
   
       prijs =  $(this).closest('tr').find('#totalrow').html(totaal);
   
       prijs =  $(this).closest('tr').find('#totalrow').val(totaal);
   
       calculateTotaal();
   
   });


    $('#verzendmethode').on('change', function() {
  if($(this).val() == 'ophalen') {
    $('#verzendkosten').val('0.00');
  } else {
    $('#verzendkosten').val('6.00');
  }
});

        $('#status').on('change', function() {
  if($(this).val() == '0') {
    $('#neworder').html('Bestelling plaatsen');
    alert
  } 
  if($(this).val() == '1') {
    $('#neworder').html('Bestelling on-hold zetten');
  } 
  if($(this).val() == '2') {
    $('#neworder').html('Bestelling voltooien');
  } 

});

        $('#orderform').submit(function() {
          $('#neworder').attr('disabled', true);
        });