<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Voorraad;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

use DB;

class Product extends Model
{

	use Cachable;
	use SoftDeletes;

	protected $fillable = ['name', 'description', 'shortdescription', 'type', 'wcid', 'merk', 'soort'];

	protected $dates = ['deleted_at'];

    // Return de afbeeldingen van een product artikel
	public function images()
	{

		return $this->hasMany('App\ProductImage');

	}

	// Return de categorieën van een product artikel
	public function categorie()
	{

		return $this->belongsToMany('App\Categorie', 'product_categorie');
	
	}

	public function order()
	{

        return $this->belongsToMany('App\Order', 'order_product');

	}

	public function prijs($id)
	{

			$aantal = DB::select('select aantal from order_product where product_id = '.$id.'');

			$prijs = DB::select('select nieuwprijs from order_product where product_id = '.$id.'');

			$totalsum = $aantal[0]->aantal * $prijs[0]->nieuwprijs;

			return $totalsum;

	}

	public function stukprijs($id)
	{
			$prijs = DB::select('select nieuwprijs from order_product where product_id = '.$id.'');

	    	return $prijs[0]->nieuwprijs; 

	}

	public function aantal($id)
	{

			$aantal = DB::select('select aantal from order_product where product_id = '.$id.'');

			return $aantal[0]->aantal;

	}

	public function getStock($id)
	{

		$aantal = 0;

		$voorraads = Voorraad::where('product_id', $id)->get();

		foreach($voorraads as $voorraad) {

			$aantal = $aantal + $voorraad->aantal;

		}

		return $aantal;

	}



		public function getInkoopRaw($id)
		{

			$voorraad = Voorraad::where([
				['product_id', '=', $id],
				['aantal', '>', '0']
			])->orderBy('created_at')->first();

		

			if( !empty($voorraad->inkoopprijs)) {

				echo round($voorraad->inkoopprijs, 2);

			} else {

			echo '?';

				}

		}

	public function subProduct($id, $aantal)
	{

		$hoeveelheid = $aantal;

		$voorraads = Voorraad::where('product_id', $id)->orderBy('aantal')->get();

		foreach($voorraads as $voorraad) {

			if($voorraad->aantal <= $aantal) {

				$first = $aantal - $voorraad->aantal;

				$voorraad->delete($voorraad->id);

			}

			if($voorraad->aantal > $aantal) {

				$voorraad->aantal = $voorraad->aantal - $first;

				$voorraad->save();

			}

		}

	}

}
