<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Verekening extends Model
{

	protected $fillable = ['klant_id', 'status', 'prijs'];

	protected $table = 'verekeningens';

	public function status($id)
	   {

	    $verekening = Verekening::find($id);

	    $status = array(
	    	"Openstaand",  // 0
	    	"Betaald", // 1
	    );

	    return $status[$verekening->status];

	}

	public function orders()
	{

       return $this->belongsToMany('App\Order', 'verekening_order');

	}

	public function verekeningTotaal($id)
	{

		$sum = 0;

		$orders = DB::table('verekening_order')->where([
			['verekening_id', '=', $id]
		])->get();


		foreach($orders as $order)
		{	

		$products = DB::select('select * from order_product where order_id = '.$order->order_id.'');


		$shipping = Order::find($order->order_id);

		foreach($products as $product) {

			$total = ($product->aantal * $product->nieuwprijs); 

			$sum = $sum + $total;
		}

		$sum = $sum + $shipping->verzendkosten;


		}

		return $sum;

	}

}
