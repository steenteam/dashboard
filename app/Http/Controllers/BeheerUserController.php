<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class BeheerUserController extends Controller
{

	use RegistersUsers;

	// Overzicht van gebruikers tonen
	public function index()
	{

		$users = User::all();

		return view('userbeheer.userbeheer')->with(compact('users'));

	}

	// Nieuwe gebruiker aanmaken
	public function new()
	{

		return view('userbeheer.newuser');

	}
	// Nieuwe user logica

	public function create(Request $request)
	{

        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);


		if ($v->fails())
        {
          return redirect('userbeheer')->with('error', 'Er is wat fout gegaan.');
        }

		return User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'avatar' => 'aa'
        ]);

        activity()->log('User '.$request->input('name').' gemaakt.');

        return redirect('userbeheer')->with('status', 'De gebruiker is succesvol aangemaakt.');

	}

    // User laten zien
	public function show($id)
	{

		$user = User::find($id);

		$activity = Activity::causedBy($user)->get();

		$logs = Activity::causedBy($user)->orderBy('id', 'desc')->take(6)->get();

		$permissions = Permission::all();

		$permissionsuser = $user->permissions;

	//	$user->givePermissionTo('edit order');

	    $userroles = $user->getRoleNames();

		$roles = Role::all();

		return view('userbeheer.bekijkuser')->with(compact('user', 'activity', 'logs', 'userroles', 'roles', 'permissions', 'permissionsuser'));

	}

	 // Delete logica van gebruikers.
	public function delete($id)
	{

		$user = User::find($id);

		$user->delete();

		activity()->log('Gebruiker '.$user->name.' verwijderd.');

		return redirect('userbeheer')->with('status', 'De user is succesvol verwijderd.');

	}

	public function saveRoles(Request $request, $id)
    {

 	    $user = User::find($id);

 	    $allroles = Role::all();

 	    foreach($allroles as $all) {

 	    	if($user->hasRole($all->name)) {
 	    		$user->removeRole($all->name);
 	    	}

 	    }

        $roles = $request->roles;

        if(!empty($roles)) {

        foreach($roles as $role) {

        	$user->assignRole($role);

        }
    }

        return response()->json(['success'=>'De groepen zijn succesvol opgeslagen.']);
    }

	public function savePermissions(Request $request, $id)
    {

 	    $user = User::find($id);

 	    $allpermissions = Permission::all();

        $permissions = $request->permissions;

        $permissionsuser = $user->permissions;

        foreach($allpermissions as $allp) {

        	if($user->hasPrivatePermission($allp->name, $user->id)) {
        		$user->revokePermissionTo($allp->name);
        	}

        }

        if(!empty($permissions)) {

        foreach($permissions as $permission) {
        	$user->givePermissionTo($permission);
        }

    }

        return response()->json(['success'=>'De permissies zijn succesvol opgeslagen.']);
    }



	
    //
}
