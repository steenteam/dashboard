<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Verekening;
use App\Klant;
use App\KlantGeschiedenis;
use App\VerekeningGeschiedenis;

use URL;
use Illuminate\Support\Facades\Input;

class VerekeningenController extends Controller
{

	public function newPakbon($id, Request $request)
	{

	    $pakbon = new Verekening();	

	    $pakbon->prijs = $request->input('bedrag');
	    $pakbon->created_at = $request->input('created_at');
	    $pakbon->klant_id = $id;
	    $pakbon->status = 0;

	    $pakbon->save();

	     return redirect(URL::previous());

	}

	public function deletePakbon($id)
	{

		$verekening = Verekening::find($id);

		$verekening->delete();

		return redirect(route('bestellingen'))->with('status', 'De verekening is succesvol verwijderd.');

	}

	public function verekenen($som, $id, Request $request)
	{

		$orders = Order::where([
			['klant_id', $id],
			['status', '0']
		])->get();

		foreach($orders as $order) {

			$order->status = 2;

			$order->save();

		}

		$verekeningen = Verekening::where([
			['klant_id', $id],
			['status', '0']
		])->get();

		foreach($verekeningen as $verekening) {

			$verekening->status = 2;

			$verekening->save();

		}

		$totaalover = $som - $request->input('verekengetal');

		$verekening = new Verekening();

		$verekening->prijs = $totaalover;

		$verekening->status = 0;

		$verekening->klant_id = $id;

       	$verekening->save();

		$verekening->orders()->attach($orders);

		// Geschiedenis

	    	    $history2 = new VerekeningGeschiedenis();

	    $history2->klant_id = $verekening->id;

	    $history2->bedrag = $request->input('verekengetal');

	    $history2->save();


		activity()->log('Heeft een nieuwe verekening gemaakt.');

	 	return redirect(URL::previous());

	}

	public function voltooiSelected(Request $request)
	{

		$debug = false;

		$ids = array();

		$som = 0;

		foreach($request->input('verekeningselect') as $selected)
		{

			array_push($ids, $selected);

			
		if($debug == false) {

			$order = Order::find($selected);

			$order->status = 2;

			$som = $som + ($order->order_total($order->id) + $order->verzendkosten);

			$order->save();

		}
		
		}

		$id = Input::get('id', '0');

	    $orders = Order::findMany($ids);

		$totaalover = $som - $request->input('verekengetal');

		$verekening = new Verekening();

		$verekening->prijs = $totaalover;


			if($request->input('verekengetal') == $som) {

			$verekening->status = 1;

					$verekening->klant_id = $id;

       	$verekening->save();

        $history2 = new VerekeningGeschiedenis();

	    $history2->klant_id = $verekening->id;

	    $history2->bedrag = $request->input('verekengetal');

	    $history2->save();

		} else {

			$verekening->status = 0;

					$verekening->klant_id = $id;

       	$verekening->save();

		}

		$verekening->orders()->attach($orders);


	 	 return redirect(URL::previous());

	 	}

	public function voltooiVerekening(Request $request)
	{	

		$id = $request->input('verekeningid');

		$bedrag = $request->input('verekengetal');

		// Verekening

		$verekening = Verekening::find($id);

		if($bedrag > $verekening->prijs) {
			return redirect(URL::previous())->with('status', 'Het bedrag van de verekening is te hoog.');
		}

		if($verekening->prijs == $bedrag) {

			$verekening->status = 1;

			$verekening->prijs = 0;


	    $verekening->save();


		} else {

		$verekening->prijs = $verekening->prijs - $bedrag;


	    $verekening->save();

	    }


	    $history2 = new VerekeningGeschiedenis();

	    $history2->klant_id = $verekening->id;

	    $history2->bedrag = $bedrag;

	    $history2->save();

	    // Geschiedenis

	 	return redirect(URL::previous());

	}

	public function voltooiOrder(Request $request)
	{

		$id = $request->input('orderid');

		$klantid = $request->input('klantid');

		$bedrag = $request->input('verekengetal');

		$order = Order::find($id);

		$orderleft = ($order->order_total($order->id) + $order->verzendkosten) - $order->betaald;

		if($bedrag > $orderleft) {
			return redirect(URL::previous());
		}

		$order->betaald = $order->betaald + $bedrag;

		$orderbedrag = ($order->betaald + $bedrag);

		if($orderleft == $request->input('verekengetal')) {

			$order->status = '2';

		    $history = new KlantGeschiedenis();

		    $history->klant_id = $klantid;

		    $history->order_id = $id;

		    $history->bedrag = $order->order_total($order->id) + $order->verzendkosten;

		    $history->save();

		}

			$order->save();

		// Geschiedenis

	 	return redirect(URL::previous());

	}

	public function bekijkVerekening($id)
	{

		$verekening = Verekening::find($id);

		$klant = Klant::find($verekening->klant_id);

		$history = VerekeningGeschiedenis::orderBy('created_at', 'ASC')->where('klant_id', $id)->get();

		return view('bekijkverekening')->with(compact('verekening', 'klant', 'history'));

	}
    //
}
