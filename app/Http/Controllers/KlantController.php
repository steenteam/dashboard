<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Klant;

use Illuminate\Support\Facades\Input;

use App\Order;
use App\Verekening;
use App\Product;
use App\KlantGeschiedenis;
use App\KlantAgenda;

use URL;

use Auth;


use Redirect;

class KlantController extends Controller
{

	public function deleteKlant()
	{

		$klant = Klant::find(Input::get('id'));

		$klant->delete();

		return redirect(route('klanten'))->with('status', 'Klant succesvol verwijderd.');


	}
 
    // Overzicht van alle klanten
 	public function index()
	{

		$klanten = Klant::all();

		return view('klanten.klanten')->with(compact('klanten'));

	}

    // Het deleten van Geschiedenis van een klant
    public function delete($id)
	{

		$geschiedenis = KlantGeschiedenis::find($id);

		$geschiedenis->delete();

		return redirect(URL::previous());
		
	}

	public function edit($id)
	{

		$klant = Klant::find($id);

		return view('klanten.editklant')->with(compact('klant'));

	}

	public function modify(Request $request, $id)
	{

		$klant = Klant::find($id);

		$klant->fill($request->all());

				if(!empty($request->input('checkkrediet'))) {
			$klant->kredietplafon = $request->input('maxkrediet');
		} else {
			$klant->kredietplafon = 0;
		}

		$klant->huisnummer = $request->input('huisnummer');
		
		$klant->factureren = $request->input('checkfactuur');


		$klant->save();

		activity()->log('Klant '.$klant->name.' aangepast.');

			return redirect('klanten')->with('status', 'De klant is succesvol aangepast.');

	}
    
    // Een nieuwe klant maken
	public function new()
	{
		
		return view('klanten.newklant');

	}

	public function create(Request $request)
	{

		$klant = new Klant(Input::all());

		$newid = Klant::all()->count();

		if(!empty($request->input('checkkrediet'))) {
			$klant->kredietplafon = $request->input('maxkrediet');
		} else {
			$klant->kredietplafon = 1;
		}

		$klant->huisnummer = $request->input('huisnummer');

		$klant->factureren = $request->input('checkfactuur');

		$klant->save();

		activity()->log('Klant '.$klant->name.' gemaakt.');

		return redirect('klanten')->with('status', 'De klant is succesvol aangemaakt.');

	}

	public function deleteagenda($id)
	{

		$agenda = KlantAgenda::find($id);

		$agenda->delete();

		return redirect(URL::previous());

	}

		public function newagendaklant(Request $request)
		{

		$agenda = new KlantAgenda();

		$agenda->content = $request->input('content');

		$agenda->tijd = $request->input('tijd');

		$agenda->klant_id = $request->input('klantid');

		$agenda->save(); 

		return redirect(URL::previous());

	}


	public function show($id)
	{

		$klant = Klant::find($id);

		$agenda = KlantAgenda::where('klant_id', $id)->get();

		$orders = Order::where([
			['klant_id', $id],
			['status', '0'],
			['factuur', null]
		])->orderBy('created_at', 'desc')->get();

		$ordersclean = Order::whereNull('betaald')->where([
			['klant_id', $id],
			['status', '0'],
			['factuur', null]
		])->orderBy('created_at', 'desc')->get();
		
		$verekeningen = Verekening::where([
			['klant_id', $id],
			['status', '0']
		])->orderBy('created_at', 'desc')->get();

	     $done = Verekening::where([
			['klant_id', $id],
			['status', '1']
		])->orderBy('created_at', 'desc')->get();


		$verekensum = 0;

		foreach($ordersclean as $order) {

			$verekensum = $verekensum + $order->order_total($order->id) + $order->verzendkosten;

		}

		$history = KlantGeschiedenis::orderBy('created_at', 'ASC')->where('klant_id', $id)->get();

		$products = Product::all();

		$verekeningen = Verekening::where([
			['klant_id', $id],
			['status', 0]
		])->get();

		return view('klanten.bekijkklant')->with(compact('klant', 'orders', 'verekensum', 'verekeningen', 'products', 'history', 'verekeningen', 'done', 'ordersclean', 'agenda'));

	}

	public function isKredietWaardig($id)
	{

		$klant = Klant::find($id);

		$waardigheid = ($klant->openstaand($klant->id) / $klant->kredietplafon) * 100;

	 if($waardigheid >= 100) {
	    return response()->json(['html'=>'<div class="alert alert-danger"><strong>Let op!</strong> Deze klant heeft zijn kredietlimiet reeds overstreden.</div>']);
     } 
	}

	public function autocomplete(){
	$term = Input::get('term');
	
	$results = array();
	
	$queries = DB::table('klanten')
		->where('id', 'LIKE', '%'.$term.'%')
		->orWhere('bedrijfsnaam', 'LIKE', '%'.$term.'%')
		->take(5)->get();
	
	foreach ($queries as $query)
	{
	    $results[] = [ 'id' => $query->id, 'value' => $query->id.' '.$query->bedrijfsnaam ];
	}
return Response::json($results);
}

public function zoekklant(Request $request) {

	$klant = Klant::find($request->input('q'));

	if(empty($klant)) {

		return redirect('/')->with('status', 'De klant is niet gevonden.');

	} else {

		return redirect::to('/bekijkklant/'.$klant->id.'');

	}

}
    
}
