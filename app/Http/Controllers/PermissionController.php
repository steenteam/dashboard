<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;

class PermissionController extends Controller
{

    // Functie om alle permissions toe te voegen aan het systeem
	public function addPermissions()
	{

		// Order permissies

		$permission = Permission::create(['name' => 'edit order']);

		$permission = Permission::create(['name' => 'create order']);

		// Klant permissies

		$permission = Permission::create(['name' => 'see credit']);

		// Gebruikers permissies

		$permission = Permission::create(['name' => 'edit users']);


	}

	public function addRoles()
	{

		$admin = Role::create(['name' => 'super-admin']);

		$admin->givePermissionTo(Permission::all());

		$sales = Role::create(['name' => 'sales']);

		$user = User::find('1');

		$user->assignRole('super-admin');

	}


    //
}
