<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Kalender;

use URL;

class KalenderController extends Controller
{

	public function delete($id)
	{

		$agenda = Kalender::find($id);

		$agenda->delete();

		return redirect(URL::previous());

	}


}
