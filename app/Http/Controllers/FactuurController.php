<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use URL;


class FactuurController extends Controller
{

	public function index()
	{

		$ordersopen = Order::where('factuur', 0)->get();
		$ordersdone = Order::where('factuur', 1)->get();

		return view('facturen')->with(compact('ordersopen', 'ordersdone'));

	}

	public function voltooi(Request $request, $id)
	{

		$order = Order::find($id);

		$order->factuurnr = $request->input('factuurnr');

		$order->factuur = 1;

		$order->save();

		return redirect(URL::previous());
	}

	public function factureren($id)
	{

		$order = Order::find($id);

		$order->factuur = 0;

		$order->save();

		return redirect(URL::previous());

	}


    //
}
