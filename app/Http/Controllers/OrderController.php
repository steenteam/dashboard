<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Order;
use App\Klant;
use App\Voorraad;

use App\Product;
use App\KlantGeschiedenis;
use Auth;
use File;
use Woocommerce;
use Excel;

use Sauladam\ShipmentTracker\ShipmentTracker;

use GoogleCloudPrint;
use PHPExcel_Worksheet_PageSetup;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Alignment;

use Redirect;
use DB;

require_once('../app/SendCloudApi.php');

class OrderController extends Controller
{
   
    // Algemeen overzicht van alle orders
	public function index()
	{

		$orders = Order::latest('created_at')
        ->where([
            ['status', '=', 0],
            ['factuur', '=', NULL]
        ])
        ->get();

		return view('bestelling.bestellingen')->with(compact('orders'));

	}

	// Toon een enkele order
	public function show($id)
	{

		$order = Order::find($id);

        $checkorders = DB::table('verekening_order')->where('order_id', $id)->first();

		return view('bestelling.bestelling')->with(compact('order', 'checkorders'));

	}

	public function new()
	{

		$klanten = Klant::all();

		$products = Product::select('id', 'name', 'hoeveelheid')->get();

		$api = new \SendCloudApi('93ee3b815b914b73b4ad84d2dcdae47e', '92ab4f7329a442428db6a6340678adc9');

		// $shipping_methods = $api->shipping_methods->get();

		return view('bestelling.newbestelling')->with(compact('klanten', 'products'));

	}

    public function cancelOnHold($id)
    {

        $old = Order::find($id);

        $old->delete();

        return redirect(URL::previous());

    }

    public function checkOnhold($id)
    {

        $klanten = Klant::all();

        $products = Product::select('id', 'name', 'hoeveelheid')->get();

        $order = Order::where([
            ['id', '=', $id],
            ['status', '=', '1'],
            ['factuur', '=', NULL]
        ])->first();
    

        if($order) {

        $orderproducts = DB::table('order_product')->where([
                ['order_id', '=', $order->id]
            ])->get();

        $api = new \SendCloudApi('93ee3b815b914b73b4ad84d2dcdae47e', '92ab4f7329a442428db6a6340678adc9');

        $shipping_methods = $api->shipping_methods->get();

        // dd($shipping_methods);

        return view('bestelling.checkonhold')->with(compact('klanten', 'products', 'shipping_methods', 'orderproducts', 'order'));

        } else {

            return 'Error, this order is not on hold or cannot be found. <a href="/">Go back.</a>';

        }

    }

	public function create(Request $request)
	{

            ####################################
            ## Aanmaken van order in database ##
            ####################################

    		$order = new Order(); 
    		$order->creator_id = Auth::user()->id;
    		$order->klant_id = $request->input('klant');
    		$order->note = $request->input('note');

		if($request->input('status') === '0' || $request->input('status') === '1') {

			$order->status = $request->input('status');

		} elseif($request->input('status') === '2') {

			$order->status = 0;
			$order->betaald = $request->input('betaald');
    	    $history = new KlantGeschiedenis();
    	    $history->klant_id = $request->input('klant');
    	    $history->bedrag = $request->input('betaald');
    	    $history->save();
	    
		}

		$orderid = mt_rand() . $request->input('klant');
		$order->ordercode = $orderid;
        $order->verzendkosten = $request->input('verzendkosten');


		$order->save();
		$indexid = -1;

        #######################################
        ## Toevoegen van producten aan order ##
        #######################################

        $rowsproducts = array();

		foreach($request->input('products') as $product) {

            // Het toevoegen van de producten aan de order

            $indexid++;
            $aantallen = $request->input('aantallen')[$indexid];
            $prijzen = $request->input('prijzen')[$indexid];
            DB::insert('insert into order_product (order_id, product_id, aantal, nieuwprijs) values (' . $order->id . ', ' . $product . ', ' . $aantallen . ', ' . $prijzen . ')');


           // Niet van de voorraad afhalen
                    if ($request->input('voorraadaf') == false) {

                $voorraad = Voorraad::where([
                    ['product_id', '=', $product],
                    ['aantal', '>', '0']
                ])->first();
                $aantalnew =  $voorraad->aantal - $aantallen;
                $voorraad->aantal = $aantalnew;
                $voorraad->save();
                $producta = Product::find($product);

                            $data = [
              'sku' => $producta->sku
            ];

            $product1 = Woocommerce::get('products', $data);
            $data2 = [
                'manage_stock' => true,
                'stock_quantity' =>  $aantalnew
            ];
            $woocommerce = Woocommerce::put('products/'.$product1[0]['id'].'', $data2);

            }


            $producta = Product::find($product);
            array_push($rowsproducts, array(
                '', $producta->sku, $producta->locatie, $producta->name, $aantallen, $prijzen, $aantallen * $prijzen, ''
            ));

        }

        if($request->input('status') != '1') { // Controleren of de order on hold moet worden gezet

        #########################
        ## SendCloud koppeling ##
        #########################

		    $api = new \SendCloudApi('93ee3b815b914b73b4ad84d2dcdae47e', '92ab4f7329a442428db6a6340678adc9');
		    $klantdata = Klant::find($request->input('klant'));

            if($klantdata->country == 'Netherlands') {
                $land = 'NL';
            } elseif($klantdata->country == 'Belgie') {
                $land = 'BE';
            } elseif($klantdata->country == 'Duitsland') {
                $land = 'DE';
            } elseif($klantdata->country == 'France') {
                $land = 'FR';
            } else {
                $land = 'NL';
            }

		    if($request->input('verzendmethode') != 'ophalen')  {

                if($request->input('anderadres') === '2') {

                            $data = array(
                                'name' => $klantdata->voornaam,
                                'company_name' => $klantdata->bedrijfsnaam,
                                'address' => $request->input('adres'),
                                'house_number' => $request->input('huisnummer'),
                                'city' => $request->input('stad'),
                                'postal_code' => $request->input('postcode'),
                                'telephone' => $klantdata->telefoon,
                                'email' => $klantdata->email,
                                'request_label' => true,
                                'country' => $land, // AAnpassen nadat migratie is voltooid
                                'shipment' => array(
                                    'id' => $request->input('verzendmethode'),
                                ),
                            );

            } else {    

                            $data = array(
                                'name' => $klantdata->voornaam,
                                'company_name' => $klantdata->bedrijfsnaam,
                                'address' => $klantdata->adres, 
                                'house_number' => $klantdata->huisnummer,
                                'city' => $klantdata->stad,
                                'postal_code' => $klantdata->postcode,
                                'telephone' => $klantdata->telefoon,
                                'email' => $klantdata->email,
                                'request_label' => true,
                                'country' => $land, // AAnpassen nadat migratie is voltooid
                                'shipment' => array(
                                	'id' => $request->input('verzendmethode'),
                                ),
                            );


        if($request->input('checkfactuur') == 1) {
            $order->factuur = 0;
        }

            }

		    $new_parcel = $api->parcels->create($data);
		    $datalabel = array(
		    	'id' => $new_parcel['id'],
		    );
		    $label = $api->label->get($new_parcel['id']);

            #############################
            ## Printen sendcloud label ##
            #############################

		    $username = "93ee3b815b914b73b4ad84d2dcdae47e";
            $password = "92ab4f7329a442428db6a6340678adc9";
            $remote_url = $label['label_printer'];

            // Stroom maken
            $opts = array(
              'http'=>array(
                'method'=>"GET",
                'header' => "Authorization: Basic " . base64_encode("$username:$password")                 
              )
            );

            $context = stream_context_create($opts);
            // Open the file using the HTTP headers set above
            $file = file_get_contents($remote_url, false, $context);
            file_put_contents("".$orderid.".pdf", $file);


            $printerIdLabel = '4516feb5-9e59-471a-bac8-0c7bde6a5bac'; 
            $label = GoogleCloudPrint::asPdf()
                ->file(''.$orderid.'.pdf')
                ->range(1, 1)
                ->printer($printerIdLabel)
                ->send();

            $trackingurl = $api->parcels->get($new_parcel['id']);
            DB::table('order_tracking')->insert(
            	['order_id' => $order->id, 'trackingurl' => $trackingurl['tracking_url']]
            );

        }

            ########################
            ## Maken Excel pakbon ##
            ########################

	    Excel::create($orderid, function($excel) use ($rowsproducts, $klantdata, $request) {
				
            $excel->sheet('Pakbon', function($sheet) use ($rowsproducts, $klantdata, $request)  {

                    $sheet->getDefaultStyle()
                        ->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $sheet->setpaperSize(5);
                    $sheet->setHeight(3, 5);
                    $sheet->setShowGridlines(false);
                    $sheet->cell('A2', function($cell) {

                        $cell->setValue('PICK-UP LIST');
                        $cell->setFont(array(
                        	'size' => '18',
                        	'bold' => true
                        ));

                    });

                $rowcount = 11;
                $subtotal = 0;
                $shipping = $request->input('verzendkosten');
                $linecount = 0;

                foreach($rowsproducts as $prod) {
                   	        $sheet->row($rowcount, array(
                    	    ''.$prod[2].'',  ''.$prod[3].'', $prod[4], '€'.number_format((float)$prod[5], 2, '.', '').'', '€'.number_format((float)$prod[6], 2, '.', '').'', $prod[7]
                ));

       	         $linecount++;
       	        if($rowcount % 2 == 0) {

       	             echo $rowcount;

       	             $sheet->row($rowcount, function($row) { $row->setBackground('#e7e7e7'); });

       	        }

       	        $subtotal = $subtotal + $prod[6];
       	        $rowcount = $rowcount + 1;

            }

            $total = $subtotal + $shipping;

                $sheet->row($rowcount + 1, array(
                	'', 'Subtotaal:', '', '€'.number_format((float)$subtotal, 2, '.', '').'', ''
                ));

                $sheet->row($rowcount + 2, array(
                	'', 'Verzendkosten:', '', '€'.number_format((float)$shipping, 2, '.', '').'', ''
                ));

                $sheet->row($rowcount + 3, array(
                	'', 'Totaal:', '', '€'.number_format((float)$total, 2, '.', '').'', ''
                ));

                $cc = $rowcount + 1;
                $bb = $rowcount + 2;
                $aa = $rowcount + 3;

                $sheet->cells('B'.$cc.'', function($cells) {

                      $cells->setAlignment('right');

                });

                 $sheet->cells('B'.$bb.'', function($cells) {

                      $cells->setAlignment('right');

                });

                 $sheet->cells('B'.$aa.'', function($cells) {

                      $cells->setAlignment('right');

                });

                 $rowcountstart = $rowcount + 5;

                $sheet->row($rowcountstart, array(
                    	'', 'Notities', '', '', '', '', '', ''
                ));

                $sheet->row($rowcountstart + 1, array(
                    	'', $request->input('note'), '', '', '', '', '', ''
                 ));

                $sheet->setWidth('B', 65);
                $sheet->setWidth('C', 20);
                $sheet->setWidth('A', 20);
                $sheet->setWidth('D', 15);


                $sheet->row(10, array(
                	'locatie', 'name', 'amount', 'price', 'total'
                ));

                $sheet->row(4, array(
                	'cameracheck', ' name', 'date', '', '#'
                ));

                $time = date("h:i:sa");

                $time = date("h:i:sa", time() + 30);

                $sheet->row(5, array(
                    	date("H:i", strtotime($time)), ' '.$klantdata->bedrijfsnaam.'', date('Y-m-d'), '', '1/1'
                 ));

                $sheet->getStyle('A5')->applyFromArray(array(
                    'fill' => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'e1e1e1')
                    ) 
                ));


                $sheet->getStyle('A10:H10')->applyFromArray(array(
                    'fill' => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '979797')
                    )
                ));

                $sheet->getStyle('A4:H4')->applyFromArray(array(
                    'fill' => array(
                    'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '979797')
                    )
                ));

                $sheet->cells('A10:H10', function($cells) {

                $cells->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '13',
                    'color' => array('rgb' => 'FFFFFF'),
                ));

                $cells->setAlignment('left');

                });


                $sheet->cells('A4:H4', function($cells) {

                $cells->setFont(array(
                    'family'     => 'Calibri',
                    'size'       => '13',
                    'color' => array('rgb' => 'FFFFFF'),
                ));

                $cells->setAlignment('left');

                });

        // EINDE HEADER VAN PRODUCTENLIJST

    });

     })->store('pdf', public_path('app/public'));

     $printerId = 'd15336ea-1924-2c67-8317-9bca7008857d';  
     sleep(1);
     $docs = File::files(storage_path('storage/public'));
     $url = asset('app/public/'.$orderid.'.pdf');

        if($request->input('geenpakbon') == false) {

                        $order = GoogleCloudPrint::asPdf()
                        ->url($url)
                        ->range(1, 1)
                        ->printer($printerId)
                        ->marginsInCentimeters(-1, -1, -1, -1)
                        ->send();

                        $order = GoogleCloudPrint::asPdf()
                        ->url($url)
                        ->range(1, 1)
                        ->printer($printerId)
                        ->marginsInCentimeters(-1, -1, -1, -1)
                        ->send();

        }

    }

		activity()->log('bestelling '.$orderid.' gemaakt.');

		return redirect('/')->with('status', 'De bestelling is succesvol aangemaakt.');

    }


            public function onholdupdate(Request $request, $id)
    {

        $old = Order::find($id);

        $old->delete();

        OrderController::create($request);

        return redirect('/')->with('status', 'De bestelling is succesvol aangemaakt.');

    
            }


    public function printPakbon($orderid)
    {

        $printerId = 'd15336ea-1924-2c67-8317-9bca7008857d'; 


        $url = asset('app/public/'.$orderid.'.pdf');

        $order = GoogleCloudPrint::asPdf()
    ->url($url)
    ->range(1, 1)
    ->printer($printerId)
    ->marginsInCentimeters(-1, -1, -1, -1)
    ->send();

    }

	public function cancel($id)
	{

		// Orderstatus veranderen


		$order = Order::find($id);

		$order->status = 3;

		$order->save();

		// Product terugzetten

		// $producten = DB::select('select * from order_product where order_id = '.$id.'');

		// foreach($producten as $product) {

		// 	$productaantal = DB::select('select * from products where id = '.$product->product_id.'');

		// 	$nieuweproduct = $productaantal[0]->hoeveelheid + $product->aantal;

		// 	DB::update('update products set hoeveelheid = '.$nieuweproduct.' where id = '.$product->product_id.'');

		// //	dd($product);

		// }

		return Redirect::back();



	}

	public function inlineedit($id) 
	{

	    $klanten = Klant::all();
	    $klant = Klant::where('id', '5')->first();

	    $order = DB::select('select * from order_product where order_id = '.$id.'');
	    $order2 = DB::select('select * from order_product where order_id = '.$id.'');

		$totaal = 0;

		foreach($order2 as $order2) {

			$totaal = $totaal + $order2->nieuwprijs * $order2->aantal;

		}

		return view('modals.edit')->with(compact('klanten', 'totaal', 'id', 'order', 'klant'));

	}


	public function getrowdata($id)
	{

		$producten = DB::select('select * from order_product where order_id = '.$id.'');

        $rows = array();

		foreach($producten as $product) {

			array_push($rows, array($product->id, $product->aantal, $product->nieuwprijs));

		}

         $response = array(
            'status' => 'success',
            'response' => $rows,
        );


        return \Response::json($response);


	}

	public function update($id, Request $request)
	{

		$order = Order::find($id);

		$order->save();

		$indexid = -1;

		$test = array();

        $rowsproducts = array();

        $productsinorder = DB::table('order_product')->where('order_id', $id)->delete();




        foreach($request->input('products') as $product) {

            // Het toevoegen van de producten aan de order

            $indexid++;

            $aantallen = $request->input('aantallen')[$indexid];

            $prijzen = $request->input('prijzen')[$indexid];

            DB::insert('insert into order_product (order_id, product_id, aantal, nieuwprijs) values (' . $order->id . ', ' . $product . ', ' . $aantallen . ', ' . $prijzen . ')');

            $producta = Product::find($product);


            array_push($rowsproducts, array(
                '', $producta->sku, $producta->locatie, $producta->name, $aantallen, $prijzen, $aantallen * $prijzen, ''
            ));

        }

		 return Redirect::back()->with('status', 'De bestelling is succesvol aangepast.');

}

}
