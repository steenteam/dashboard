<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Categorie;
use DB;
use Response;

use Vnn\WpApiClient\Auth\WpBasicAuth;
use Vnn\WpApiClient\Http\GuzzleAdapter;
use Vnn\WpApiClient\WpClient;
use App\Voorraad;

use GuzzleHttp\Client;

use Woocommerce;

use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{

	public function __construct() {

		// Authorisatie: is de gebruiker ingelogd.
		$this->middleware('auth');

	}

	public function getprice($id)
	{

		$product = Product::find($id);

		return $product->price;

	}

    // Overzicht product pagina
	public function index() 
    {

		$product = Product::all();

		return view('product')->with(compact('product'));

	}

	public function editInkoopshow($id)
	{

		$product = Product::find($id);

		return view('editinkoop')->with(compactb  ('product'));

	}

	public function editInkoop($id, Request $request)
	{

		$product = Product::find($id);

		$product->price = $request->price;

		$product->save();

		return redirect('product');

	}

	// Maak een nieuwe product
	public function new()
	{

		$params = [
  		  'per_page' => 100,
  		  'page' => 1
		];

		$allcategories = Woocommerce::get('products/categories', $params);

		return view('newproduct')->with(compact('allcategories'));

	}

	// Daadwerkelijk aanmaken van een product
	public function create(Request $request)
	{

		$files = $request->file('images');

		$images = array();

         if($files) {
			foreach($files as $file)
			{
				$id = uniqid('img_');
				$position = 0;
				$file->move('uploads', "".$id.".png");
				echo ''.$id.'.png succes<br>';
				array_push($images, array('src' => ''.url('/uploads').'/'.$id.'.png', 'position' => $position));
				$position++;
			}
		}

			// dd(json_encode($images));

				$data = [
				    'name' => $request->input('name'),
				    'type' => 'simple',
				    'sku' => $request->input('sku'),
				    'regular_price' => $request->input('price'),
				    'description' => $request->input('description'),
				    'short_description' => $request->input('short_description'),
				    'images' => $images,
				];

   $commerce = Woocommerce::post('products', $data);

   dd($commerce);

		$product = new product(Input::all());

		$product->wcid = $commerce->id;

		$product->type = "default"; // WooCommerce Display

		$product->save();

		activity()->log('product '.$product->name.' gemaakt.');

		return redirect('product')->with('status', 'De product is succesvol aangemaakt.');

	}
     
    // Delete logica van de product
	public function delete($id)
	{

		$product = product::find($id);

		$product->delete();

		activity()->log('product '.$product->name.' verwijderd.');

		return redirect('product')->with('status', 'De product is succesvol verwijderd.');

	}

	// Edit logica voor productën

	public function edit($id)
	{

		$product = product::find($id);

		$allproducts = product::all();

		return view('editproduct')->with(compact('product', 'allproducts'));

	}

	// Update logica voor productën

	public function update($id, Request $request)
	{

		$product = product::find($id);

		$product->fill(Input::all());

		if ($request->hasFile('image')) {

		$path = $request->file('image')->store('public/catimages');

		$product->image = $path;

	    }

	    if(!empty($request->input('parentid'))) {
			$product->parentid = $request->input('parentid');
		} else {
			$product->parentid = 0;
		}

		$product->wcid = rand(5, 15);

		$product->save();

		activity()->log('product '.$product->name.' aangepast.');

		return redirect('product')->with('status', 'De product is succesvol aangepast.');

	}

 
public function searchProduct(Request $request)
{

    $data = Product::select("name as name")->where("name","LIKE","%{$request->input('query')}%")->get();

    return response()->json($data);

}

    public function getStock(Request $request, $id)
    {

    	$product = Product::find($id);

    	$stock = 0;

    	$voorraad = Voorraad::where([
    		['product_id', '=', $product->id],
    		['aantal', '>', '0']
    	])->get();

    	foreach($voorraad as $voor) {
    		$stock = $stock + $voor->aantal;
    	}

        $response = array(
            'status' => 'success',
            'response' => $stock,
            'name' => $product->name
        );
        return \Response::json($response);
    }

    // Magazijn pagina

	public function magazijn()
	{

			$product = Product::all();

			return view('magazijn')->with(compact('product'));


	}

        public function showeditMagazijn($id)
        {
        	$locatie = Product::find($id);

        	return view('editlocatie')->with(compact('locatie'));

        }

		public function editMagazijn(Request $request)
		{

			$product = Product::find($request->input('productid'));

			$product->locatie = $request->input('locatie');

			$product->save();

			return redirect('magazijn');

		}

		public function getinkoop($id)
		{

			$product = Product::find($id);

			$inkoop = $product->price;

			        $response = array(
            'status' => 'success',
            'response' => $inkoop,
        );

        return \Response::json($response);

		}



}
