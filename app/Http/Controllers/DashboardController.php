<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use App\Order;
use App\Inkoop;
use App\Klant;
use App\Verekening;
use App\Kalender;
use App\Voorraad;


use GoogleCloudPrint;

use File;

use Smalot\Cups\Builder\Builder;
use Smalot\Cups\Manager\JobManager;
use Smalot\Cups\Manager\PrinterManager;
use Smalot\Cups\Transport\Client;
use Smalot\Cups\Transport\ResponseParser;

use PHPExcel_Worksheet_PageSetup;

use Excel;

use Carbon\Carbon;

use Auth;
use DB;

use Spatie\Activitylog\Models\Activity;

class DashboardController extends Controller
{

	public function __construct() {

        // Authorisatie: is de gebruiker ingelogd.
		$this->middleware('auth');

	}

	public function index() {


		$orders = Order::where([
			['factuur', '=', NULL],
			['status', '!=', '1']
		])->whereDate('created_at', Carbon::today())->get();

		$ordershold = Order::where([
			['status', '=', '1'],
			['creator_id', '=', Auth::user()->id],
			['factuur', '=', NULL]
		])->get();

		$klantenopen = Klant::all();

		$waardig = array();

		foreach($klantenopen as $klantopen) {

			if($klantopen->openstaand($klantopen->id) >= $klantopen->kredietplafon){
				array_push($waardig, $klantopen);
			}

		}



		$producten = Product::all()->count();

		$logs = Activity::orderBy('id', 'desc')->take(4)->get();

		$productstoday = Product::whereDate('created_at', DB::raw('CURDATE()'))->get();

		$productstodaycount = $productstoday->count();

		$kalender = Kalender::where('creator_id', Auth::user()->id)->get();

		return view('dashboard')->with(compact('producten', 'logs', 'orders', 'ordershold', 'productstoday', 'productstodaycount', 'inkoopcontrole', 'waardig', 'kalender'));

	}

	public function test()
	{

// 		Excel::load('klantgegevens.xlsx', function($reader) {

// 			$id = 2;

// 			foreach($reader->get() as $read) 
// 			{

// 			    $klant = new Klant();

// 			    $klant->creator_id = $read->creator_id;
// 			    $klant->id = $id;
// 			    $klant->voornaam = $read->voornaam;
// 			    $klant->achternaam = $read->achternaam;
// 			    $klant->bedrijfsnaam = $read->bedrijfsnaam;
// 			    $klant->email = $read->email;
// 			    $klant->adres = $read->adres;
// 			    $klant->stad = $read->stad;
// 			    $klant->country = $read->country;
// 			    $klant->postcode = $read->postcode;
// 			    $klant->kredietplafon = $read->kredietplafon;
// 			    $klant->telefoon = $read->telefoon;

// 			    $id++;

// 			    $klant->save();

// 			}



// });


// 		Excel::load('klantgegevens.xlsx', function($reader) {

// 			$id = 2;

// 			//dd($reader->get());

// 			foreach($reader->get() as $read) 
// 			{

// 			    $klant = new Klant();

// 			    $klant->creator_id = $read->creator_id;
// 			    $klant->id = $id;
// 			    $klant->voornaam = $read->voornaam;
// 			    $klant->achternaam = $read->achternaam;
// 			    $klant->bedrijfsnaam = $read->bedrijfsnaam;
// 			    $klant->email = $read->email;
// 			    $klant->adres = $read->adres;
// 			    $klant->huisnummer = $read->huisnummer;
// 			    $klant->stad = $read->stad;
// 			    $klant->country = $read->country;
// 			    $klant->postcode = $read->postcode;
// 			    $klant->kredietplafon = $read->kredietplafon;
// 			    $klant->telefoon = $read->telefoon;

// 			    $id++;

// 			    $klant->save();

// 			}



// });


	}

	public function agendanew(Request $request)
	{

		$kalender = new Kalender();

		$kalender->content = $request->input('content');

		$kalender->tijd = $request->input('tijd');

		$kalender->creator_id = Auth::user()->id;

		$kalender->save(); 

		return redirect('/');

	}

	public function showStatistics()
	{

		// Alle openstaande debiteuren en totaal openstaand

		$klant = Klant::all();

		$klanten = array();

		$totaalopenstaand = 0;

		foreach($klant as $klant)
		{

			if($klant->openstaand($klant->id) > 0) {

				array_push($klanten, $klant);

				$totaalopenstaand = $totaalopenstaand + $klant->openstaand($klant->id);

			}

		}

		// Totale waarde van voorraad berekenen

		$totalevoorraad = 0;

		$totaleaantal = 0;

	    $voorraad = Voorraad::all();

		foreach($voorraad as $voorraad)
		{

			$count = $voorraad->aantal * $voorraad->inkoopprijs;

			$totalevoorraad = $totalevoorraad + $count;

			$totaleaantal = $totaleaantal + $voorraad->aantal;

		}

		return view('statistics')->with(compact('klanten', 'totaalopenstaand', 'totalevoorraad', 'totaleaantal'));

	}

	public function getInkoopRaw($id)
		{

			$voorraad = Voorraad::where([
				['aantal', '>', '0']
			])->get();

			$neweerst = $voorraad->sortBy('created_at');

			foreach($neweerst as $new)
			{

							echo $new->inkoopprijs;
							echo '<br><b>';
							echo $new->product_id;
							echo '</b><br>';

			}


		}

			public function productsimport2()
	{

		Excel::load('imporrt.xlsx', function($reader) {

			$id = 1;

			foreach($reader->get() as $read) 
			{

				//dd($read);

			    $product = new Product();

			    $product->id = $id;

			    $product->wcid = '1';
			    $product->name = $read->title;
			    $product->price = $read->price;
			    $product->hoeveelheid = '100';
			    $product->type = 'normal';
			    $product->description = $read->title;
			    $product->sku = $read->sku;
			    $product->inkoop = '2';
			    $product->locatie = 'A1';
			    $product->shortdescription = $read->title;

			    $id++;

			    $product->save();
			    
			    echo $product->name;
			    echo $product->price;
			    echo '<br>';

			}



});

	}

	public function productsimport()
	{

		Excel::load('importproducts.csv', function($reader) {

			$id = 1;

			foreach($reader->get() as $read) 
			{

				//dd($read);

			    $product = new Product();

			    $product->id = $id;

			    $product->wcid = $read->id;
			    $product->name = $read->title;
			    $product->price = $read->price;
			    $product->hoeveelheid = $read->stock;
			    $product->type = 'normal';
			    $product->description = $read->title;
			    $product->sku = $read->sku;
			    $product->inkoop = '2';
			    $product->locatie = 'A1';
			    $product->shortdescription = $read->title;

			    $id++;

			    $product->save();
			    
			    echo $product->name;
			    echo $product->price;
			    echo '<br>';

			}



});

	}

		public function voorraadimport()
	{

		Excel::load('voorraad.xlsx', function($reader) {

			$id = 1;

			foreach($reader->get() as $read) 
			{

			if($read->euro != 0) {

				$product = Product::where('sku', $read->artikelcode)->first();

				//dd($read);

			    $voorraad = new Voorraad();

			    $voorraad->id = $id;

			    if($product) {

			        $voorraad->product_id = $product->id;

			    } else {

			    	$voorraad->product_id = '100000';

			    }


			    $voorraad->aantal = 1000;

			    $voorraad->inkoopprijs = $read->euro;
                

                echo $read->euro;
			    echo '<br>';

			    $id++;

			    $voorraad->save();


			}

			}



});

	}

	public function testPakbon()
	{

Excel::create('pakbon', function($excel) {

       $excel->sheet('pakbon', function($sheet) {

       	$sheet->getPageSetup()
->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LETTER_SMALL);

$sheet->setpaperSize(5);


        // ADRES GEGEVENS 

        $sheet->row(2, array(
            '', 'Steen Solutions'
        ));

        $sheet->row(3, array(
        	'', 'Oude Kraan 72-412'
        ));

        $sheet->row(4, array(
        	'', '6865BN Doorwerth'
        ));

        $sheet->row(5, array(
        	'', 'Nederland'
        ));

        // EIND ADRESGEGEVENS

        // HEADER VAN PRODUCTENLIJST

        $sheet->row(7, array(
        	'', 'SKU', 'LOCATION', 'NAME', 'QTY', 'PRICE', 'TOTAL', 'CHECK'
        ));

        $sheet->cells('B7:H7', function($cells) {

    $cells->setFont(array(
    'family'     => 'Calibri',
    'size'       => '16',
    'bold'       =>  true
));
});

        

        // EINDE HEADER VAN PRODUCTENLIJST




});

})->download('xlsx');

	}


public function testerror() 
{

dd(aaa);

}


		public function importeerpakbonnen()
	{

		Excel::load('klantpakbonnen.xlsx', function($reader) {

    	$counter = 0;



		    

			foreach($reader->get() as $read) 
			{

				 $klant = Klant::where('bedrijfsnaam', $read->klant_name)->first();

               if(!empty($klant->id)) {

				 if($read->datum_1 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_1;
				 $verekening->created_at = $read->datum_1;
				 echo '1';
				 $verekening->save();	

				 }

				 if($read->datum_2 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_2;
				 $verekening->created_at = $read->datum_2;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_3 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_3;
				 $verekening->created_at = $read->datum_3;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_4 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_4;
				 $verekening->created_at = $read->datum_4;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_5 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_5;
				 $verekening->created_at = $read->datum_5;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_6 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_6;
				 $verekening->created_at = $read->datum_6;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_7 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_7;
				 $verekening->created_at = $read->datum_7;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_8 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_8;
				 $verekening->created_at = $read->datum_8;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_9 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_9;
				 $verekening->created_at = $read->datum_9;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_10 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_10;
				 $verekening->created_at = $read->datum_10;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_11 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_11;
				 $verekening->created_at = $read->datum_11;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_12 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_12;
				 $verekening->created_at = $read->datum_12;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_13 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_13;
				 $verekening->created_at = $read->datum_13;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_14 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_14;
				 $verekening->created_at = $read->datum_14;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_16 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_16;
				 $verekening->created_at = $read->datum_16;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_17 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_17;
				 $verekening->created_at = $read->datum_17;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_18 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_18;
				 $verekening->created_at = $read->datum_18;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_19 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_19;
				 $verekening->created_at = $read->datum_19;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_20 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_20;
				 $verekening->created_at = $read->datum_20;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_21 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_21;
				 $verekening->created_at = $read->datum_21;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_22 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_22;
				 $verekening->created_at = $read->datum_22;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_23 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_23;
				 $verekening->created_at = $read->datum_23;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_24 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_24;
				 $verekening->created_at = $read->datum_24;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_25 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_25;
				 $verekening->created_at = $read->datum_25;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_26 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_26;
				 $verekening->created_at = $read->datum_26;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_27 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_27;
				 $verekening->created_at = $read->datum_27;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_28 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_28;
				 $verekening->created_at = $read->datum_28;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_29 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_29;
				 $verekening->created_at = $read->datum_29;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_30 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_30;
				 $verekening->created_at = $read->datum_30;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_31 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_31;
				 $verekening->created_at = $read->datum_31;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_32 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_32;
				 $verekening->created_at = $read->datum_32;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_33 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_33;
				 $verekening->created_at = $read->datum_33;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_34 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_34;
				 $verekening->created_at = $read->datum_34;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_35 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_35;
				 $verekening->created_at = $read->datum_35;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_36 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_36;
				 $verekening->created_at = $read->datum_36;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_37 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_37;
				 $verekening->created_at = $read->datum_37;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_38 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_38;
				 $verekening->created_at = $read->datum_38;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_39 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_39;
				 $verekening->created_at = $read->datum_39;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_40 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_40;
				 $verekening->created_at = $read->datum_40;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_41 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_41;
				 $verekening->created_at = $read->datum_41;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_42 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_42;
				 $verekening->created_at = $read->datum_42;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_43 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_43;
				 $verekening->created_at = $read->datum_43;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_44 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_44;
				 $verekening->created_at = $read->datum_44;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_45 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_45;
				 $verekening->created_at = $read->datum_45;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_46 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_46;
				 $verekening->created_at = $read->datum_46;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_47 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_47;
				 $verekening->created_at = $read->datum_47;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_48 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_48;
				 $verekening->created_at = $read->datum_48;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_49 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_49;
				 $verekening->created_at = $read->datum_49;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_50 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_50;
				 $verekening->created_at = $read->datum_50;
				 echo '1';
				 $verekening->save();	

				 }

				 				 if($read->datum_51 != null) {

				 $verekening = new Verekening();
				 $verekening->status = 0;
				 $verekening->klant_id = $klant->id;
				 $verekening->prijs = $read->bedrag_51;
				 $verekening->created_at = $read->datum_51;
				 echo '1';
				 $verekening->save();	

				 }




				 echo $klant->id;


				} else {
					echo 'FOUT!';
					echo $read->klant_name;
				}
				 echo '<br>';


		
				

			}

});

	}


	public function testprint()
	{

     // $printerId = 'e6cb3a30-3387-2230-216f-4cf9a2541589';  PageWide
	//	$printerId = '574c2971-d077-35c1-5b56-904b7b69fa66'; // Zebra 
		$printerId = '1ecc64e6-e5d1-ff07-0b86-0d20e1bd0261';

GoogleCloudPrint::asText()
    ->content('This is a test')
    ->printer($printerId)
    ->marginsInCentimeters(1, 1, 1, 1)
    ->send();


	}

}
