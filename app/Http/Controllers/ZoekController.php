<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Klant;

class ZoekController extends Controller
{

	public function index()
    {

    	return view('autocomplete');

    }

    public function ajaxData(Request $request)
    {

        $query = $request->get('query','');  

        $klanten = Klant::where('bedrijfsnaam','LIKE','%'.$query.'%')->get();        

        return response()->json($klanten);

	}
    
}
