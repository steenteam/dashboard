<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;

use App\Voorraad;

use Excel;

use App\Inkoop;

class VoorraadController extends Controller
{


    // Pagina waarvanuit de productlijst kan worden geëxporteert.
	public function exportPagina()
	{


		return view('voorraad.exporteren');

	}

	// Methode om daadwerkelijk te exporteren
	public function export(Request $request)
	{

Excel::create('Exports', function($excel) use ($request) {

		$producten = array();

		$query = Product::query();

		if($request->device != 'none') {
			$query->where('device', $request->device);
		}

		if($request->merk != 'none') {
			$query->where('merk', $request->merk);
		}

		if($request->categorie != 'none') {
			$query->where('categorie', $request->categorie);
		}

		$products = $query->get();

		foreach($products as $product)
		{

			$aantal = $product->getStock($product->id);

			if($aantal < $request->input('aantal'))
			{

				array_push($producten, $product->id);

			}

		}



			    $excel->sheet('Voorraad', function($sheet) use ($producten) {
  	$row = 2;

			    	$sheet->row(1, array(
     'SKU', 'Quantity', 'Price', 'Total'
));

			    	foreach($producten as $prod) {

			    		$details = Product::where('id', $prod)->first();

			    		$sheet->row($row, array(
                           $details->sku
                        ));

			    		$row++;


			    	}

        

                  });


        })->download('xlsx');

	}

	public function voorraadBeheer()
	{

		$inkopentodo = Inkoop::where('gecontroleerd', false)->get();

		$inkopenaf = Inkoop::where('gecontroleerd', true)->get();

		return view('voorraad.beheer')->with(compact('inkopentodo', 'inkopenaf'));

	}

	public function newInkoop()
	{

		return view('voorraad.new');

	}

	public function create(Request $request)
	{

		  if(!$request->hasFile('excelfile')) {
    return response()->json(['upload_file_not_found'], 400);
  }


  $file = $request->file('excelfile');
  if(!$file->isValid()) {
    return response()->json(['invalid_file_upload'], 400);
  }

  $today = date("Ymd");
  $rand = strtoupper(substr(uniqid(sha1(time())),0,4));
  $unique = $today . $rand;


  $path = public_path() . '/imports/';
  $file->move($path, ''.$unique.'-'.$file->getClientOriginalName().'' );

  $inkoops = new Inkoop();

  $inkoops->excelfile = ''.$unique.'-'.$file->getClientOriginalName().'';
  $inkoops->tracking = $request->input('tracking');
  $inkoops->gecontroleerd = 0;

  $inkoops->save();

  return redirect('/voorraad/beheer');

	}

	public function controleer($id)
	{

		$inkoop = Inkoop::find($id);

		$reader = Excel::load('/public/imports/'.$inkoop->excelfile.'');

		   $results = $reader->get();


        return view('voorraad.controleer')->with(compact('inkoop', 'results'));

	}

	public function gecontroleerd($id)
	{

		$inkoop = Inkoop::find($id);

		$inkoop->gecontroleerd = 1;

		$inkoop->save();

		$reader = Excel::load('/public/imports/'.$inkoop->excelfile.'');

		$results = $reader->get();

		foreach($results as $result) {

			$voorraad = new Voorraad();

			$productid = Product::where('sku', $result->sku)->first();

			$voorraad->product_id = $productid->id;

			$voorraad->aantal = $result->quantity;

			$voorraad->inkoopprijs = $result->price;

			$voorraad->save();

		}

		return redirect('/voorraad/beheer');

	}

    //
}
