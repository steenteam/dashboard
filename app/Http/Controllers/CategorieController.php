<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Categorie;

use Illuminate\Support\Facades\Input;

class CategorieController extends Controller
{

	public function index()
	{

		$categorieen = Categorie::all();

		return view('categorieen')->with(compact('categorieen'));
	
	}


	// Maak een nieuwe categorie
	public function new()
	{

		$allcategories = Woocommerce::get('orders');

		return view('newcategorie')->with(compact('allcategories'));

	}

	// Daadwerkelijk aanmaken van een categorie
	public function create(Request $request)
	{

		$categorie = new Categorie(Input::all());

		$path = $request->file('image')->store('public/catimages');

		$categorie->image = $path;

		if(!empty($request->input('parentid'))) {
			$categorie->parentid = $request->input('parentid');
		} else {
			$categorie->parentid = 0;
		}

		$categorie->wcid = rand(5, 15);

		$categorie->save();

		activity()->log('Categorie '.$categorie->name.' gemaakt.');

		return redirect('categorieen')->with('status', 'De categorie is succesvol aangemaakt.');

	}
     
    // Delete logica van categorieën
	public function delete($id)
	{

		$categorie = Categorie::find($id);

		$categorie->delete();

		activity()->log('Categorie '.$categorie->name.' verwijderd.');

		return redirect('categorieen')->with('status', 'De categorie is succesvol verwijderd.');

	}

	// Edit logica voor categorieën

	public function edit($id)
	{

		$categorie = Categorie::find($id);

		$allcategories = Categorie::all();

		return view('editcategorie')->with(compact('categorie', 'allcategories'));

	}

	// Update logica voor categorieën

	public function update($id, Request $request)
	{

		$categorie = Categorie::find($id);

		$categorie->fill(Input::all());

		if ($request->hasFile('image')) {

		$path = $request->file('image')->store('public/catimages');

		$categorie->image = $path;

	    }

	    if(!empty($request->input('parentid'))) {
			$categorie->parentid = $request->input('parentid');
		} else {
			$categorie->parentid = 0;
		}

		$categorie->wcid = rand(5, 15);

		$categorie->save();

		activity()->log('Categorie '.$categorie->name.' aangepast.');

		return redirect('categorieen')->with('status', 'De categorie is succesvol aangepast.');

	}


    //
}
