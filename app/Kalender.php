<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kalender extends Model
{

	protected $fillable = ['creator_id', 'content', 'tijd'];

}
