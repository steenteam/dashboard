<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Woocommerce;

class Wookoppeling extends Model
{

	public static function addToStore($test)
	{

	    	$data = [
               'product' => [
               'name' => $test
            ]
            ];

            return Woocommerce::post('products', $data);

	}
    //
}
