<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Klant;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $klanten = Klant::all();

        view()->share('klanten', $klanten);
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
