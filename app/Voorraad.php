<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voorraad extends Model
{

	protected $softDelete = true;

	public function getInkoopPrijs($id)
	{

		$aantal = 0;

		$prijs = 0;

		$voorraads = Voorraad::where('product_id', $id)->get();

		foreach($voorraads as $vooraad) {

			$aantal = $aantal + $voorraad->aantal;

			$prijs = $prijs + ($voorraad->inkoopprijs * $voorraad->aantal);

		}

		return $prijs / $aantal;

	}
    //
}
