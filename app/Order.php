<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Order extends Model
{

		public function getStatus($tracking)
	{

		$data = parcel_track()
		->dhlExpress()
		->setTrackingNumber($tracking)
		->fetch();
	
		return $data['tracker']['delivered'];

	}

	protected $fillable = ['klantid', 'betaald'];

	public function klant()
	{

		return $this->belongsTo('App\Klant');

	}

	public function products()
	{

		return $this->belongsToMany('App\Product');

	}

	public function order_total($id)
	{

		$products = DB::select('select * from order_product where order_id = '.$id.'');

		$total = 0;

		foreach($products as $product) {
			$total = $total + ($product->aantal * $product->nieuwprijs);
		}

		return $total;

	}

		public function totalprijs($id, $order_id)
	{

		$sum = 0;

		$items = DB::table('order_product')->where([
			['order_id', '=', $order_id],
		])->get();

		foreach($items as $item)
		{

						$aantal = DB::select('select aantal from order_product where (product_id = '.$item->product_id.' AND order_id = '.$item->order_id.')');

			$prijs = DB::select('select nieuwprijs from order_product where (product_id = '.$item->product_id.' AND order_id = '.$item->order_id.')');

						$totalsum = $aantal[0]->aantal * $prijs[0]->nieuwprijs;

						$sum = $sum + $totalsum;


		}

			return $sum;

	}


	public function prijs($id, $order_id)
	{

			$aantal = DB::select('select aantal from order_product where (product_id = '.$id.' AND order_id = '.$order_id.')');

			$prijs = DB::select('select nieuwprijs from order_product where (product_id = '.$id.' AND order_id = '.$order_id.')');

			$totalsum = $aantal[0]->aantal * $prijs[0]->nieuwprijs;

			return $totalsum;

	}

	public function stukprijs($id, $order_id)
	{
			$prijs = DB::select('select nieuwprijs from order_product where (product_id = '.$id.' AND order_id = '.$order_id.')');

	    	return $prijs[0]->nieuwprijs; 

	}


	public function getTracking($id)
	{

		$tracking = DB::table('order_tracking')->where([
			['order_id', '=', $id],
		])->first();

		if(empty($tracking)) {
			return 'null';
		} else {

		return $tracking->trackingurl;

	}

	}

	public function aantal($id, $order_id)
	{

		$aantal = DB::select('select aantal from order_product where (product_id = '.$id.' AND order_id = '.$order_id.')');

		return $aantal[0]->aantal;

	}


	public function status($orderid)
	{

	    $order = Order::find($orderid);

	    $status = array(
	    	"Openstaand",  // 0
	    	"On hold", // 1
	    	"Voltooid", // 2
	    	"Geannuleerd" // 3
	    );

	    return $status[$order->status];

	}

}
