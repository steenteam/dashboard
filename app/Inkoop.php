<?php

namespace App;

use Sauladam\ShipmentTracker\ShipmentTracker;

use Illuminate\Database\Eloquent\Model;

class Inkoop extends Model
{

	public function getStatus($tracking)
	{

		$data = parcel_track()
		->dhlExpress()
		->setTrackingNumber($tracking)
		->fetch();
	
		return $data['tracker']['delivered'];

	}

	public function getArrived() {

		return $aantal;

	}

}
