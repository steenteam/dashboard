<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Verekeningen;
use App\Order;

class Klant extends Model
{

	protected $fillable = ['voornaam', 'achternaam', 'bedrijfsnaam', 'email', 'adres', 'stad', 'postcode', 'telefoon', 'kredietplafon', 'country'];

    public $incrementing = true;
    //

    public function order()
    {

    	return $this->hasMany('App\Order');
    
    }

    public function getname($id)
    {

        $product = Product::find($id)->name;

        return $product;

    }

    public function hasAnything($id)
    {

        $order = Order::where('klant_id', $id)->get();

        $verekening = Verekening::where('klant_id', $id)->get();

        if($order->isEmpty() && $verekening->isEmpty()) {
            $door = true;
        } else {

            $door = false;
        }

        return $door;

    }

    public function openstaand($klantid)
    {

    	$openstaand = 0;

    	// Openstaande bedragen van verekeningen berekenen

    	$verekeningen = Verekening::where([
    		['klant_id', '=', $klantid],
    		['status', '=', '0']
    	])->get();

    	
    	foreach($verekeningen as $verekening) {
    		$openstaand = $openstaand + $verekening->prijs;
    	}

    	// Openstaande bedragen van orders berekenen

    	$orders = Order::where([
    		['klant_id', '=', $klantid],
    		['status', '=', '0'],
            ['factuur', '=', null]
    	])->get();

    	foreach($orders as $order) {
    		$prijs = $order->order_total($order->id) + $order->verzendkosten;
    		$openstaand = $openstaand + $prijs;
    	}

    	return $openstaand;

    }
}
