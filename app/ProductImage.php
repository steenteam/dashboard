<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{	

    // Return de parent van de productafbeelding
	public function product()
	{
		return $this->belongsTo('App\Product');
	}
    
}
