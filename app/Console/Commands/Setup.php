<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'De hele applicatie opzetten.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Permission::query()->delete();

        Role::query()->delete();

        app()['cache']->forget('spatie.permission.cache');

        app('App\Http\Controllers\PermissionController')->addPermissions();

        app('App\Http\Controllers\PermissionController')->addRoles();

        $this->line('Permissions opgezet');
        //
    }
}
