<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;

class TestUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testuser {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a testuser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $id = DB::table('users')->insertGetId([
            'name' => ''.$this->argument('name').' gebruiker',
            'email' => ''.$this->argument('name').'@'.$this->argument('name').'.nl',
            'password' => bcrypt('secret'),
            'avatar' => 'http://i.imgur.com/H357yaH.jpg',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        activity()->causedBy($id)->log('is aangemaakt.');

        $this->line('User succesvol aangemaakt');
        $this->line('E-mail: '.$this->argument('name').'@'.$this->argument('name').'.nl');
        $this->line('Wachtwoord: secret');

        //
    }
}
