<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{

	protected $fillable = ['name', 'image'];

	public function product()
	{

		return $this->belongsToMany('App\Product');

	}
    
}
