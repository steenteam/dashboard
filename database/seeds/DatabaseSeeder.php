<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            'name' => 'Stijn Bakker',
            'email' => 'Contact@steensolutions.nl',
            'password' => bcrypt('secret'),
            'avatar' => 'https://media.licdn.com/dms/image/C4D03AQE0rZNv6syjUQ/profile-displayphoto-shrink_200_200/0?e=1531353600&v=beta&t=18kvT9Eg2ptBskET8URV5Fggr_p-S1tgPSppHQG-xdM'
        ]);

        DB::table('klants')->insert([
            'voornaam' => 'Stijn',
            'achternaam' => 'Bakker',
            'bedrijfsnaam' => 'Steen Solutions',
            'email' => 'Contact@steensolutions.nl',
            'adres' => 'Kabeljauwallee 2',
            'stad' => 'Doorwerth',
            'postcode' => '6865BN',
            'telefoon' => '0681318737',
            'kredietplafon' => '1000'
        ]);

        DB::table('products')->insert([
            'wcid' => '1',
            'name' => 'Wasmachine',
            'price' => '300',
            'type' => 'default',
            'hoeveelheid' => '1',
            'sku' => 'aaa',
            'locatie' => 'Amersfoort',
            'inkoop' => '1',
            'description' => 'Een mooie wasmachine, wat een prachtding.',
            'shortdescription' => 'Mooi ding.',
        ]);

        DB::table('products')->insert([
            'wcid' => '2',
            'name' => 'Dweil',
            'price' => '300',
            'type' => 'default',
            'hoeveelheid' => '1',
            'sku' => 'aaa',
            'locatie' => 'Amersfoort',
            'inkoop' => '1',
            'description' => 'Een mooie Dweil, wat een prachtding.',
            'shortdescription' => 'Mooi ding.',
        ]);

        DB::table('orders')->insert([
            'klant_id' => '1',
            'status' => '2',
            'ordercode' => '2222',
        ]);

        DB::table('order_product')->insert([
            'order_id' => '1',
            'product_id' => '1',
            'aantal' => '1',
            'nieuwprijs' => '1',

        ]);



    }
}
