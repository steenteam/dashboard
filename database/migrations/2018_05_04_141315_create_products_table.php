<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('wcid')->unique();

            $table->string('name');

            $table->string('soort');

            $table->string('merk');

            $table->string('device');

            $table->string('price');

            $table->string('categorie');

            $table->integer('hoeveelheid');

            $table->string('type');

            $table->longText('description');

            $table->string('sku');

            $table->integer('inkoop');

            $table->string('locatie');

            $table->text('shortdescription');

            $table->softDeletes();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
