<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->increments('id');

            $table->string('ordercode');

            $table->integer('factuur')->nullable();

            $table->integer('factuurnr')->nullable();

            $table->integer('klant_id');

            $table->integer('creator_id');

            $table->integer('betaald')->nullable();

            $table->integer('status');

            $table->string('verzendkosten');

            $table->integer('verekening')->nullable();

            $table->longtext('note')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }

}
