<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('klants', function (Blueprint $table) {

            $table->increments('id');

            // Klantengegevens

            $table->string('voornaam');

            $table->string('achternaam');

            $table->string('bedrijfsnaam');

            $table->string('email');

            $table->string('adres');

            $table->integer('huisnummer');

            $table->string('country');

            $table->string('stad'); 

            $table->string('postcode');

            $table->string('telefoon');     

            $table->integer('creator_id');

            $table->integer('factureren')->nullable();

            // Kredietwaardigheid

            $table->integer('kredietplafon');       

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('klants');
    }
}
